# OSIS

In this project we provide several algorithms to optimally solve One-Sided Info-Sharing Dec-POMDPs. 

## Environment requirement

  - c++		    version >= 5.4.0
  - clang++ 	version >= 3.8.0
  - boost 	  version >= 1.66
  - eigen 	  version >= 3.0.0
  - cplex 	  version >= 12.63


### Prerequisites

1. Install eigen3, clang

```shell
  sudo apt-get install libeigen3-dev
  sudo apt-get install clang
```

2. Install boost library, version 1.66

  - Go to section 5.1 "Easy Build and Install" in [boost download link](http://www.boost.org/doc/libs/1_60_0/more/getting_started/unix-variants.html) and follow the instructions on how to install boost.
  - The installation prefix should be /usr/local

3. Install ILOG CPLEX (third party software)

  The installation should be in /opt/ibm/ILOG/CPLEX_Studio128 on Linux machines and /Applications/CPLEX_Studio128 on MAC machines


### Installing

After all requirements are done, go to the directory where Makefile is in. 

```shell
  make -j8 
```

It should compile if all requirements are satisfied and then an executable file named sdms appears in build directory.

## Algorithm

| Algorithms List |  Description  |
| :-:| :-: |
|   HSVI_1  |   HSVI uses linear representations and corresponding update rules |      
| HSVI_2  |   HSVI uses piecewise-linear and convex (PWLC) representions and exhaustive enumeration to greedily select actions |
| HSVI_3  |   HSVI uses piecewise-linear and convex (PWLC) representions and Mixed Integer Linear Programming to greedily select actions |

## How to use
```
  -h [ --help ]                                 produce help message
  -a [ --algorithm ] arg (=HSVI_1)      set the algorithm to be used -- e.g. HSVI_1
  -g [ --model ] arg (=dpomdp)                  set the model of the problem to be solved -- e.g. dpomdp
  -f [ --filename ] arg (=mabc)                 set the benchmark filename -- e.g. mabc, recycling, tiger, etc
  -p [ --planning_horizon ] arg (=0)            set the planning horizon
  -o [ --optimal-epsilon ] arg(=9.99999975e-05) set the epsilon optimal parameter
  -t [ --trials ] arg (=1)                      set the number of trials
  -m [ --memory ] arg (=1)                      set the memory length limit
  -d [ --discount-factor ] arg (=1)             set the discount factor
  -z [ --output-directory ] arg                 set the output directory -- e.g. results/<algorithm-name>/<bench>/<params>
```
### Command line example
```
./build/sdms -aHSVI_1 -ftiger -p10 -o0.0 -t10000 -m1 -d1.0 -ztest.csv
```

