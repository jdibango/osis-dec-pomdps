/*=============================================================================

==============================================================================*/
#pragma once

#include <functional>

#include "../types.hpp"
#include "../utils/linear_algebra/vector.hpp"

//!
//! \file     hash.hpp

//! \brief    hash class
//! \version  1.0
//! \date     08 May 2016
//!
//! This class provides basic type alias for dpomdp.
//!

//! \namespace  sdm
//!
//! Namespace grouping all tools required for sequential decision making.
namespace sdm{
  template<typename T>
  struct hash : public std::unary_function<T, number> {
      number operator()(const T& p_value) const;
  };

  template<typename T>
  struct hash<T*> : public std::unary_function<T*, number> {
      number operator()(const T*& p_value) const;
  };

  template<typename T>
  struct hash<std::shared_ptr<T>> : public std::unary_function<std::shared_ptr<T>, number> {
      number operator()(const std::shared_ptr<T>& p_value) const;
  };
}
