//! \date  October 2019
#pragma once

/**

*/
#include "../../decision_rules/light_decision_rule.hpp"
#include "../history_state/jhistory.hpp"

/**

*/
//!
//! \file     light_occupancy_state.hpp

//! \brief    light_occupancy_state class
//! \version  1.0
//! \date     10 October 2019
//!
//! This class provides getter and setter methods for light_occupancy_state
namespace sdm{
  /**

  * as a dictionnary from vectors to vectors. I will suggest to to have a container (as an attribute) which should
  * be a mapping from beliefs to reals, so std::unordered_map<std::shared_ptr<Vector>, double> is good!
  *
  */
  template<typename ihistory_t = observation_ihistory>
  class light_occupancy_state {
  private:

  public:
    horizon h = 0;
    observation z0;
    double prob = 1;
    ihistory_t* ih0 = NULL;


    /**

    * /first std::shared_ptr<Vector>  belief
    * /second double probability
    */
    std::unordered_map<std::shared_ptr<Vector>, double> container;

    /*!
     * Constructor
     * Initializing the container with start belief distribution and probability 1.
     */
    light_occupancy_state(horizon=0);

    /*!
     * Constructor
     * Initializing the container with given state and probility 1.
     */
    light_occupancy_state(std::unordered_map<std::shared_ptr<Vector>, double>&, horizon=0);

    /*!
    * Deconstructor
    */
    ~light_occupancy_state();

    horizon getHorizon() const;

    double getValueAt(const std::shared_ptr<Vector>&) const;

    /*!
    *  \fn getVariations
    *  \brief this method computes the variations for the light_decision_rules.
    */
    variations<std::vector<std::shared_ptr<Vector>>, light_decision_rule> getVariations();

    double getReward(std::pair<action, std::shared_ptr<light_decision_rule>>&);

    std::set<std::shared_ptr<Vector>> getSuccessorSupports(action);

    /*!
     * return a decision with a randam action
     */
    std::shared_ptr<light_decision_rule> getRandomDecisionRule();

    std::pair<std::shared_ptr<Vector>, double> next(const std::shared_ptr<Vector>&, action, observation);

    /* !
     * \fn next
     * \param pair of action and observation of agent 1
     * \param light decision rule of agent 2
     *
     */
    std::shared_ptr<light_occupancy_state> next(std::pair<action, observation>&, const std::shared_ptr<light_decision_rule>&);

    ihistory_t* next(action, observation);
  };

  template<typename ihistory_t = observation_ihistory>
  std::ostream& operator<<(std::ostream&, const light_occupancy_state<ihistory_t>&);
}
