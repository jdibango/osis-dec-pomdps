/*=============================================================================

==============================================================================*/
#pragma once

#include "../../../types.hpp"
#include "../../recursive_struct/graph.hpp"

//!
//! \file     ihistory.hpp

//! \brief    observation_ihistory typename
//! \version  1.0
//! \date     11 January 2017
//!
//! This typename provides getter and setter methods for individual histories.
//!

//! \namespace  sdm
//!
//! Namespace grouping all tools required for sequential decision making.
namespace sdm{

  /*
  *  \class observation_ihistory
  *  \brief builds individual history instance
  *
  *  Each instance of this class is a \@tree of individual observations.
  */
  template<typename T>
  using ihistory = graph<T>;

  /*
  *  \class observation_ihistory
  *  \brief builds individual history instance
  *
  *  Each instance of this class is a \@tree of individual observations.
  */
  using observation_ihistory = ihistory<observation>;

  /*
   *  \class action_observation_ihistory
   *  \brief builds individual history instance
   *
   *  Each instance of this class is a \@tree of individual actions and observations.
   */
  using action_observation_ihistory = ihistory<std::pair<action,observation>>;
}
