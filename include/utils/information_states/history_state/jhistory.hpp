/*=============================================================================

==============================================================================*/
#pragma once

#include "ihistory.hpp"

//!
//! \file     jhistory.hpp

//! \brief    jhistory class
//! \version  1.0
//! \date     12 Avril 2016
//!
//! This class provides getter and setter methods for joint histories.
//! Each instance of this class is a \@tree of individual observations.

//! \namespace  sdm
//!
//! Namespace grouping all tools required for sequential decision making.
namespace sdm{

  template<typename T>
  using jhistory = graph<T,1>;

  using observation_jhistory = jhistory<observation>;

  using action_observation_jhistory = jhistory<std::pair<action,observation>>;

  using delayed_action_observation_jhistory = jhistory<std::pair<action,observation>>;

}
