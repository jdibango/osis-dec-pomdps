/*=============================================================================

==============================================================================*/
#pragma once

#include <iostream>

#include "../../types.hpp"

//!
//! \file     node.hpp

//! \brief    node class
//! \version  1.0
//! \date     14 Avril 2016
//!
//! This class provides getter and setter methods for tree node structures.

//! \namespace  sdm
//!
//! Namespace grouping all tools required for sequential decision making.
namespace sdm{
  /*!
  *  \class      graph      graph.hpp
  *  \typename   T type of the item.
  *  \brief      graph structure -- FORWARD DECLARATION
  */
  template<typename T, int N> class base_graph;

  /*!
  *  \class      node      tree.hpp
  *  \brief      tree node structure
  */
  template<typename T, int N = 0>
  class node{
  protected:
    T item;                                    //<! pointer to the item
    base_graph<T, N>* parent = NULL;           //<! parent graph node

  public:

    node();

    node(const T&, base_graph<T,N>*);

    virtual ~node();

    /*!
    *  \fn     T getItem() const
    *  \brief  Returns the item
    *  \return const T& item
    */
    T getItem() const;

    /*!
    *  \fn     base_graph<T,N>*const getParent() const
    *  \brief  Returns the pointer onto the parent graph.
    *  \return node* pointer onto the parent graph.
    */
    base_graph<T,N>*const getParent() const;

    /*!
    *  \fn     friend std::ostream& operator<<(std::ostream&, const node&)
    *  \brief  Returns an ostream instance
    *  \param  std::ostream&  ostream instance
    *  \param  const node& tree node
    *  \return std::ostream&   ostream instance
    */
    friend std::ostream& operator<<(std::ostream& os, const node<T,N>& n){
      if( !n.parent ) os << "->";
      else if(n.parent->getData()) os << *n.parent->getData() << ".";
      os << base_graph<T,N>::to_string(n.item);
      return os;
    }
  };
}
