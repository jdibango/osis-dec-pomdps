/*=============================================================================

==============================================================================*/
#pragma once

#include <unordered_set>
#include <unordered_map>

#include "node.hpp"
#include "../../types.hpp"

//!
//! \file     base_graph.hpp

//! \brief    base_graph class
//! \version  1.0
//! \date     12 Avril 2016, Modified on the 09th of January 2017
//!
//! This class provides getter and setter methods for tree structures.

//! \namespace  sdm
//!
//! Namespace grouping all tools required for sequential decision making.
namespace sdm{

  class limited_length {
  protected:
    static number length_limit;                       //<! length limit in memory

  public:
    limited_length();

    virtual ~limited_length();

    /*!
    *  \fn number getLengthLimit()
    *  \brief Returns the length limit
    */
    static number getLengthLimit();

    /*!
    *  \fn void setLengthLimit(number)
    *  \param number
    *  \brief Sets the length limit
    */
    static void setLengthLimit(number);
  };

  /*!
  *  \class      base_graph      base_graph.hpp
  *  \typename   T type of the item.
  *  \brief      base_graph structure
  */
  template<typename T, int N = 0>
  class base_graph : public limited_length{
  protected:
    horizon depth = 0;                                //<! depth of the tree
    bool marked = false;
    node<T,N>* data = NULL;                           //<! data of the current tree
    base_graph* origin = NULL;                        //<! root of the tree
    std::unordered_map<T, base_graph*> children;      //<! mapping of items to successor trees

    template<typename output>
    output*const base_truncated_expand(const T&, bool = true);

  public:
    /*!
    *  \fn     base_graph(number=999)
    *  \brief  constructor
    *  This constructor builds a default and empty tree.
    */
    base_graph();

    /*!
    *  \fn     base_graph(base_graph*, const T&, bool = true)
    *  \brief  constructor
    *  \param  base_graph*  parent tree
    *  \param  T         item
    *  This constructor builds a tree with a given parent and item.
    */
    base_graph(base_graph*, const T&, bool = true);


    number getNumChildren() const;

    /*!
    *  \fn     ~base_graph()
    *  \brief  destructor
    *
    *  This destructor recursively all, children and the item from
    *  the tree, bottom up.
    */
    virtual ~base_graph();

    /*!
    *  \fn     base_graph<T>* expand(const T)
    *  \brief  expand the base_graph
    *  \param  const T
    *  \return base_graph*
    *
    *  If child leading from the item previously exists, the method return
    *  that child. Otherwise, it expands the tree by adding an item at the
    *  current leaf of the tree and creating if necessary a corresponding
    *  child. The constructed child is returned.
    */
    template<typename output>
    output*const base_expand(const T&, bool = true);


    /*!
    *  \fn      node* getPredecessor() const
    *  \brief  Returns the pointer onto the root tree node.
    *  \return const node*  pointer onto the root tree node.
    */
    node<T,N>*const getData() const;

    /*!
    *  \fn     base_graph*const getParent() const
    *  \brief  Returns the pointer onto the parent base_graph.
    *  \return node<T>* pointer onto the parent base_graph node.
    */
    base_graph*const getParent() const;

    /*!
    *  \fn      tree* getChild(const T) const
    *  \brief  Returns the pointer onto the child tree.
    *  \return tree* pointer onto the child tree.
    */
    base_graph*const getChild(const T&) const;

    /*!
    *  \fn     T item() const
    *  \brief  Returns the item
    *  \return const T&   item
    */
    T getItem() const;

    /*!
    *  \fn     V getHorizon() const
    *  \brief  Returns the horizon
    *  \return V : horizon
    */
    horizon getHorizon() const;

    /*!
    *  \fn     void setHorizon(V)
    *  \brief  Sets the horizon
    */
    void setHorizon(horizon);

    static std::string to_string(const T&);

    /*!
    *  \fn     friend std::ostream& operator<<(std::ostream&, const tree<T>&)
    *  \brief  Returns an ostream instance
    *  \param  std::ostream&  ostream instance
    *  \param  const tree<T>& tree of items
    *  \return std::ostream&   ostream instance
    */
    friend std::ostream& operator<<(std::ostream& os, const base_graph<T,N>& forest){
      int tabs = 0;
      for(tabs=0; tabs<forest.getHorizon(); ++tabs)  os << "\t";

      if( forest.children.empty() ){
        os << "<tree address=\"" << &forest << "\" size=\""<< forest.getNumChildren() <<"\" horizon=\"" << forest.getHorizon() << "\" />" << std::endl;
      }

      else{
        os << "<tree address=\"" << &forest << "\" size=\""<< forest.getNumChildren() <<"\"  horizon=\"" << forest.getHorizon() << "\">" << std::endl;
        for(auto child : forest.children){
          for(tabs=0; tabs<forest.getHorizon(); ++tabs)  os << "\t";
          os << " <subtree item=\"" << to_string(child.first) << "\">" << std::endl;
          os << *child.second;
          for(tabs=0; tabs<forest.getHorizon(); ++tabs)  os << "\t";
          os << " </subtree>" << std::endl;
        }

        for(tabs=0; tabs<forest.getHorizon(); ++tabs)  std::cout << "\t";
        os << "</tree>" << std::endl;
      }

      return os;
    }
  };

  /*!
  *  \class      graph<T,N>
  *  \typename   T type of the item.
  *  \brief      base_graph structure
  */
  template<typename T, int N = 0>
  class graph : public base_graph<T,N>{
  protected:
    action label;                                     //<! label (e.g., action of a policy tree) of the tree/node

    agent name = -1;                                   //<! name of the owner of the tree, if available

    std::vector<T> keys;                              //<! set of possible keys

    std::vector<action> values;                       //<! set of indexes associated with keys

  public:
    /*!
    *  \fn     graph()
    *  \brief  constructor
    *  This constructor builds a default and empty tree.
    */
    graph();


    /*!
    *  \fn     graph(const agent&)
    *  \param  agent - name of the agent
    *  \brief  constructor
    *  This constructor builds a default and empty tree.
    */
    graph(const agent&);

    /*!
    *  \fn     graph(graph*, const T&, bool = true)
    *  \brief  constructor
    *  \param  base_graph*  parent tree
    *  \param  T         item
    *  This constructor builds a tree with a given parent and item.
    */
    graph(graph*, const T&, bool = true);


    /*!
    *  \fn     graph(const std::vector<T>&, const std::vector<action>&)
    *  \brief  constructor
    *  \param  const std::vector<T>&  parameters of a function
    *  \param  const std::vector<action>& values of that same function
    *  This constructor builds a parametric graph using parameters and corresponding values.
    */
    graph(const std::vector<T>&, const std::vector<action>&);

    /*!
    *  \fn     ~graph()
    *  \brief  destructor
    *
    *  This destructor recursively all, children and the item from
    *  the tree, bottom up.
    */
    virtual ~graph();

    /*!
    *  \fn     graph<T,N>* expand(const T)
    *  \brief  expand the base_graph
    *  \param  const T
    *  \return base_graph*
    *
    *  If child leading from the item previously exists, the method return
    *  that child. Otherwise, it expands the tree by adding an item at the
    *  current leaf of the tree and creating if necessary a corresponding
    *  child. The constructed child is returned.
    */
    graph*const expand(const T&, bool = true);


    /*!
    *  \fn agent getAgent()
    *  \brief Returns the agent
    */
    agent getAgent() const;

    /*!
    *  \fn void setAgent(const agent&)
    *  \param agent
    *  \brief Sets the agent
    */
    void setAgent(const agent&);

    /*!
    *  \fn      action getLabel() const
    *  \brief  Returns the label of the tree.
    *  \return action is the  label of the tree.
    */
    action getLabel() const;

    /*!
    *  \fn      void setLabel(const action&)
    *  \brief  Sets the label of the tree.
    */
    void setLabel(const action&);

    void setParams(const std::vector<T>&, const std::vector<action>&);

    const std::vector<T>& getKeys() const;

    const std::vector<action>& getValues() const;

    /*!
    *  \fn     void initTree(const std::vector<base_graph*>&, const horizon&);
    *  \param  const std::vector<base_graph*>& subgraphs
    *  \param  const horizon& planning horizon
    *  \brief  Initializes the parameters of a graph
    */
    void initTree(const std::vector<graph*>&, const horizon&);

    /*!
    *  \fn     friend std::ostream& operator<<(std::ostream&, const observation_ihistory&)
    *  \brief  Returns an ostream instance
    *  \param  std::ostream&  ostream instance
    *  \param  const graph<T,N>& tree of individual graph
    *  \return std::ostream&   ostream instance
    *
    *  This method allows the individual history instances to be printed
    *  using an XML style, e.g., for the dpomdp-tiger problem, we'll have
    *  <individual-history children="1"  horizon="3">
    *    <item id="1" history="hear-left:hear-left::hear-left"/>
    *  </individual-history>
    *  This method requires \@dpomdp havind a semantic associated with each
    *  observation id. If no semantic is available, it print the id.
    */
    friend std::ostream& operator<<(std::ostream& os, const graph<T,N>& ih){
      os << "(agent:" << ih.getAgent() <<  ")";

      if(ih.getData() == NULL) return os;

      os << *ih.getData();
      return os;
    }
  };

  /*!
  *  \class      graph<T,1>
  *  \typename   T type of the item.
  *  \brief      base_graph structure
  */
  template<typename T>
  class graph<T,1> : public base_graph<T,1>, public std::unordered_map<agent, graph<T>*>{
  protected:

  public:
    /*!
    *  \fn     graph()
    *  \brief  constructor
    *  This constructor builds a default and empty tree.
    */
    graph();

    /*!
    *  \fn     base_graph(base_graph*, const T&, bool = true)
    *  \brief  constructor
    *  \param  base_graph*  parent tree
    *  \param  T         item
    *  This constructor builds a tree with a given parent and item.
    */
    graph(graph*, const T&, bool = true);

    graph(const std::vector<graph<T>*>&);

    /*!
    *  \fn     ~graph()
    *  \brief  destructor
    *
    *  This destructor recursively all, children and the item from
    *  the tree, bottom up.
    */
    virtual ~graph();

    /*!
    *  \fn     graph<T,1>* expand(const T)
    *  \brief  expand the graph
    *  \param  const T
    *  \return graph*
    *
    *  If child leading from the item previously exists, the method return
    *  that child. Otherwise, it expands the tree by adding an item at the
    *  current leaf of the tree and creating if necessary a corresponding
    *  child. The constructed child is returned.
    */
    graph*const expand(const T&, bool = true);

    /*!
    *  \fn     friend std::ostream& operator<<(std::ostream&, const jhistory&)
    *  \brief  Returns an ostream instance
    *  \param  std::ostream&  ostream instance
    *  \param  const jhistory& tree of joint observations
    *  \return std::ostream&   ostream instance
    *
    *  This method allows the joint history instances to be printed
    *  using an XML style, e.g., for the dpomdp-tiger problem, we'll have
    *  <joint-history children="1"  horizon="3">
    *    <agent name="0">
    *      <item id="1" history="hear-left:hear-left::hear-left"/>
    *    </agent>
    *    <agent name="1">
    *      <item id="0" history="hear-right:hear-left::hear-left"/>
    *    </agent>
    *  </graph<T,1>>
    *  This method requires \@dpomdp havind a semantic associated with each
    *  observation id. If no semantic is available, it print the id.
    */
    friend std::ostream& operator<<(std::ostream& os, const graph<T,1>& jh){
      std::set<agent> agents;
      for(auto p : jh){
        if( p.second != NULL ) agents.insert(p.first);
      }

      for(agent i : agents){
        os << *jh.at(i) << " ";
      }

      return os;
    }
  };
}

namespace std{
  template<>
  struct hash<pair<sdm::action,sdm::observation>>{
    virtual size_t operator()(const pair<sdm::action,sdm::observation>&) const;
  };

  template<>
  struct equal_to<pair<sdm::action,sdm::observation>>{
    virtual bool operator()(const pair<sdm::action,sdm::observation>&, const pair<sdm::action,sdm::observation>&) const;
  };
}
