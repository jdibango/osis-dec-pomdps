#pragma once

#include <memory>

#include "../../types.hpp"
#include "../../common.hpp"
#include "../../dpomdp/dpomdp.hpp"

//!
//! \file     light_decision_rule.hpp

//! \brief    light_decision_rule class
//! \version  1.0
//! \date     18 October 2019
//!
//! This class provides getter and setter methods for light_decision_rule

namespace sdm{

  class light_decision_rule{
  private:
    action u0;

  public:
    /*!
     * /first the belief
     * /second action and probability
     */
    std::unordered_map<std::shared_ptr<Vector>,std::unordered_map<action, double>> container;

    /*!
     * Constructor
     * empty
     */
    light_decision_rule();


    /*!
     * Constructor
     * Initialize with a decision rule
     */
    light_decision_rule(const std::vector<std::shared_ptr<Vector>>&, const std::vector<action>&);

    /*!
     * Constructor
     * Initialize with a decision rule
     */
    light_decision_rule(std::unordered_map<std::shared_ptr<Vector>, std::unordered_map<action, double>>&);


    /*!
     * Deconstructor
     *
     */
    ~light_decision_rule();

    number size() const;

    void setAction(action);

    action getAction() const;

    double getValueAt(const std::shared_ptr<Vector>&, action) const;

    void setValueAt(const std::shared_ptr<Vector>&, action, double);
  };

  std::ostream& operator<<(std::ostream&, const light_decision_rule&);

}
