/*=============================================================================

==============================================================================*/
#pragma once

#include <functional>

#include "../types.hpp"
#include "linear_algebra/vector.hpp"

//!
//! \file     equal.hpp

//! \brief    equal class
//! \version  1.0
//! \date     08 May 2016
//!
//! This class provides basic type alias for dpomdp.
//!

//! \namespace  sdm
//!
//! Namespace grouping all tools required for sequential decision making.
namespace sdm{
  template<typename T>
  struct equal: public std::binary_function<T, T, bool> {
      bool operator()(const T& left, const T& right) const;
  };

  template<typename T>
  struct equal<T*>: public std::binary_function<T*, T*, bool> {
      bool operator()(const T*& left, const T*& right) const;
  };

  template<typename T>
  struct equal<std::shared_ptr<T>>: public std::binary_function<std::shared_ptr<T>, std::shared_ptr<T>, bool> {
      bool operator()(const std::shared_ptr<T>& left, const std::shared_ptr<T>& right) const;
  };
}
