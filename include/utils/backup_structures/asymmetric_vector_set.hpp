/*==============================================================================================

 ===============================================================================================*/

#pragma once

#include "../linear_algebra/vector_container.hpp"
#include "../information_states/occupancy_state/light_occupancy_state.hpp"

//!
//! \file asymmetric_vector_set.hpp

//! \version 1.0
//! \date 22 January 2020
//!
//! This class provides getter and setter methods for the vector sets

//! \namespace sdm
//!
//! Namespace grouping all tools required for sequential decision making.
namespace sdm {
  template<typename value_t, typename ihistory_t = observation_ihistory>
  class asymmetric_vector_set{
  private:
    //<! type belief_t
    using belief_t = std::shared_ptr<Vector>;

    //<! v_default value
    std::unordered_map<horizon, value_t> v_default;

    //<! type beta_t
    using beta_t = std::unordered_map<belief_t, value_t, hash_container, equal_container>;

    //<! type gamma_t
    using gamma_t = std::unordered_map<observation, beta_t>;

    //<! the container of the value function for all horizons
    std::unordered_map<horizon, std::vector<gamma_t>> container;

  protected:

    void pruning();

    value_t add(const value_t&, const value_t&);

    double getBeliefValueAt(const belief_t&, const value_t&);

    double getBeliefValueAt(const belief_t&, const beta_t&, horizon);

    beta_t add(const std::shared_ptr<light_occupancy_state<ihistory_t>>&, const gamma_t&);

    value_t getBeliefValueAt(const belief_t&, action, observation, const gamma_t&, horizon);

    value_t getBeliefValueAt(const belief_t&, observation, const gamma_t&, double&, horizon);

    double getVectorValueAt(const std::shared_ptr<light_occupancy_state<ihistory_t>>&, const value_t&);

    double getValueAt(const std::shared_ptr<light_occupancy_state<ihistory_t>>&, const beta_t&, horizon);

  public:

    //<! constructor
    asymmetric_vector_set();

    //<! destructor
    ~asymmetric_vector_set();

    number size() const;

    number size(horizon) const;

    void initialize();

    //<! this method sets the value at the given occupancy state at the given horizon
    void setValueAt(const std::shared_ptr<light_occupancy_state<ihistory_t>>&, horizon);

    //<! this method returns the value at the given occupancy state at the given horizon
    double getValueAt(const std::shared_ptr<light_occupancy_state<ihistory_t>>&, horizon);

    //<! this method returns a greedy decision rule given the horizon and the occupancy state
    std::shared_ptr<light_decision_rule> getGreedyDecisionAt(const std::shared_ptr<light_occupancy_state<ihistory_t>>&, beta_t&, horizon);

    friend std::ostream& operator<<(std::ostream &os, const asymmetric_vector_set &lb) {
      os << "<asymmetric_vector_set size=\'" << lb.size() << "\' >" << std::endl;
      for(horizon h=0; h<=common::model->getPlanningHorizon(); ++h){//Bigbag
        os << "\t" << "<horizon id=\'" << h  << "\'>" << std::endl;
        for(number gamma = 0; gamma < lb.container.at(h).size(); ++gamma){//gamma
          os << "\t\t" << "<gamma id=\'" << gamma  << "\'>" << std::endl;
          for(const auto& z_beta : lb.container.at(h).at(gamma)){
              os << "\t\t\t" << "<beta z=\'" << z_beta.first  << "\'>"<< std:: endl;
              for(const auto& bv : z_beta.second){
                os << "\t\t\t\t" << "<entry belief=\'" << *bv.first  << "\' value="<< bv.second <<" />"<< std:: endl;
              }
              os << "\t\t\t" << "</beta>"<< std:: endl;
          }
          os << "\t\t" << "</gamma>" << std::endl;
        }
        os << "\t" << "</horizon>" << std::endl;
      }
      os << "</asymmetric_vector_set>" << std::endl;

      return os;
    }
  };
}
