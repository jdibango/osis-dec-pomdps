/*==============================================================================================

 ===============================================================================================*/

#pragma once

#include "../linear_algebra/vector_container.hpp"
#include "../information_states/occupancy_state/light_occupancy_state.hpp"

//!
//! \file asymmetric_point_set.hpp

//! \version 1.0
//! \date 26 January 2020
//!
//! This class provides getter and setter methods for the point sets

//! \namespace sdm
//!
//! Namespace grouping all tools required for sequential decision making.
namespace sdm {
  class asymmetric_point_set{
  private:
    //<! type belief_t
    using belief_t = std::shared_ptr<Vector>;

    //<! type belief_t
    using value_t = std::unordered_map<belief_t, double>;

    //<! v_default value
    std::unordered_map<horizon, std::shared_ptr<Vector>> v_default;

    //<! type beta_t : belief -> value
    using beta_t = std::unordered_map<belief_t, std::pair<double, double>, hash_container, equal_container>;

    //<! type theta_t : observation -> { belief -> value_t }
    using theta_t = std::unordered_map<observation, value_t>;

    //<! type gamma_t : action -> { beta_t : belief -> value }
    using gamma_t = std::unordered_map<action, beta_t>;

    //<! the container of the value function for all horizons
    std::unordered_map<horizon, std::unordered_map<observation_ihistory*, gamma_t>> container;

  protected:

    void pruning();

    /**
     * \brief : this method computes the value associated to the current belief and action pair given the next-step value function.
    */
    double getValueAt(const belief_t&, action, const theta_t&) const;

    /**
     * \brief : this method computes the value associated to the current belief and joint action
    */
    double getValueAt(observation_ihistory*, const belief_t&, action, horizon) const;

    /**
     * \brief : this method computes the saw-tooth approximation value of a belief given a belief-value point.
    */
    double sawtooth(const belief_t&, const belief_t, const std::pair<double, double>&, horizon) const;

    /**
     * \brief : this method computes the value function associated to the next belief-occupancy state, i.e., mapping from every observation and next belief to real.
    */
    theta_t getValueFunctionAt(const std::shared_ptr<light_occupancy_state<observation_ihistory>>&, const std::shared_ptr<light_decision_rule>&, horizon);

  public:

    //<! constructor
    asymmetric_point_set();

    //<! destructor
    ~asymmetric_point_set();

    number size() const;

    number size(horizon) const;

    void initialize();

    //<! this method returns the value at the given occupancy state at the given horizon
    double getValueAt(const std::shared_ptr<light_occupancy_state<observation_ihistory>>&, horizon);

    //<! this method sets the value at the given occupancy state at the given horizon
    void setValueAt(const std::shared_ptr<light_occupancy_state<observation_ihistory>>&, const std::shared_ptr<light_decision_rule>&, horizon);

    //<! this method returns a greedy decision rule given the horizon and the occupancy state
    std::shared_ptr<light_decision_rule> getGreedyDecisionAt(const std::shared_ptr<light_occupancy_state<observation_ihistory>>&, double&, horizon);

    friend std::ostream& operator<<(std::ostream &os, const asymmetric_point_set &vf) {
      os << "<asymmetric_point_set size=\'" << vf.size() << "\' >" << std::endl;
      for(horizon h=0; h<=common::model->getPlanningHorizon(); ++h){//Bigbag
        os << "\t" << "<horizon id=\'" << h  << "\'>" << std::endl;
        for(const auto& ih : vf.container.at(h))
        for(action u=0; u<common::model->getNumActions(); ++u){//action
          if(vf.container.at(h).at(ih.first).find(u) != vf.container.at(h).at(ih.first).end()){
            os << "\t\t" << "<gamma ih0=\'" << *ih.first << "\' action=\'" << u << "\'>" << std::endl;
            os << "\t\t\t" << "<beta size=\'" << ih.second.at(u).size() << "\' action=\'" << u << "\'>" << std::endl;
            for(const auto& bv : ih.second.at(u)){ // belief -> value_t
              os << "\t\t\t" << "<entry belief=\'" << *bv.first  << "\' value=\'" << bv.second.first << "\' mdp=\'" << bv.second.second << "\'/>"<< std:: endl;
            }
            os << "\t\t\t" << "</beta>" << std::endl;
            os << "\t\t" << "</gamma>" << std::endl;
          }
        }
        os << "\t" << "</horizon>" << std::endl;
      }
      os << "</asymmetric_point_set>" << std::endl;

      return os;
    }
  };
}
