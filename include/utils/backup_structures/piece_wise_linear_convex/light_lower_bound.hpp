//! \date  11 October 2019


#pragma once

/**

*/

#include <tuple>
#include <string>
#include <vector>
#include <stdlib.h>
#include <algorithm>
#include <unordered_set>
#include <ilcplex/ilocplex.h>

#include "../../information_states/occupancy_state/light_occupancy_state.hpp"
#include "light_upper_bound.hpp"

/**

*/
//!
//! \file     light_lower_bound.hpp

//! \brief    light_lower_bound class
//! \version  1.0
//! \date     10 October 2019
//!
//! This class provides getter and setter methods for light_lower_bound

namespace sdm{
  typedef std::tuple<std::shared_ptr<Vector>, action, observation> light_lower_bound_key_t;

  struct light_lower_bound_key_hash : public std::unary_function<light_lower_bound_key_t, std::size_t>{
      std::size_t operator()(const light_lower_bound_key_t& key) const{
        return std::get<1>(key) ^ std::get<2>(key) ^ std::hash<Vector>()(*std::get<0>(key));
      }
  };

  struct light_lower_bound_key_equal : public std::binary_function<light_lower_bound_key_t, light_lower_bound_key_t, bool>{
    bool operator()(const light_lower_bound_key_t& v0, const light_lower_bound_key_t& v1) const{
      return (
          std::get<0>(v0) == std::get<0>(v1) &&
          std::get<1>(v0) == std::get<1>(v1) &&
          std::get<2>(v0) == std::get<2>(v1)
      );
    }
  };

  typedef std::unordered_map<light_lower_bound_key_t, std::shared_ptr<Vector>, light_lower_bound_key_hash, light_lower_bound_key_equal> light_lower_bound_map_t;


  template<typename ihistory_t, number optimizer>
  class light_lower_bound{
  private:
    /*
     * A bag of alpha-vector sets
     */
    std::unordered_map<horizon, std::vector<std::vector<std::shared_ptr<Vector>>>> container;

    light_lower_bound_map_t alpha_container;

    bool is_finite = true;

  protected:

    /*!
     * \fn bool isDominatedPointWiseBy
     * \param const std::shared_ptr<Vector>& first alpha vector
     * \param const std::shared_ptr<Vector>& second alpha vector
     * \detail return whether or not first alpha-vector is dominated by the second, comparison pointwise
     */
    bool isDominatedPointWiseBy(const std::shared_ptr<Vector>&, const std::shared_ptr<Vector>&);

    /*!
     * \fn bool isDominatedBagWiseBy
     * \param const std::shared_ptr<Vector>& an alpha vector
     * \param const std::shared_ptr<Vector>& a set of alpha vectors
     * \detail return whether or not an alpha-vector is dominated by another alpha-vector, member of a set of alpha-vectors
     */
    bool isDominatedBagWiseBy(const std::shared_ptr<Vector>&, const std::vector<std::shared_ptr<Vector>>&);

    /*!
     * \fn bool isDominatedBy
     * \param const std::shared_ptr<Vector>& a set of alpha vectors
     * \param const std::shared_ptr<Vector>& another set of alpha vectors
     * \detail return whether or not the first set is dominated by the other one, i.e., checking whether at all alpha-vectors in the first bag are dominated by the second bag
     */
    bool isDominatedBy(const std::vector<std::shared_ptr<Vector>>&, const std::vector<std::shared_ptr<Vector>>&);


    /*!
     * \fn storeAlphaVector   --- (first step)
     * \param number identifier of the bag of alpha-vectors at step (horizon+1)
     * \param number identifier of the alpha-vector within the bag of alpha-vectors at step (horizon+1)
     * \param action joint action of the team
     * \param observation joint observation of the team
     * \param horizon defines the step corresponding to the alpha-vector that we want to build
     * /return stores the resulted alpha-vector into a map from ids to alpha-vectors, the map should be an attribute of the class.
     * /details  nalpha(x) = \sum_y T(x,u,y) * O(u,y,z) * \alpha_vector_j_k(y)
     */
    void storeAlphaVector(number, number, action, observation, horizon);

    void getGreedyDecisionAt(std::shared_ptr<light_occupancy_state<ihistory_t>>&, horizon);

    // void getDebugging(std::shared_ptr<light_occupancy_state<ihistory_t>>&, horizon);

    std::shared_ptr<light_decision_rule> setSolution(std::shared_ptr<light_occupancy_state<ihistory_t>>&, const IloCplex&, const IloNumVarArray&);

    void setValueAt(std::shared_ptr<light_occupancy_state<ihistory_t>>&, std::pair<action, std::shared_ptr<light_decision_rule>>, horizon);

    /*!
     * \fn getArgMaxAlphaVector
     * \param belief : state distribution of probabilities
     * \param joint action
     * \param joint observation
     * \param number identifier of the bag of alpha-vectors at step (horizon+1)
     * \param horizon defines the step corresponding to the alpha-vector that we want to build
     * /return  alpha-vector
     * /details  r_alpha(x) =  \argmax_{\alpha(k,u,z,j)} <b, \alpha(k,u,z,j)>
     */
    std::shared_ptr<Vector> getArgMaxAlphaVector(const std::shared_ptr<Vector>&, action, observation, number, horizon);

    double  getValueAt(const std::shared_ptr<Vector>&, const std::vector<std::shared_ptr<Vector>>&);

  public:
    /*!
     * Constructor
     */
    light_lower_bound();

    /*!
     * Deconstructor
     */
    ~light_lower_bound();

    /*!
     * \fn intialize
     * \param horizon
     */
    void initialize(bool=true);

    number size() const;
    number size(horizon) const;

    /*!
     * \fn getValueAt
     * \param light_occupancy_state<ihistory_t>
     * \param horizon
     */
    double getValueAt(const std::shared_ptr<light_occupancy_state<ihistory_t>>&, horizon);


    number getBestBagAt(std::shared_ptr<light_occupancy_state<ihistory_t>>&, action, std::shared_ptr<light_decision_rule>&, observation, horizon);


    /*!
     * \fn setValueAt
     * \param light_occupancy_state<ihistory_t>
     * \param horizon
     */
    void setValueAt(std::shared_ptr<light_occupancy_state<ihistory_t>>&, double, horizon);

    void pruning(horizon);


    void deleteAlphaVector(number, number, action, observation, horizon);

    void deleteAllAlphaVectors(number, horizon);

    friend std::ostream& operator<<(std::ostream &os, const light_lower_bound &llb) {
      os << "<light_lower_bound>" << std::endl;
      int size = 0;
      for(horizon h=0; h<=common::model->getPlanningHorizon(); ++h){//Bigbag
        os << "\t" << "<horizon id=\'" << h  << "\'>" << std::endl;
        for(number bag = 0; bag < llb.container.at(h).size(); ++bag){//bag
          os << "\t\t" << "<bag id=\'" << bag  << "\'>" << std::endl;
          for(number i=0; i<llb.container.at(h).at(bag).size(); ++i){
              os << "\t\t\t" << "<alpha-vector id=\'" << i  << "\' values=" << *llb.container.at(h).at(bag).at(i) << "/>"<< std:: endl;
              size++;
          }
          os << "\t\t" << "</bag>" << std::endl;
        }
        os << "\t" << "</horizon>" << std::endl;
      }
      os << "</light_lower_bound size=" << size << ">" << std::endl;

      os << "<alpha-vector-container size=" << llb.alpha_container.size() << ">" << std::endl;
      for(auto& alpha : llb.alpha_container){
       os << "\t" << "<alpha-vector values=" << *alpha.second << "/>"<< std:: endl;
      }
      os << "</alpha-vector-container>" << std::endl;
      return os;
    }
  };
}
