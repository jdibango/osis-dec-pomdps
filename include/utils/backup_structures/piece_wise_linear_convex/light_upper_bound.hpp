//! \date  October 2019

#pragma once

/**

*/
#include <set>
#include <limits>
#include <string>
#include <algorithm>

#include <tuple>
#include <string>
#include <stdlib.h>
#include <algorithm>

#include <vector>
#include <memory>
#include <ilcplex/ilocplex.h>

#include "../../information_states/occupancy_state/light_occupancy_state.hpp"
#include "../../../algorithms/mdps/search/asymmetric_hsvi.hpp"
#include "../../decision_rules/light_decision_rule.hpp"
#include "../../decision_rules/variations.hpp"


//!
//! \file     light_upper_bound.hpp

//! \brief    light_upper_bound class
//! \version  1.0
//! \date     10 October 2019
//!
//! This class provides getter and setter methods for light_upper_bound
namespace sdm{
  typedef std::vector<std::tuple<std::shared_ptr<Vector>, double, double, double>> ubag_t;

  template<typename ihistory_t, number optimizer, bool pomdp=false>
  class light_upper_bound{
  private:
    std::vector<std::unordered_map<std::shared_ptr<light_occupancy_state<ihistory_t>>, std::pair<double, double>>> point_set;

    /*
     * A bag of belief-value sets
     */
    std::unordered_map<horizon, std::vector<ubag_t>> container;

    /*
     * (in)finite-horizon MDP value function
     */
    std::unordered_map<horizon, std::shared_ptr<Vector>> mdp_vf;


    /*
     * (in)finite-horizon POMDP value function
     */
    std::shared_ptr<asymmetric_hsvi<ihistory_t, double, std::shared_ptr<asymmetric_belief_state<ihistory_t, false>>, action, false, false>> pomdp_vf = nullptr;

    bool is_finite = true;

  protected:
    /*!
    *  \fn getSawtooth
    *  \param const std::shared_ptr<light_occupancy_state<ihistory_t>>&
    *  \param const std::pair<std::shared_ptr<light_occupancy_state<ihistory_t>>, std::pair<double, double>>&
    *  \brief this method computes the relative upper-bound value of a light_occupancy_state<ihistory_t> given a reference non-corner point.
    */
    double getSawtooth( const std::shared_ptr<light_occupancy_state<ihistory_t>>&, const std::pair<std::shared_ptr<light_occupancy_state<ihistory_t>>, std::pair<double, double>>& );

    /*!
    *  \fn getMarkovDecisionProcessValueAt
    *  \param const std::shared_ptr<light_occupancy_state<ihistory_t>>&
    *  \param horizon
    *  \brief this method computes the mdp value for a given light occupancy state.
    */
    double getMarkovDecisionProcessValueAt(const std::shared_ptr<light_occupancy_state<ihistory_t>>&, horizon) const;

    double getMarkovDecisionProcessValueAt(const std::shared_ptr<Vector>&, horizon) const;

    /*!
     * \fn setSolution
     * \param light occupancy state
     * \param horizon
     */
    std::shared_ptr<light_decision_rule> setSolution(std::shared_ptr<light_occupancy_state<ihistory_t>>&, const IloCplex& cplex, const IloNumVarArray& var);


    void getMarkovDecisionProcessFiniteValueFunction();

    void getMarkovDecisionProcessInfiniteValueFunction();

    void getPartiallyObservableMarkovDecisionProcessFiniteValueFunction(number, number);

  public:
    /*!
     * Constructor
     */
    light_upper_bound();

    /*!
     * Deconstructor
     */
    ~light_upper_bound();

    /*!
     * \fn intialize
     * \param planning_horizon
     */
     void initialize(number, number, bool=true);

    /*!
     * \fn getValueAt
     * \param light occupancy state
     * \param horizon
     */
    double getValueAt(const std::shared_ptr<light_occupancy_state<ihistory_t>>& s, horizon) const;


    /*!
     *  \fn getGreedyDecisionAt
     *  \param light occupancy state
     *  \param double
     *  \param horizon
     */
    std::pair<action, std::shared_ptr<light_decision_rule>> getGreedyDecisionAt(std::shared_ptr<light_occupancy_state<ihistory_t>>&, double&, double, horizon);


    /*!
     * \fn setValueAt
     * \param occupancy_state_light
     * \param value
     * \param horizon
     * \return std::pair<action, std::shared_ptr<light_decision_rule>>
     */
    std::pair<action, std::shared_ptr<light_decision_rule>> setValueAt(std::shared_ptr<light_occupancy_state<ihistory_t>>&, double, horizon);


    /*!
     * \fn size
     * \param horizon
     */
    number size(horizon) const;

    number size() const;

    /*!
    * \fn pruning
    * \brief this method prunes dominated non-corner points, known as bounded pruning by Trey Smith.
    */
    void pruning(horizon);

    /*!
    * \fn isDominated
    * \brief this method checks if the first is dominated by the second one
    */
    bool isDominated(const std::pair<std::shared_ptr<light_occupancy_state<ihistory_t>>, std::pair<double, double>>&, const std::pair<std::shared_ptr<light_occupancy_state<ihistory_t>>, std::pair<double, double>>&);

    friend std::ostream& operator<<(std::ostream &os, const light_upper_bound &llb) {
      int size = 0;
      os << "<light_upper_bound>" << std::endl;
      for(horizon h=0; h<=common::model->getPlanningHorizon(); ++h){
        os << "\t" << "<horizon id=\'" << h  << "\'>" << std::endl;
        for(const auto& sv : llb.point_set.at(h)){
          os << "\t\t" << "<point @=\'" << sv.first  << "\' ub=\'" << sv.second.first << "\' mdp=\'" << sv.second.second  << "\' />"<< std::endl;
          os << *sv.first;
        }
        os << "\t" << "</horizon>" << std::endl;
      }

      os << "</light_upper_bound size=" << size << ">" << std::endl;
      os << "<mdp planning-horizon=\'" << common::model->getPlanningHorizon() << "\'>"<<std:: endl;
      for(horizon h=0; h<=common::model->getPlanningHorizon(); ++h)
        os << "\t" << "<horizon=\'" << h << "\'\t vector=\'" << *llb.mdp_vf.at(h) <<"\' />"<<std:: endl;
      os << "</mdp>"<<std:: endl;

      return os;
    }
  };
}
