/*=============================================================================

==============================================================================*/
#pragma once

#include <cmath>
#include <memory>
#include <vector>
#include <functional>
#include <unordered_set>

#include "vector.hpp"
#include "../../types.hpp"
#include "boost/functional/hash/hash.hpp"

namespace sdm{
  struct equal_belief_occupancy{
    virtual bool operator()(const std::pair<std::shared_ptr<Vector>, std::shared_ptr<Vector>>&, const std::pair<std::shared_ptr<Vector>, std::shared_ptr<Vector>>&) const;
  };

  struct hash_belief_occupancy{
    virtual size_t operator()(const std::pair<std::shared_ptr<Vector>, std::shared_ptr<Vector>>&) const;
  };


  struct equal_container{
    virtual bool operator()(const std::shared_ptr<Vector>&, const std::shared_ptr<Vector>&) const;
  };

  struct hash_container{
    virtual size_t operator()(const std::shared_ptr<Vector>&) const;
  };

  struct vector_container : public std::unordered_set<std::shared_ptr<Vector>, hash_container, equal_container> {
    vector_container();
    bool contains(std::shared_ptr<Vector>&);

    /*!
    *  \fn     friend std::ostream& operator<<(std::ostream&, const vector_container&)
    *  \brief  Returns an ostream instance
    *  \param  std::ostream&  ostream instance
    *  \param  const vector_container&
    *  \return std::ostream&   ostream instance
    */
    friend std::ostream& operator<<(std::ostream& os, const vector_container& container){
      os << "<vector_container size=" << container.size() << ">" << std::endl;
      for(const auto& entry : container)
        os << "\t" << "<belief values="<< *entry << " @=" << entry <<">" << std::endl;
      os << "</vector_container>" << std::endl;
      return os;
    }
  };

}

namespace std{
  template<>
  struct hash<sdm::Vector>{
    virtual size_t operator()(const sdm::Vector&) const;
  };

  template<>
  struct equal_to<sdm::Vector>{
    virtual bool operator()(const sdm::Vector&, const sdm::Vector&) const;
  };
}
