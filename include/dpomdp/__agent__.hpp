#pragma once

#include <string>

#include "../types.hpp"
#include "../public/world.hpp"

//!
//! \file     agent.hpp
//! \brief    agent class
//! \version  1.0
//!
//! This class provides getter and setter methods for agents.
//!

//! \namespace  sdm
//!
//! Namespace grouping all tools required for sequential decision making.
namespace sdm{

  //!
  //! \class  agent  agents.hpp
  //!
  class __agent__ : public World{
  protected:
      //! mapping from names (of agent, actions, observations, and states) to indexes and vice-versa.
      bimap agent_names_bimap;                    //<! list of agent names.

      //! \param number_agents          number of agents involved in the system.
      agent number_agents;

    public:

      //! \fn       agent getNumAgents() const
      //! \brief    Returns the number of agents
      //! \return   agent number
      agent getNumAgents() const;

      //! \fn       void setNumAgents(agent)
      //! \brief    Sets the number of agents
      void setNumAgents(agent);

      //! \fn       void setNumAgents(std::vector<std::string>&)
      //! \param    const std::vector<std::string>&
      //! \brief    Sets the number of agents and their corresponding names.
      void setNumAgents(const std::vector<std::string>&);

      //! \fn       agent getAgentIndex(const std::string&)
      //! \param    const std::string& a agent name
      //! \brief    Returns the index of the agent
      agent getAgentIndex(const std::string&);

      //! \fn       std::string getAgentName(agent);
      //! \param    agent  index
      //! \brief    Returns the name associated with the agent index
      std::string getAgentName(agent);
  };
}
