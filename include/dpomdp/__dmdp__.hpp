#pragma once

#include "../types.hpp"
#include "__agent__.hpp"
#include "__state__.hpp"
#include "__action__.hpp"
#include "__constraint__.hpp"
#include "__dynamics__.hpp"
#include "__observation__.hpp"
#include <unordered_map>

//!
//! \file     __dmdp__.hpp
//! \brief    __dmdp__ class
//! \version  1.0
//!
//! This class provides getter and setter methods for states.
//!

//! \namespace  sdm
//!
//! Namespace grouping all tools required for sequential decision making.
namespace sdm{

  //!
  //! \class  __dmdp__.hpp
  //!
  class __dmdp__ : public __state__, public __agent__, public __action__, public __observation__, public __constraint__, public __dynamics__{
    protected:
      //! checked;
      bool isDmdp = false;
      bool isCheckedDmdp = false;

      //! \param state2observation is a surjective function from states to joint observations
      std::unordered_map<state, observation> state2observation;

    public:

      //! \fn       bool isJointlyFullyObservable()
      //! \brief    Returns true if the dpomdp is jointly fully observable, and false otherwise.
      bool isJointlyFullyObservable();

      //! \fn       observation getObservationFromState(state);
      //! \brief    Returns observation given a corresponding state.
      observation getObservationFromState(state);
  };
}
