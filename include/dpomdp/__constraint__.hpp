#pragma once

#include <vector>

#include "../types.hpp"
#include "__reward__.hpp"
#include "../utils/linear_algebra/vector.hpp"

//!
//! \file     constraint.hpp
//! \brief    constraint class
//! \version  1.0
//!
//! This class provides getter and setter methods for the constraint model.
//!

//! \namespace  sdm
//!
//! Namespace grouping all tools required for sequential decision making.
namespace sdm{

  //!
  //! \class  __constraint__  __constraint__.hpp
  //!
  class __constraint__ : public __reward__ {
    protected:
      double bound;

    public:
      //! \fn       value getCost(state, action) const
      //! \param    state
      //! \param    action
      //! \brief    Returns cost
      //! \return   value
      double getCost(state, action);

      double getBound() const;
  };

}
