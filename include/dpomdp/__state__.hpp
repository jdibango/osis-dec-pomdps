/*=============================================================================

==============================================================================*/
#pragma once

#include <cstddef>
#include <vector>

#include <boost/bimap.hpp>

#include "../utils/decision_rules/joint.hpp"
#include "../utils/decision_rules/variations.hpp"

#include "../types.hpp"

//!
//! \file     state.hpp

//! \brief    dpomdp class
//! \version  1.0
//! \date     12 Avril 2016
//!
//! This class provides getter and setter methods for states.
//!

//! \namespace  sdm
//!
//! Namespace grouping all tools required for sequential decision making.
namespace sdm{

  //!
  //! \class  state  state.hpp
  //!
  class __state__ {
    private:

    protected:
      //! \param number_states          number of states in the system.
      state number_states;

      //! list of state names.
      bimap state_names_bimap;

    public:

      //! \fn       state getNumStates() const
      //! \brief    Returns the number of states
      //! \return   state number
      state getNumStates() const;

      //! \fn       void setNumStates(state)
      //! \brief    Sets the number of states
      void setNumStates(state);

      //! \fn       void setNumStates(const std::vector<std::string>&)
      //! \param    const std::vector<std::string>&
      //! \brief    Sets the number of states and their corresponding names.
      void setNumStates(const std::vector<std::string>&);

      //! \fn       state getStateIndex(const std::string&)
      //! \param    const std::string& a state name
      //! \brief    Returns the index of the state
      state getStateIndex(const std::string&);

      //! \fn       std::string getStateName(state)
      //! \param    state  index
      //! \brief    Returns the name associated with the state index
      std::string getStateName(state);
  };
}
