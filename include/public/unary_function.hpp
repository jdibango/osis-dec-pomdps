/*=============================================================================

==============================================================================*/
#pragma once

#include "../types.hpp"
#include "state.hpp"
#include "action.hpp"

//!
//! \file     utility_function.hpp

//! \brief    utility_function class
//! \version  1.0
//! \date     11 Avril 2016
//!
//! This class provides the utility functions' public interface.
//!

//! \namespace  sdm
//!
//! Namespace grouping all tools required for sequential decision making.
namespace sdm{

  template<typename S, typename A>
  class UnaryFunction{
  public:
    virtual ~UnaryFunction();
    virtual double getValueAt(const S&) const = 0;
    virtual double getDefaultValue(horizon=0) const = 0;
    virtual void setValueAt(const S&, const A&, double) = 0;
  };

}
