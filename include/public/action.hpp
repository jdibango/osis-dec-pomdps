/*=============================================================================

==============================================================================*/
#pragma once

#include "state.hpp"

//!
//! \file     state.hpp

//! \brief    state class
//! \version  1.0
//! \date     11 Avril 2016
//!
//! This class provides the states' public interface.
//!

//! \namespace  sdm
//!
//! Namespace grouping all tools required for sequential decision making.
namespace sdm{

  class Action{
  public:
    virtual ~Action();
    virtual std::size_t hash() const = 0;
    virtual void operator+=(const Action&);
    virtual void setObservation(observation);
    virtual bool equal_to(const std::shared_ptr<Action>&) const = 0;
  };
}

namespace std{
  template<>
  struct hash<shared_ptr<sdm::Action>>{
    virtual size_t operator()(const shared_ptr<sdm::Action> &) const;
  };

  template<>
  struct equal_to<shared_ptr<sdm::Action>>{
    virtual bool operator()(const shared_ptr<sdm::Action> &, const shared_ptr<sdm::Action> &) const;
  };
}
