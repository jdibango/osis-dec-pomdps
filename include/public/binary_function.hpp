/*=============================================================================

==============================================================================*/
#pragma once

#include "state.hpp"
#include "action.hpp"

//!
//! \file     binary_function.hpp

//! \brief    binary_function class
//! \version  1.0
//! \date     11 Avril 2016
//!
//! This class provides the binary functions' public interface.
//!

//! \namespace  sdm
//!
//! Namespace grouping all tools required for sequential decision making.
namespace sdm{

  template<typename S, typename A>
  class BinaryFunction{
  public:
    virtual ~BinaryFunction();
    virtual double getQValueAt(const S&, const A&) const = 0;
    virtual void setQValueAt(const S&, const A&, double) = 0;
  };

}
