/*=============================================================================

==============================================================================*/
#pragma once

#include "../types.hpp"
#include "feedback.hpp"

//!
//! \file     state.hpp

//! \brief    state class
//! \version  1.0
//! \date     11 Avril 2016
//!
//! This class provides the states' public interface.
//!

//! \namespace  sdm
//!
//! Namespace grouping all tools required for sequential decision making.
namespace sdm{
  class State{
  protected:
    std::shared_ptr<State> uncompressed = nullptr;

  public:
    virtual ~State();
    virtual std::size_t hash() const = 0;
    virtual horizon getHorizon() const = 0;
    virtual bool equal_to(State* const) const = 0;
    virtual std::shared_ptr<Action> endAction() = 0;
    virtual std::shared_ptr<Action> nextAction() = 0;
    virtual std::shared_ptr<Action> beginAction() = 0;
    virtual std::shared_ptr<Action> randomAction() = 0;
    const std::shared_ptr<State>& getUncompressed();
    virtual observation setObservation(std::shared_ptr<Action>&);
    virtual bool equal_to(const std::shared_ptr<State>&) const = 0;
    virtual double operator^(const std::shared_ptr<Action>&) const = 0;
    // virtual State* expand(const std::shared_ptr<Action>&, bool = true) = 0;
    void setUncompressed(const std::shared_ptr<State>&);
    virtual std::shared_ptr<State> next(const std::shared_ptr<Action>&, bool = true) = 0;
    virtual std::unordered_map<state, std::unordered_map<action, double>> getMarkovDecisionRule(const std::shared_ptr<Action>&) = 0;
    virtual double getFutureValueAt(const std::shared_ptr<Action>&, const UnaryFunction<std::shared_ptr<State>, std::shared_ptr<Action>>&, bool = true) = 0;


    /*!
    *  \fn State* const convert(State* const) const  -  convert a "state" in argument into a base provided by the current "state"
    *  \param  State* const                         -  a "state" to be converted
    *  \return  State* const                        -  a "state" that results from the convertion
    *  \brief setter for an entry of an occupancy measure
    */
    // virtual State* const convert(State* const) const = 0;
  };
}

namespace std{
  template<>
  struct hash<shared_ptr<sdm::State>>{
    virtual size_t operator()(const shared_ptr<sdm::State>& ) const;
  };

  template<>
  struct equal_to<shared_ptr<sdm::State>>{
    virtual bool operator()(const shared_ptr<sdm::State>& , const shared_ptr<sdm::State>& ) const;
  };

  template<>
  struct hash<sdm::State*>{
    virtual size_t operator()(sdm::State*const ) const;
  };

  template<>
  struct equal_to<sdm::State*>{
    virtual bool operator()(sdm::State*const , sdm::State*const ) const;
  };
}
