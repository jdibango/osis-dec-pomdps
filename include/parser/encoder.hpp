/*=============================================================================

==============================================================================*/
#pragma once

#include "ast.hpp"
#include "../dpomdp/dpomdp.hpp"

///////////////////////////////////////////////////////////////////////////////
//  AST processing
///////////////////////////////////////////////////////////////////////////////
namespace sdm {
  namespace ast {

    struct belief_encoder : boost::static_visitor<Vector> {
      number size;

      belief_encoder(const number size) : boost::static_visitor<Vector>(){
        this->size = size;
      }

      Vector operator()(const std::string& name) const {
        Vector v( size );
        if(name=="uniform") {
          for(state i=0; i<size; ++i){
            v[i] = 1.0 / size;
          }
        }

        return v;
      }

      Vector operator()(const std::vector<float>& belief) const{
        Vector v( size );
        for(state i=0; i<size; ++i) {
          v[i] = belief[i];
        }

        return v;
      }
    };

    struct matrix_encoder : boost::static_visitor<Matrix>{
      number rows, cols;

      matrix_encoder(number rows, number cols) : rows(rows), cols(cols){}

      Matrix operator()(const std::string & str) const{
        number s, s_;
        Matrix m( rows, cols );

        if( str == "uniform" ){
          for( s=0; s<rows; ++s){
            for( s_=0; s_<cols; ++s_){
              m(s,s_) = 1.0 / cols;
            }
          }
        }

        else if( str == "identity" ) {
          for( s=0; s<rows; ++s){
            for( s_=0; s_<cols; ++s_){
              m(s,s_) = s==s_ ? 1.0 : 0.0;
            }
          }
        }

        return m;
      }

      Matrix operator()(const std::vector<std::vector<float>>& v) const{
        state s, s_;
        Matrix m( rows, cols );

        for( s=0; s<rows; ++s){
          for( s_=0; s_<cols; ++s_){
            m(s,s_) = v[s][s_];
          }
        }

        return m;
      }
    };

    struct action_encoder : boost::static_visitor<action>{
      agent ag;

      action_encoder(agent ag) : boost::static_visitor<action>(){
        this->ag = ag;
      }

      action operator()(action a) const{
        return a;
      }

      action operator()(const std::string & a_str) const {
        return common::model->getActionIndex(this->ag, a_str);
      }
    };

    std::vector<action> encode_joint_action(const std::vector<identifier_t>& as) {
      std::vector<action> a_vec;
      if(as.size() == 1) {
        for(action a=0; a<common::model->getNumActions(); ++a) {
          a_vec.push_back(a);
        }
      } else {
        std::vector<action>  ja;
        for(agent ag=0; ag<common::model->getNumAgents(); ++ag){
          action_encoder a_encoder(ag);
          ja.push_back( boost::apply_visitor(a_encoder, as[ag]) );
        }
        a_vec.push_back(joint_action::getJointItemIdx(ja));

      }
      return a_vec;
    }

    struct observation_encoder : boost::static_visitor<observation>{
      agent ag;

      observation_encoder(agent ag) : boost::static_visitor<observation>(){
        this->ag = ag;
      }

      observation operator()(observation o) const{
        return o;
      }

      observation operator()(const std::string & o_str) const {
        return common::model->getObservationIndex(this->ag, o_str);
      }
    };

    std::vector<observation> encode_joint_observation(const std::vector<identifier_t>& os) {
      std::vector<observation> o_vec;
      if(os.size() == 1) {
        for(observation o=0; o<common::model->getNumObservations(); ++o) {
          o_vec.push_back(o);
        }
      } else {
        std::vector<observation> jo;
        for(agent ag=0; ag<common::model->getNumAgents(); ++ag){
          observation_encoder o_encoder(ag);
          jo.push_back( boost::apply_visitor(o_encoder, os[ag]) );
        }

        o_vec.push_back(joint_observation::getJointItemIdx(jo));
      }

      return o_vec;
    }

    struct state_encoder : boost::static_visitor<std::vector<state>>{
      std::vector<state> operator()(state s) const{
        std::vector<state> st_ptr;
        st_ptr.push_back(s);
        return st_ptr;
      }

      std::vector<state> operator()(const std::string & s_str) const {
        std::vector<state> st_ptr;
        if(s_str == "*") {
          for(state s=0; s<common::model->getNumStates(); ++s) {
            st_ptr.push_back(s);
          }
        } else {
          st_ptr.push_back(common::model->getStateIndex(s_str));
        }

        return st_ptr;
      }
    };

    struct agent_encoder : boost::static_visitor<> {
      void operator()(agent ag) const {
        common::model->setNumAgents(ag);
      }

      void operator()(const std::vector<std::string> & ags) const {
        common::model->setNumAgents(ags);
      }
    };

    struct state_space_encoder : boost::static_visitor<> {
      void operator()(state s) const {
        common::model->setNumStates(s);
      }

      void operator()(const std::vector<std::string>& ss) const {
        common::model->setNumStates(ss);
      }
    };

    struct action_space_encoder : boost::static_visitor<> {
      void operator()(const std::vector<action>& actions) const {
        common::model->setNumActions(actions);
      }

      void operator()(const std::vector<std::vector<std::string>>& actions_str) const {
        common::model->setNumActions(actions_str);
      }
    };

    struct observation_space_encoder : boost::static_visitor<> {
      void operator()(const std::vector<observation>& observations) const {
        common::model->setNumObservations(observations);
      }

      void operator()(const std::vector<std::vector<std::string>>& observations_str) const {
        common::model->setNumObservations(observations_str);
      }
    };

    struct dynamics_encoder : boost::static_visitor<>{
      std::vector<Matrix> t_model,  o_model;

      dynamics_encoder() : boost::static_visitor<>(){
        for(action a=0; a<common::model->getNumActions(); ++a){
          auto t = Matrix(common::model->getNumStates(), common::model->getNumStates());
          auto o = Matrix(common::model->getNumStates(), common::model->getNumObservations());

          for(state x=0; x<common::model->getNumStates(); ++x){
            for(state y=0; y<common::model->getNumStates(); ++y){
              t(x, y) = 0.0;
            }

            for(observation z=0; z<common::model->getNumObservations(); ++z){
              o(x, z) = 0.0;
            }
          }

          this->t_model.push_back( t );
          this->o_model.push_back( o );
        }
      }

      void operator()(const observation_entry_3_t& z3){
        std::vector<action> ja = encode_joint_action(z3.jaction);

        matrix_encoder m_encoder(common::model->getNumStates(), common::model->getNumObservations());
        Matrix prob = boost::apply_visitor(m_encoder, z3.probabilities);

        for(action a : ja){
          this->o_model[a] = prob;
        }
      }

      void operator()(const transition_entry_3_t& t3){
        std::vector<action> ja = encode_joint_action(t3.jaction);

        matrix_encoder m_encoder(common::model->getNumStates(), common::model->getNumStates());
        Matrix prob = boost::apply_visitor(m_encoder, t3.transitions);

        for(action a : ja){
          this->t_model[a] = prob;
        }
      }

      void operator()(const observation_entry_2_t& z2){
        belief_encoder bl_encoder(common::model->getNumObservations());
        Vector prob = boost::apply_visitor(bl_encoder, z2.probabilities);

        std::vector<action> ja = encode_joint_action(z2.jaction);

        state_encoder s_encoder;
        auto s_space = boost::apply_visitor(s_encoder, z2.next_state);

        for(action a : ja) for(observation o=0; o<common::model->getNumObservations(); ++o){
          for(state y : s_space){
            this->o_model[a](y, o) = prob[o];
          }
        }
      }

      void operator()(const transition_entry_2_t& t2){
        belief_encoder bl_encoder(common::model->getNumStates());
        Vector prob = boost::apply_visitor(bl_encoder, t2.probabilities);

        std::vector<action> ja = encode_joint_action(t2.jaction);

        state_encoder x_encoder;
        auto x_space = boost::apply_visitor(x_encoder, t2.current_state);

        for(action a : ja){
          for(state x : x_space){
            for(state y=0; y<common::model->getNumStates(); ++y){
              this->t_model[a](x, y) = prob[y];
            }
          }
        }
      }

      void operator()(const observation_entry_1_t& z1){
        std::vector<action> ja = encode_joint_action(z1.jaction);

        std::vector<observation> jo = encode_joint_observation(z1.next_observation);

        state_encoder s_encoder;
        auto y_space = boost::apply_visitor(s_encoder, z1.next_state);

        double prob = z1.probability;
        for(action a : ja) for(observation o : jo){
          for(state y : y_space){
            this->o_model[a](y, o) = prob;
          }
        }
      }

      void operator()(const transition_entry_1_t& t1){
        state_encoder s_encoder;
        auto y_space = boost::apply_visitor(s_encoder, t1.next_state);
        auto x_space = boost::apply_visitor(s_encoder, t1.current_state);
        std::vector<action> ja = encode_joint_action(t1.jaction);
        double prob = t1.probability;
        for(action a : ja){
          for(state x : x_space){
            for(state y : y_space){
              this->t_model[a](x, y) = prob;
            }
          }
        }
      }
    };

    struct reward_encoder : boost::static_visitor<>{
      void operator()(const reward_entry_1_t& r1) const {
        double r = r1.reward;
        state_encoder s_encoder;
        std::vector<state> s_ptr = boost::apply_visitor(s_encoder, r1.state);

        for(state s : s_ptr) {
          std::vector<action> ja = encode_joint_action(r1.jaction);
          for(action a : ja) common::model->setReward(s, a, r);
        }
      }


      void operator()(const reward_entry_2_t& r2) const {
        belief_encoder bl_encoder(common::model->getNumStates());
        Vector v = boost::apply_visitor(bl_encoder, r2.rewards);

        std::vector<action> ja = encode_joint_action(r2.jaction);
        for(action a : ja) common::model->setReward(a, v);
      }
    };


    struct preamble_encoder : boost::static_visitor<>{
      ////////////////////////////////////////////////////////////////////////////////////////////////////////////
      // Preamble encoder
      ////////////////////////////////////////////////////////////////////////////////////////////////////////////

      void operator()(preamble_t const& ast){

        common::model = std::make_shared<sdm::dpomdp>();

        agent_encoder ag_encoder;
        boost::apply_visitor(ag_encoder, ast.agent_param);

        common::model->setDiscount(ast.discount_param);

        common::model->setCriterion(ast.value_param=="reward");

        state_space_encoder st_encoder;
        boost::apply_visitor(st_encoder, ast.state_param);

        auto v = std::make_shared<Vector>( common::model->getNumStates() );
        belief_encoder bl_encoder( common::model->getNumStates() );
        *v = boost::apply_visitor(bl_encoder, ast.start_param);
        common::model->setStart(v);

        action_space_encoder a_space_encoder;
        boost::apply_visitor(a_space_encoder, ast.action_param);

        observation_space_encoder o_encoder;
        boost::apply_visitor(o_encoder, ast.observation_param);

        common::model->initReward(common::model->getNumActions(), common::model->getNumStates());
        reward_encoder r_encoder;
        for(reward_entry_t const& rew : ast.reward_spec)
        boost::apply_visitor(r_encoder, rew);

        common::model->initDynamics(common::model->getNumActions(), common::model->getNumObservations(), common::model->getNumStates());

        dynamics_encoder d_encoder;
        for(transition_entry_t const& tr : ast.transition_spec){
          boost::apply_visitor(d_encoder, tr);
        }

        for(observation_entry_t const& obs : ast.observation_spec){
          boost::apply_visitor(d_encoder, obs);
        }

        common::model->setTransitions(d_encoder.t_model);
        common::model->setObservations(d_encoder.o_model);

        for(action u=0; u<common::model->getNumActions(); ++u){
          for(state x=0; x<common::model->getNumStates(); ++x){
            double weight=0;
            for(state y=0; y<common::model->getNumStates(); ++y){
              weight += d_encoder.t_model[u](x, y);
              for(observation o=0; o<common::model->getNumObservations(); ++o){
                double prob = d_encoder.t_model[u](x, y) * d_encoder.o_model[u](y, o);
                common::model->setDynamics(x, u, o, y, prob);
              }
            }
          }
        }

        #ifdef DEBUG
          std::cout << "Model Soundness=" << (common::model->isSound()?"yes":"no") << std::endl;
          #ifdef VERBOSE
            std::cout << "Print model" << std::endl;
            std::cout << *common::model << std::endl;
          #endif
        #endif
      }


    };

  }
}
