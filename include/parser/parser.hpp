/*=============================================================================

  ==============================================================================*/
#pragma once

#include "ast.hpp"

#include <boost/spirit/home/x3.hpp>

namespace sdm{
  namespace x3 = boost::spirit::x3;

  ///////////////////////////////////////////////////////////////////////////////
  // preamble public interface
  ///////////////////////////////////////////////////////////////////////////////
  namespace parser{
    struct preamble_class;

    typedef x3::rule<preamble_class, ast::preamble_t> preamble_type;

    BOOST_SPIRIT_DECLARE(preamble_type);

    int parse_file(char const*);
  }

  parser::preamble_type const& preamble();
}
