#pragma once
#include "../../../types.hpp"

#include "../../algorithm.hpp"

#include "../../../public/state.hpp"
#include "../../../public/world.hpp"

#include "../../../utils/backup_structures/piece_wise_linear_convex/light_upper_bound.hpp"
#include "../../../utils/backup_structures/piece_wise_linear_convex/light_lower_bound.hpp"

#include <chrono>

//!
//! \file     fbhsvi_light.hpp
//! \brief    fbhsvi_light class
//! \version  1.0
//!
//! This class provides getter and setter methods for fbhsvi_light

namespace sdm{

  template<typename ihistory_t, number optimizer, bool finite = true, bool pomdp = false>
  class light_fbhsvi: public Algorithm{
  private:
    std::chrono::high_resolution_clock::time_point start_time;

    std::ofstream perf_out;
    /*!
     * /fn stop
     * /param light_occupancy_state<ihistory_t>
     * /param
     * /param horizon
     * determinate if we can stop
     */
    bool stop(const std::shared_ptr<light_occupancy_state<ihistory_t>>&, double, horizon);

    void printDebug(std::shared_ptr<light_occupancy_state<ihistory_t>>&, horizon, bool = true);

    void saveStats(std::shared_ptr<light_occupancy_state<ihistory_t>>&, horizon);

    void saveStatsBegins(std::string, bool= true);

    /*!
     * /fn greedyActionAt
     * /param light occupancy state
     * /param horizon
     */
    std::pair<action, std::shared_ptr<light_decision_rule>> greedyActionAt(std::shared_ptr<light_occupancy_state<ihistory_t>>&, horizon);

    /*!
     * /fn update
     * /param light_occupancy_state<ihistory_t>
     * /param horizon
     */
    void update(std::shared_ptr<light_occupancy_state<ihistory_t>>&, horizon);

    /*!
     * /fn getGreedyObservation
     * /param light occupancy state
     * /param action consist of action of agent 1 and decision rule of agent 2
     * /horizon
     */
    observation getGreedyObservation(std::shared_ptr<light_occupancy_state<ihistory_t>>&, std::pair<action, std::shared_ptr<light_decision_rule>>&,horizon);

    /*!
     * /fn gap
     * /param light occupancy state
     * /param action consist of action of agent 1 and decision rule of agent 2
     * /param observation
     * /param horizon
     */
    double gap(std::shared_ptr<light_occupancy_state<ihistory_t>>&, std::pair<action, std::shared_ptr<light_decision_rule>>&, observation, horizon);


    double reward(std::shared_ptr<light_occupancy_state<ihistory_t>>&, std::pair<action, std::shared_ptr<light_decision_rule>>&, horizon);


  protected:
    /*!
    * lower-bound value function
    */
    light_lower_bound<ihistory_t, optimizer> light_lb;

    /*!
    * upper-bound value function
    */
    light_upper_bound<ihistory_t, optimizer, pomdp> light_ub;

    /*!
    * initial belief state
    */
    std::shared_ptr<light_occupancy_state<ihistory_t>> start;

  public:
    /*!
    * Constructor
    *  \param number of trials
    */
    light_fbhsvi(number, number,std::string);

    /*!
    * Deconstructor
    */
    ~light_fbhsvi();

    /*!
     *  \fn explore
     *  \param const light_occupancy_state<ihistory_t>&
     *  \param error
     *  \param number is the depth
     */
    void explore(std::shared_ptr<light_occupancy_state<ihistory_t>>&, double, horizon);

    /*!
    *  \fn solve
    *  \param const std::shared_ptr<World>& is the problem to be solved.
    *  \param horizon is the planning horizon -- here this is the infinity by default.
    *  \param double is the precision, how close do we want the returned value function to be.
    *  \brief
    */
    void solve(const std::shared_ptr<World>&, horizon, double=0.001, double=1.0);
	};
}
