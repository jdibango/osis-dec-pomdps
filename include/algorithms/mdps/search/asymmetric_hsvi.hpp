#pragma once

#include <chrono>
#include <limits>

#include "../../../dpomdp/dpomdp.hpp"
#include "../../algorithm.hpp"
#include "../../../utils/linear_algebra/vector.hpp"
#include "../../../utils/information_states/occupancy_state/light_occupancy_state.hpp"

//!
//! \file     asymmetric_hsvi.hpp
//! \brief    asymmetric_hsvi class
//! \version  1.0
//!
//! This class provides the algorithm' public interface.
//!
//! \namespace  sdm
//!
//! Namespace grouping all tools required for sequential decision making.
namespace sdm{

  //+---------------------------------------------------+//
  //               asymmetric_belief_state               //
  //+---------------------------------------------------+//
  template<typename ihistory_t, bool history = false>
  class asymmetric_belief_state{
  private:
  public:
    ihistory_t* h = nullptr;
    std::shared_ptr<Vector> b;

    ~asymmetric_belief_state();
    asymmetric_belief_state();
    asymmetric_belief_state(const std::shared_ptr<Vector>&, ihistory_t*);
    asymmetric_belief_state(const std::shared_ptr<asymmetric_belief_state>&, action, observation, double&);
    ihistory_t* next(action, observation);
  };

  //+---------------------------------------------------+//
  //            equal_asymmetric_belief_state            //
  //+---------------------------------------------------+//
  template<typename ihistory_t, bool history = false>
  struct equal_asymmetric_belief_state{
    virtual bool operator()(const std::shared_ptr<asymmetric_belief_state<ihistory_t, history>>&, const std::shared_ptr<asymmetric_belief_state<ihistory_t, history>>&) const;
  };

  //+---------------------------------------------------+//
  //             hash_asymmetric_belief_state            //
  //+---------------------------------------------------+//
  template<typename ihistory_t, bool history = false>
  struct hash_asymmetric_belief_state{
    virtual size_t operator()(const std::shared_ptr<asymmetric_belief_state<ihistory_t, history>>&) const;
  };

  //+---------------------------------------------------+//
  //                asymmetric_upper_bound               //
  //+---------------------------------------------------+//
  template<typename ihistory_t, typename value_t, typename ostate_t, typename oaction_t, bool history, bool pwlc, bool pomdp>
  class asymmetric_hsvi;
  
  template<typename ihistory_t, bool history=false, bool pwlc=false, bool pomdp = false>
  class asymmetric_upper_bound{
  private:
    using mdp_t = std::shared_ptr<Vector>;
    using state_t = std::shared_ptr<asymmetric_belief_state<ihistory_t, history>>;
    using upper_bound_t = std::unordered_map<state_t, double, hash_asymmetric_belief_state<ihistory_t, history>, equal_asymmetric_belief_state<ihistory_t, history>>;
    using upper_bound_pwlc_t = std::unordered_map<ihistory_t*, upper_bound_t>;
  protected:
    std::vector<upper_bound_t> container;
    std::vector<upper_bound_pwlc_t> container_pwlc;
    std::unordered_map<horizon, mdp_t> default_value_function;

    std::shared_ptr<asymmetric_hsvi<ihistory_t, double, std::shared_ptr<asymmetric_belief_state<ihistory_t, false>>, action, false, false, false>> pomdp_vf = nullptr;

    double sawtooth(const state_t&, const state_t&, double, horizon) const;

    void getPartiallyObservableMarkovDecisionProcessFiniteValueFunction(number, number);

    void getMarkovDecisionProcessFiniteValueFunction();

    double getDefaultValue(const state_t&, horizon) const;

  public:
    asymmetric_upper_bound(number, number);
    ~asymmetric_upper_bound();
    number size() const;
    number size(horizon) const;
    void setValueAt(const upper_bound_t&, horizon);
    void setValueAt(const state_t&, double, horizon);
    double getValueAt(const state_t&, horizon) const;
    action greedyActionAt(const state_t&, horizon);
    double getValueAt(const std::shared_ptr<light_occupancy_state<ihistory_t>>&, horizon);
    std::shared_ptr<light_decision_rule> greedyActionAt(const std::shared_ptr<light_occupancy_state<ihistory_t>>&, horizon);

    friend std::ostream& operator<<(std::ostream &os, const asymmetric_upper_bound &vf) {
      os << "<asymmetric_upper_bound size=\'" << vf.size() << "\' >" << std::endl;
      for(horizon h=0; h<=common::model->getPlanningHorizon(); ++h){
        os << "\t" << "<horizon id=\'" << h  << "\'>" << std::endl;
        for(number gamma = 0; gamma < vf.size(h); ++gamma){//gamma
          os << "\t\t" << "<gamma id=\'" << gamma  << "\'>" << std::endl;
          if( pwlc ){
            for(const auto& sv : vf.container_pwlc.at(h)){
              os << "\t\t\t" << "<beta history=\'" << *sv.first  << "\'>" << std::endl;
              for(const auto& hmap : sv.second){
                os << "\t\t\t\t" << "<entry belief=\'" << *hmap.first->b  << "\' value=\'"<< hmap.second <<"\' />" << std:: endl;
              }
              os << "\t\t\t" << "</beta>" << std::endl;
            }
          }
          else{
            for(const auto& sv : vf.container.at(h)){
              os << "\t\t\t" << "<entry history=\'" << *sv.first->h  << "\' belief=\'" << *sv.first->b  << "\' value=\'"<< sv.second <<"\' />" << std:: endl;
            }
          }
          os << "\t\t" << "</gamma>" << std::endl;
        }
        os << "\t" << "</horizon>" << std::endl;
      }
      os << "</asymmetric_upper_bound>" << std::endl;

      return os;
    }
  };

  //+---------------------------------------------------+//
  //                asymmetric_lower_bound               //
  //+---------------------------------------------------+//
  template<typename ihistory_t, typename value_t, bool history=false, bool pwlc = false>
  class asymmetric_lower_bound{
  private:
    using state_t = std::shared_ptr<asymmetric_belief_state<ihistory_t, history>>;
    using lower_bound_t = std::unordered_map<state_t, value_t, hash_asymmetric_belief_state<ihistory_t, history>, equal_asymmetric_belief_state<ihistory_t, history>>;
    using lower_bound_pwlc_t = std::unordered_map<ihistory_t*, lower_bound_t>;

  protected:
    std::vector<lower_bound_t> container;
    std::vector<value_t> default_value_function;
    std::vector<lower_bound_pwlc_t> container_pwlc;
    std::shared_ptr<asymmetric_belief_state<ihistory_t>> start;

    void pruning(horizon);

  public:
    asymmetric_lower_bound();
    ~asymmetric_lower_bound();
    number size() const;
    number size(horizon) const;
    value_t getDefault(horizon);

    void setValueAt(const state_t&, horizon);
    void setValueAt(const state_t&, value_t&, horizon);
    double getValueAt(const state_t&, value_t&, horizon) const;
    action greedyActionAt(const state_t&, value_t&, horizon);

    void setValueAt(const std::shared_ptr<light_occupancy_state<ihistory_t>>&, horizon);
    double getValueAt(const std::shared_ptr<light_occupancy_state<ihistory_t>>&, lower_bound_t&, horizon) const;
    std::shared_ptr<light_decision_rule> greedyActionAt(const std::shared_ptr<light_occupancy_state<ihistory_t>>&, lower_bound_t&, horizon);

    friend std::ostream& operator<<(std::ostream &os, const asymmetric_lower_bound &vf) {
      os << "<asymmetric_lower_bound size=\'" << vf.size() << "\' >" << std::endl;
      for(horizon h=0; h<=common::model->getPlanningHorizon(); ++h){
        os << "\t" << "<horizon id=\'" << h  << "\'>" << std::endl;
        for(number gamma = 0; gamma < vf.size(h); ++gamma){//gamma
          os << "\t\t" << "<gamma id=\'" << gamma  << "\'>" << std::endl;
          if( pwlc ){
            for(const auto& sv : vf.container_pwlc.at(h)){
              os << "\t\t\t" << "<beta history=\'" << *sv.first  << "\'>" << std::endl;
              for(const auto& hmap : sv.second){
                os << "\t\t\t\t" << "<entry belief=\'" << *hmap.first->b  << "\' value=\'"<< hmap.second <<"\' />" << std:: endl;
              }
              os << "\t\t\t" << "</beta>" << std::endl;
            }
          }
          else{
            for(const auto& sv : vf.container.at(h)){
              os << "\t\t\t" << "<entry history=\'" << *sv.first->h  << "\' belief=\'" << *sv.first->b  << "\' value=\'"<< sv.second <<"\' />" << std:: endl;
            }
          }
          os << "\t\t" << "</gamma>" << std::endl;
        }
        os << "\t" << "</horizon>" << std::endl;
      }
      os << "</asymmetric_lower_bound>" << std::endl;

      return os;
    }
  };

  //+---------------------------------------------------+//
  //                    asymmetric_hsvi                  //
  //+---------------------------------------------------+//
  template<typename ihistory_t, typename value_t, typename ostate_t, typename oaction_t, bool history=false, bool pwlc=false, bool pomdp = false>
  class asymmetric_hsvi : public Algorithm{
  private:
    using state_t = std::shared_ptr<asymmetric_belief_state<ihistory_t, history>>;
    std::chrono::high_resolution_clock::time_point start_time;
    std::ofstream perf_out;
    ostate_t start;

  protected:
    std::shared_ptr<asymmetric_upper_bound<ihistory_t, history, pwlc, pomdp>> ub;
    std::shared_ptr<asymmetric_lower_bound<ihistory_t, value_t, history, pwlc>> lb;

    ostate_t getStart();
    void save(std::string, bool=true);
    void save(const ostate_t&, horizon);
    bool stop(const ostate_t&, horizon) const;
    void print(const ostate_t&, horizon, bool);
    observation getGreedyObservation(const ostate_t&, const oaction_t&, horizon) const;

  public:
    ~asymmetric_hsvi();
    asymmetric_hsvi(number, number, std::string);
    void explore(const ostate_t&, horizon);
    double getValueAt(const ostate_t&, horizon) const;
    void solve(const std::shared_ptr<World>&, horizon, double=0.001, double=1.0);
  };
}
