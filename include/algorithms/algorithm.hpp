#pragma once

#include "../common.hpp"
#include "../public/state.hpp"
#include "../public/world.hpp"

//!
//! \file     algorithm.hpp
//! \brief    algorithm class
//! \version  1.0
//!
//! This class provides the algorithm' public interface.
//!

//! \namespace  sdm
//!
//! Namespace grouping all tools required for sequential decision making.
namespace sdm{
  class Algorithm{
  protected:
    double rate_end      = 0.0001;
    double rate_start    = 1.0;
    double rate_decay    = 100000;

    double epsilon;
    double eps_end      = 0.0001;
    double eps_start    = 1.0;
    double eps_decay    = 100000;

    double learning_rate, lr1, lr2, lr3;

    bool backup = false;

    number trial, trials;

    // the current episode index
    number episode;

    // the total number of episode
    number episodes;

    // batch size
    number batch_size;

    // the length limit in terms of observations
    number length_limit;

    std::shared_ptr<State> start;

  public:
    virtual ~Algorithm();
    virtual void solve(const std::shared_ptr<World>&, horizon, double=0.001, double=1.0) = 0;
  };
}
