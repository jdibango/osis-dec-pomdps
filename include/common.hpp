#pragma once

#include <random>

#include "types.hpp"
#include "utils/linear_algebra/vector.hpp"
#include "utils/linear_algebra/vector_container.hpp"
#include "utils/information_states/history_state/ihistory.hpp"
#include "utils/information_states/history_state/jhistory.hpp"

//!
//! \file     common.hpp
//! \brief    common class
//! \version  1.0
//!
//! This class provides basic type alias for dpomdp.
//!

//! \namespace  sdm
//!
//! Namespace grouping all tools required for sequential decision making.
namespace sdm{
  //<! constant data
  const number ENUMERATION = 0;
  const number LINEAR_PROGRAM = 1;
  const bool FINITE_HORIZON = true;
  const bool INFINITE_HORIZON = false;
  const bool POMDP = true;
  const bool MDP = false;


  //<! defines type belief_action_observation
  using belief_action_observation = std::tuple<std::shared_ptr<Vector>, action, observation>;

  //<! defines type belief_action_observation
  using belief_pair_action_observation = std::tuple<std::shared_ptr<Vector>, action, std::pair<action,observation>>;

  //<! defines type action_number_vector
  using action_number_vector = std::unordered_map<action, std::unordered_map<number, std::shared_ptr<Vector>>>;

  template<typename function>
  using Gamma = std::unordered_map<std::shared_ptr<State>, std::shared_ptr<function>>;

  //<! this stores the upper-bound value-state-action triplets along with their identifiers
  using Points = std::unordered_map<number, std::pair<std::shared_ptr<State>, double>>;

  class common{
  public:

    static void logo();

    static double error;

    //<! static set of all alpha \@vectors
    static vector_container alpha_space;

    //<! defines the model of the problem to be solved
    static std::shared_ptr<dpomdp> model;

    static void saveAlphas(std::shared_ptr<Vector>&);

    //<! static set of all belief \@vectors
    static vector_container belief_space;

    static void printBeliefSpace();

    static void saveBeliefs(std::shared_ptr<Vector>&);

    static std::shared_ptr<Vector> getStart();

    static std::default_random_engine& global_urng();

    static number kronecker(number, number);

    static void randomize();

    static int pick_a_number(int, int);

    static double pick_a_number(double, double);

    static double clip(double, double, double);

    static std::shared_ptr<Vector> nextBelief(const belief_action_observation&);

    static std::shared_ptr<Vector> nextBelief(const belief_pair_action_observation&);

    static std::string getVarNameState(state);

    static number getNumber(const std::string&);

    static void setNumber(const std::string&, number);

    static void clearVariables();

    static std::string getVarNameWeight(number);

    static std::string getVarNameJointBeliefAction( std::pair<std::shared_ptr<Vector>, std::shared_ptr<Vector>>, action );

    static std::string getVarNameBeliefAction( agent, std::shared_ptr<Vector>, action );

    static std::string getVarNameKappa( std::shared_ptr<Vector> );

    static std::string getVarNameKappaState( std::shared_ptr<Vector>, state );

    static std::string getVarNameJointBeliefActionKappaState( std::pair<std::shared_ptr<Vector>, std::shared_ptr<Vector>>, action, std::shared_ptr<Vector>, state );

    static std::string getVarNameJointHistoryActionKappaState( observation_jhistory*, action, std::shared_ptr<Vector>, state );

    static std::string getVarNameOccupancyMeasure(agent, agent, state, observation, action, observation, action);

    static std::string getVarNameOccupancyMeasure(agent, agent, state, state, state, observation, action, observation, action);

    static std::string getVarNameOccupancyMeasure(agent, agent, state, observation_ihistory*, action, observation_ihistory*, action);

    static std::string getVarNameOccupancyMeasure(agent, agent, state, action_observation_ihistory*, action, action_observation_ihistory*, action);

    static std::string getVarNameDecisionRule(agent, action, observation);

    static std::string getVarNameAuxillary(agent, agent, state, observation, observation);

    static std::string getVarNameBagObservation(number, observation);

    static std::string getVarNameAuxillary(agent, agent, state, state, state, observation, observation);

    static std::string getVarNameAuxillary(agent, agent, state, observation_ihistory*, observation_ihistory*);

    static std::string getVarNameAuxillary(agent, agent, state, action_observation_ihistory*, action_observation_ihistory*);

    static std::string getVarNameDecisionRule(action, state);

    static std::string getVarNameWeightedState(number, state);

    static std::string getVarNameWeightedBelief(number, state, state);

    static std::string getVarNameJointHistoryDecisionRule(action, observation_jhistory*);

    static std::string getVarNameStateDecisionRule(state, action);

    static std::string getVarNameStateJointHistoryDecisionRule(state, action, observation_jhistory*);

    static std::string getVarNameJointHistoryDecisionRule(action, action_observation_jhistory*);

    static std::string getVarNameStateJointHistoryDecisionRule(state, action, action_observation_jhistory*);

    static std::string getVarNameWeightedDecisionRule(action, number, state);

    static std::string getVarNameStateJointHistory(state, observation_jhistory*);

    static std::string getVarNameStateJointHistory(state, action_observation_jhistory*);

    static std::string getVarNameWeightedStateJointHistory(number, state, observation_jhistory*);

    static std::string getVarNameStateActionJointHistory(state, action, observation_jhistory*);

    static std::string getVarNameWeightedStateActionJointHistory(number, state, action, observation_jhistory*);

    static std::string getVarNameIndividualHistoryDecisionRule(action, observation_ihistory*, agent);

    static std::string getVarNameIndividualHistoryDecisionRule(action, action_observation_ihistory*, agent);

    static std::string getVarNameObservation(observation);

    static std::string getVarNameStateAction(state, action);

    static std::string getVarNameStateActionState(state, action, state);

    static std::string getVarNameStateNode(state, number);

    static std::string getVarNameStateObservation(state, observation);

    static std::string getVarNameAgentObservation(agent, observation);

    static std::string getVarNameObservationAction(observation, action);

    static std::string getVarNameStateObservationAction(state, observation, action);

    static std::string getVarNameIndividualObservationAction(observation, action, agent);

    static std::string getVarNameIndividualNodeAction(number, action, agent);

    static std::string getVarNameIndividualDynamicsNodeObservationNode(number, observation, number, agent);

    static std::string getVarNameOccupancyMeasureFSC(state, number, action, observation, number);

    static std::string getVarNameMarginalOccupancyMeasure(number, action, observation, number);

    static std::string getVarNameLightOccupancyStateValue(number, observation);

    static std::string getVarNameCornerPoint(const std::shared_ptr<Vector>&, observation);

    static std::string getVarNameNonCornerPointBelief(number, const std::shared_ptr<Vector>&);

    static std::string getVarNameNonCornerPointBeliefObservation(number, const std::shared_ptr<Vector>&, observation);




    static std::string getState(state);
    static std::string getEdge(agent, agent);
    static std::string getAction(action, action);
    static std::string getStateAction(state, action, action);
    static std::string getAgentActionState(agent, action, state);
    static std::string getStateActionObservationState(state, action, action, observation, observation, state);

    //<! this stores the cplex variables along with their identifiers
    static std::unordered_map<std::string, number> variables;

    //<! static mapping from \@belief_action_observation to \@vector
    static std::map<belief_action_observation, std::shared_ptr<Vector>> belief_transitions;

  };
}
