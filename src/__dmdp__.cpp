/*=============================================================================

==============================================================================*/
#include "../include/dpomdp/__dmdp__.hpp"

namespace sdm{

  bool __dmdp__::isJointlyFullyObservable(){
    if( this->isCheckedDmdp ) return this->isDmdp;

    this->isCheckedDmdp = true;

    for(action u=0; u<this->getNumActions(); ++u){
      for(state x=0; x<this->getNumStates(); ++x){

        if( u == 0 ){
          for(observation z=0; z<this->getNumObservations(); ++z){
            if(this->getObservationProbability(u, z, x) == 1.0)
              this->state2observation.emplace(x,z);
          }
        }

        if( this->state2observation.find(x) == this->state2observation.end()  or this->getObservationProbability(u, this->state2observation.at(x), x) != 1.0 ){
          this->isDmdp = false;
          return this->isDmdp;
        }
      }
    }

    this->isDmdp = true;
    return this->isDmdp;
  }


  observation __dmdp__::getObservationFromState(state x){
    return this->state2observation.at(x);
  }

}
