//! \date 12 October 2019

#include "../include/utils/backup_structures/piece_wise_linear_convex/light_upper_bound.hpp"

namespace sdm{
  template<typename ihistory_t, number optimizer, bool pomdp>
  light_upper_bound<ihistory_t, optimizer, pomdp>::light_upper_bound(){};

  template<typename ihistory_t, number optimizer, bool pomdp>
  light_upper_bound<ihistory_t, optimizer, pomdp>::~light_upper_bound(){};

  template<typename ihistory_t, number optimizer, bool pomdp>
  void light_upper_bound<ihistory_t, optimizer, pomdp>::initialize(number length, number trials, bool is_finite){
    this->is_finite = is_finite;
    for(horizon h=common::model->getPlanningHorizon(); h<=common::model->getPlanningHorizon(); --h){
      this->container.emplace(h, std::vector<ubag_t>());
      this->container.at(h).push_back(ubag_t());
      auto init = std::unordered_map<std::shared_ptr<light_occupancy_state<ihistory_t>>, std::pair<double,double>>();
      this->point_set.push_back(init);
    }

    if( pomdp ){
      this->getPartiallyObservableMarkovDecisionProcessFiniteValueFunction(length, trials);
    }else{
      this->mdp_vf.emplace(common::model->getPlanningHorizon(), std::make_shared<Vector>(common::model->getNumStates()));
      this->mdp_vf.at(common::model->getPlanningHorizon())->init(0.0);
      if( is_finite ) this->getMarkovDecisionProcessFiniteValueFunction();
      else this->getMarkovDecisionProcessInfiniteValueFunction();
    }
  }

  template<typename ihistory_t, number optimizer, bool pomdp>
  void light_upper_bound<ihistory_t, optimizer, pomdp>::getMarkovDecisionProcessFiniteValueFunction(){
    double value;
    for(horizon h=common::model->getPlanningHorizon(); h > 0; --h){
      this->mdp_vf.emplace(h-1, std::make_shared<Vector>(common::model->getNumStates()));
      this->mdp_vf.at(h-1)->init(std::numeric_limits<double>::lowest());
      for(state x = 0; x < common::model->getNumStates(); ++x){
        for(action u = 0; u<common::model->getNumActions(); ++u){
          value = common::model->getReward(x,u);
          for(state y = 0; y < common::model->getNumStates(); ++y){
            value += common::model->getDiscount() * common::model->getTransitionProbability(x,u,y) * (*this->mdp_vf.at(h))[y];
          }
          (*this->mdp_vf.at(h-1))[x] = std::max(value,(*this->mdp_vf.at(h-1))[x]);
        }
      }
    }
  }

  template<typename ihistory_t, number optimizer, bool pomdp>
  void light_upper_bound<ihistory_t, optimizer, pomdp>::getPartiallyObservableMarkovDecisionProcessFiniteValueFunction(number length, number trials){
    std::string s = "";
    this->pomdp_vf = std::make_shared<asymmetric_hsvi<ihistory_t, double, std::shared_ptr<asymmetric_belief_state<ihistory_t, false>>, action, false, false>>(length, trials, s);
    this->pomdp_vf->solve( common::model, common::model->getPlanningHorizon(), 0);
  }


  template<typename ihistory_t, number optimizer, bool pomdp>
  void light_upper_bound<ihistory_t, optimizer, pomdp>::getMarkovDecisionProcessInfiniteValueFunction(){
    double value;
    Vector difference;
    auto tmp = std::make_shared<Vector>(common::model->getNumStates());

    do{
      *tmp = *this->mdp_vf.at(0);
      for(state x = 0; x < common::model->getNumStates(); ++x){
        for(action u = 0; u<common::model->getNumActions(); ++u){
          value = common::model->getReward(x,u);
          for(state y = 0; y < common::model->getNumStates(); ++y)
            value += common::model->getDiscount() * common::model->getTransitionProbability(x,u,y) * (*tmp)[y];
          (*this->mdp_vf.at(0))[x] = std::max(value, (*this->mdp_vf.at(0))[x]);
        }
      }
      difference = *tmp; difference -= *this->mdp_vf.at(0);
    }while( difference.norm_1() > common::error );
  }



  template<typename ihistory_t, number optimizer, bool pomdp>
  std::pair<action, std::shared_ptr<light_decision_rule>> light_upper_bound<ihistory_t, optimizer, pomdp>::setValueAt(std::shared_ptr<light_occupancy_state<ihistory_t>>& s, double lower_bound, horizon h){
    double upper_bound;
    auto jdr = this->getGreedyDecisionAt(s, upper_bound, lower_bound, h);
    auto pair = std::make_pair(upper_bound, this->getMarkovDecisionProcessValueAt(s, h));

    if( this->point_set.at(h).find(s) == this->point_set.at(h).end() )
      this->point_set.at(h).emplace(s, pair);

    else this->point_set.at(h)[s] = pair;

    this->pruning(h);

    return jdr;
  }


  template<>
  std::pair<action, std::shared_ptr<light_decision_rule>> light_upper_bound<observation_ihistory, ENUMERATION>::getGreedyDecisionAt(std::shared_ptr<light_occupancy_state<observation_ihistory>>& s, double& upper_bound, double, horizon h){
    double q;
    auto dc_generator = s->getVariations();
    upper_bound = std::numeric_limits<double>::lowest();
    std::pair<action, std::shared_ptr<light_decision_rule>> pair;

    for(action u1 = 0; u1 < common::model->getNumActions(0); ++u1){
      auto ldr = dc_generator.begin();
      auto dr = std::make_shared<light_decision_rule>(ldr->container);
      do{
        auto jdr = std::make_pair(u1, dr);
        q = s->getReward(jdr);
        for(observation z1=0; z1<common::model->getNumObservations(0); ++z1){
          auto uz = std::make_pair(u1,z1);
          auto s_ = s->next(uz, dr);
          q += common::model->getDiscount() * s_->prob * this->getValueAt(s_, this->is_finite ? h+1 : 0 );
        }

        if(q > upper_bound){
          pair = jdr;
          upper_bound = q;
        }

        if((ldr = dc_generator.next()) != nullptr) dr = std::make_shared<light_decision_rule>(ldr->container);
      }while( ldr  != nullptr );
    }

    return pair;
  }


  template<>
  std::pair<action, std::shared_ptr<light_decision_rule>> light_upper_bound<observation_ihistory, ENUMERATION, true>::getGreedyDecisionAt(std::shared_ptr<light_occupancy_state<observation_ihistory>>& s, double& upper_bound, double, horizon h){
    double q;
    auto dc_generator = s->getVariations();
    upper_bound = std::numeric_limits<double>::lowest();
    std::pair<action, std::shared_ptr<light_decision_rule>> pair;

    for(action u1 = 0; u1 < common::model->getNumActions(0); ++u1){
      auto ldr = dc_generator.begin();
      auto dr = std::make_shared<light_decision_rule>(ldr->container);
      do{
        auto jdr = std::make_pair(u1, dr);
        q = s->getReward(jdr);
        for(observation z1=0; z1<common::model->getNumObservations(0); ++z1){
          auto uz = std::make_pair(u1,z1);
          auto s_ = s->next(uz, dr);
          q += common::model->getDiscount() * s_->prob * this->getValueAt(s_, this->is_finite ? h+1 : 0 );
        }

        if(q > upper_bound){
          pair = jdr;
          upper_bound = q;
        }

        if((ldr = dc_generator.next()) != nullptr) dr = std::make_shared<light_decision_rule>(ldr->container);
      }while( ldr  != nullptr );
    }

    return pair;
  }


  template<>
  std::pair<action, std::shared_ptr<light_decision_rule>> light_upper_bound<observation_ihistory, LINEAR_PROGRAM>::getGreedyDecisionAt(std::shared_ptr<light_occupancy_state<observation_ihistory>>& s, double& upper_bound, double, horizon h){
    upper_bound = std::numeric_limits<double>::lowest();
    std::pair<action, std::shared_ptr<light_decision_rule>> pair;

    for(action u1 = 0; u1 < common::model->getNumActions(0); ++u1){
      std::string varname;
      double weight, value = std::numeric_limits<double>::lowest();
      number index = 0, c = 0, n = 0;
      std::shared_ptr<light_decision_rule> dr = nullptr;

      std::unordered_map<std::shared_ptr<light_occupancy_state<observation_ihistory>>, number> identifiers;
      for(const auto& non_corner_point : this->point_set.at(this->is_finite ? h+1 : 0 ))
      identifiers.emplace(non_corner_point.first, n++);

      IloEnv env;
      try{
        IloModel model(env);
        IloRangeArray con(env);
        IloNumVarArray var(env);
        IloObjective obj = IloMaximize(env);

        //<! clear previously stored variables
        common::clearVariables();

        //+-------------------------------------------------------------------+//
        //|                           VARIABLES                               |//
        //+-------------------------------------------------------------------+//
        var.add(IloNumVar(env, -IloInfinity, +IloInfinity, "bound"));
        common::setNumber("bound", index++);

        //create variables a2(u2|b) -- assigning action u2 to belief b
        for(const auto& bp_current_point : s->container){
          for(action u2=0; u2<common::model->getNumActions(1); ++u2){
            varname = common::getVarNameBeliefAction(1, bp_current_point.first, u2);
            var.add(IloBoolVar(env, varname.c_str()));
            common::setNumber(varname, index++);
          }
        }

        if( this->point_set.at(this->is_finite ? h+1 : 0).size() > 0 ){
          for(observation z1 = 0; z1 < common::model->getNumObservations(0); ++z1){
            //<! create variables W(z1), value corresponding to observation z1 of agent 1
            varname = common::getVarNameObservation(z1);
            var.add(IloNumVar(env, -IloInfinity, +IloInfinity, varname.c_str()));
            common::setNumber(varname, index++);

            for(const auto& non_corner_point : this->point_set.at(this->is_finite ? h+1 : 0 )){
              for(const auto& bp_non_corner_point : non_corner_point.first->container){
                //<! create variables W(z1, s, b), weight corresponding to observation z1 of agent 1, non-corner point, and belief.
                varname = common::getVarNameNonCornerPointBeliefObservation(identifiers.at(non_corner_point.first), bp_non_corner_point.first, z1);
                var.add(IloBoolVar(env, varname.c_str()));
                common::setNumber(varname, index++);
              }
            }
          }
        }

        //+-------------------------------------------------------------------+//
        //|                      OBJECTIVE FUNCTIONS                          |//
        //+-------------------------------------------------------------------+//
        obj.setLinearCoef(var[common::getNumber("bound")], +1);

        //+-------------------------------------------------------------------+//
        //|                      CONSTRAINT FUNCTIONS                         |//
        //+-------------------------------------------------------------------+//
        // constraint 0 : \sum_{b : s(b)>0} \sum_u2 a2(u2|b) * s(b) [ <b, R(u)> + \gamma \sum_z P(z|b,u) <\tau(b,u,z), V_mdp> ] + \sum_{z1} W(z1)
        con.add(IloRange(env, 0, 0));
        con[c].setLinearCoef(var[common::getNumber("bound")], -1);
        for(const auto& bp_current_point : s->container){
          for(action u2=0; u2<common::model->getNumActions(1); ++u2){
            action u = common::model->getJointActionIndex(std::vector<action>({u1,u2}));
            weight = bp_current_point.second * (*bp_current_point.first ^ common::model->getReward(u));
            for(observation z=0; z<common::model->getNumObservations(); ++z){
              auto buz = std::make_tuple(bp_current_point.first, u, z);
              auto weighted_belief = common::nextBelief(buz);
              if( weighted_belief->sum() > 0 ){
                weight += bp_current_point.second * common::model->getDiscount() * (*weighted_belief ^ *this->mdp_vf.at(this->is_finite ? h+1 : 0));
              }
            }
            con[c].setLinearCoef(var[common::getNumber(common::getVarNameBeliefAction(1, bp_current_point.first, u2))], weight);
          }
        }

        if( this->point_set.at(this->is_finite ? h+1 : 0).size() > 0 ){
          for(observation z1 = 0; z1 < common::model->getNumObservations(0); ++z1){
            con[c].setLinearCoef(var[common::getNumber(common::getVarNameObservation(z1))], common::model->getDiscount());
          }
        }
        c++;

        // constraint 1 -- for all z1, s_non_corner, b_next : W(z1) <= W(z1, s_non_corner, b_next) * bigM +  \sum_{b:s(b)>0} \sum_{u2} a2(u2|b) *  s_current(b) \sum_{z2} P(z|b,u) 1{\tau(b,u,z) == b_next} * [(upper_bound(s_non_corner) - mdp(s_non_corner)) / s_non_corner(b_next)]
        if( this->point_set.at(this->is_finite ? h+1 : 0).size() > 0 ){
          for(const auto& non_corner_point : this->point_set.at(this->is_finite ? h+1 : 0)){
            for(const auto& bp_non_corner_point : non_corner_point.first->container){
              auto ratio = (non_corner_point.second.first - non_corner_point.second.second) / non_corner_point.first->getValueAt(bp_non_corner_point.first);
              for(observation z1 = 0; z1 < common::model->getNumObservations(0); ++z1){
                IloExpr expr(env);
                expr = var[common::getNumber(common::getVarNameObservation(z1))];
                for(const auto& bp_current_point : s->container){
                  for(action u2=0; u2<common::model->getNumActions(1); ++u2){
                    weight = 0.0;
                    auto u = common::model->getJointActionIndex(std::vector<action>({u1,u2}));
                    for(observation z2=0; z2<common::model->getNumObservations(1); ++z2){
                      auto z = common::model->getJointObservationIndex(std::vector<observation>({z1,z2}));
                      auto bp_next = s->next(bp_current_point.first, u, z);
                      weight -= (bp_next.first == bp_non_corner_point.first ? 1 : 0) * bp_current_point.second * bp_next.second * ratio;
                    }
                    expr += weight * var[common::getNumber(common::getVarNameBeliefAction(1, bp_current_point.first, u2))];
                  }
                }
                model.add( IloIfThen(env, var[common::getNumber(common::getVarNameNonCornerPointBeliefObservation(identifiers.at(non_corner_point.first), bp_non_corner_point.first, z1))] > 0, expr <= 0 ) );
              }
            }
          }
        }

        // constraint 2 -- for all z1, s_non_corner : \sum_{b : s_non_corner(b)>0} W(z1, s_non_corner, b_next) = 1
        if( this->point_set.at(this->is_finite ? h+1 : 0).size() > 0 ){
          for(observation z1 = 0; z1 < common::model->getNumObservations(0); ++z1){
            for(const auto& non_corner_point : this->point_set.at(this->is_finite ? h+1 : 0)){
              con.add( IloRange(env, 1, 1) );
              for(const auto& bp_non_corner_point : non_corner_point.first->container){
                con[c].setLinearCoef(var[common::getNumber(common::getVarNameNonCornerPointBeliefObservation(identifiers.at(non_corner_point.first), bp_non_corner_point.first, z1))], +1);
              }
              c++;
            }
          }
        }

        // constraint 3 -- for all b : s(b)>0 : \sum_{u2} a2(u2|b) = 1
        for(const auto& bp_current_point : s->container){
          con.add( IloRange(env, 1, 1) );
          for(action u2=0; u2<common::model->getNumActions(1); ++u2){
            con[c].setLinearCoef(var[common::getNumber(common::getVarNameBeliefAction(1, bp_current_point.first, u2))], +1);
          }
          c++;
        }

        model.add(obj);
        model.add(con);
        IloCplex cplex(model);

        cplex.setOut(env.getNullStream());
        cplex.setWarning(env.getNullStream());
        #ifdef DEBUG
        cplex.exportModel("upper-greedy.lp");
        #endif

        if( !cplex.solve() ){
          env.error() << "Failed to optimize greedy upper bound" << std::endl;
          throw(-1);
        }


        value = cplex.getObjValue();
        dr = this->setSolution(s, cplex, var);
      }
      catch(IloException& e){
        std::cerr << "Concert exception caught: " << e << std::endl;
      }
      catch(const std::exception& exc){
        std::cerr << "Non-Concert exception caught: " << exc.what() << std::endl;
      }
      env.end();

      if( upper_bound < value){
        pair = std::make_pair(u1, dr);
        upper_bound = value;
      }
    }

    return pair;
  }

  template<>
  std::pair<action, std::shared_ptr<light_decision_rule>> light_upper_bound<observation_ihistory, LINEAR_PROGRAM, true>::getGreedyDecisionAt(std::shared_ptr<light_occupancy_state<observation_ihistory>>& s, double& upper_bound, double, horizon h){
    upper_bound = std::numeric_limits<double>::lowest();
    std::pair<action, std::shared_ptr<light_decision_rule>> pair;

    for(action u1 = 0; u1 < common::model->getNumActions(0); ++u1){
      std::string varname;
      double weight, value = std::numeric_limits<double>::lowest();
      number index = 0, c = 0, n = 0;
      std::shared_ptr<light_decision_rule> dr = nullptr;

      std::unordered_map<std::shared_ptr<light_occupancy_state<observation_ihistory>>, number> identifiers;
      for(const auto& non_corner_point : this->point_set.at(this->is_finite ? h+1 : 0 ))
      identifiers.emplace(non_corner_point.first, n++);

      IloEnv env;
      try{
        IloModel model(env);
        IloRangeArray con(env);
        IloNumVarArray var(env);
        IloObjective obj = IloMaximize(env);

        //<! clear previously stored variables
        common::clearVariables();

        //+-------------------------------------------------------------------+//
        //|                           VARIABLES                               |//
        //+-------------------------------------------------------------------+//
        var.add(IloNumVar(env, -IloInfinity, +IloInfinity, "bound"));
        common::setNumber("bound", index++);

        //create variables a2(u2|b) -- assigning action u2 to belief b
        for(const auto& bp_current_point : s->container){
          for(action u2=0; u2<common::model->getNumActions(1); ++u2){
            varname = common::getVarNameBeliefAction(1, bp_current_point.first, u2);
            var.add(IloBoolVar(env, varname.c_str()));
            common::setNumber(varname, index++);
          }
        }

        if( this->point_set.at(this->is_finite ? h+1 : 0).size() > 0 ){
          for(observation z1 = 0; z1 < common::model->getNumObservations(0); ++z1){
            //<! create variables W(z1), value corresponding to observation z1 of agent 1
            varname = common::getVarNameObservation(z1);
            var.add(IloNumVar(env, -IloInfinity, +IloInfinity, varname.c_str()));
            common::setNumber(varname, index++);

            for(const auto& non_corner_point : this->point_set.at(this->is_finite ? h+1 : 0 )){
              for(const auto& bp_non_corner_point : non_corner_point.first->container){
                //<! create variables W(z1, s, b), weight corresponding to observation z1 of agent 1, non-corner point, and belief.
                varname = common::getVarNameNonCornerPointBeliefObservation(identifiers.at(non_corner_point.first), bp_non_corner_point.first, z1);
                var.add(IloBoolVar(env, varname.c_str()));
                common::setNumber(varname, index++);
              }
            }
          }
        }

        //+-------------------------------------------------------------------+//
        //|                      OBJECTIVE FUNCTIONS                          |//
        //+-------------------------------------------------------------------+//
        obj.setLinearCoef(var[common::getNumber("bound")], +1);

        //+-------------------------------------------------------------------+//
        //|                      CONSTRAINT FUNCTIONS                         |//
        //+-------------------------------------------------------------------+//
        // constraint 0 : \sum_{b : s(b)>0} \sum_u2 a2(u2|b) * s(b) [ <b, R(u)> + \gamma \sum_z P(z|b,u) <\tau(b,u,z), V_mdp> ] + \sum_{z1} W(z1)
        con.add(IloRange(env, 0, 0));
        con[c].setLinearCoef(var[common::getNumber("bound")], -1);
        for(const auto& bp_current_point : s->container){
          for(action u2=0; u2<common::model->getNumActions(1); ++u2){
            action u = common::model->getJointActionIndex(std::vector<action>({u1,u2}));
            weight = bp_current_point.second * (*bp_current_point.first ^ common::model->getReward(u));
            for(observation z=0; z<common::model->getNumObservations(); ++z){
              auto buz = std::make_tuple(bp_current_point.first, u, z);
              auto weighted_belief = common::nextBelief(buz);
              if( weighted_belief->sum() > 0 ){
                auto hnext = s->next(common::model->getObservationIndex(0, z), true);
                auto unweighted_belief = std::make_shared<Vector>(common::model->getNumStates());
                *unweighted_belief = (1.0/weighted_belief->sum()) * (*weighted_belief);
                auto hb = std::make_shared<asymmetric_belief_state<observation_ihistory>>(unweighted_belief, hnext);
                weight += bp_current_point.second * common::model->getDiscount() * weighted_belief->sum() * this->pomdp_vf->getValueAt(hb, h+1);
              }
            }
            con[c].setLinearCoef(var[common::getNumber(common::getVarNameBeliefAction(1, bp_current_point.first, u2))], weight);
          }
        }

        if( this->point_set.at(this->is_finite ? h+1 : 0).size() > 0 ){
          for(observation z1 = 0; z1 < common::model->getNumObservations(0); ++z1){
            con[c].setLinearCoef(var[common::getNumber(common::getVarNameObservation(z1))], common::model->getDiscount());
          }
        }
        c++;

        // constraint 1 -- for all z1, s_non_corner, b_next : W(z1) <= W(z1, s_non_corner, b_next) * bigM +  \sum_{b:s(b)>0} \sum_{u2} a2(u2|b) *  s_current(b) \sum_{z2} P(z|b,u) 1{\tau(b,u,z) == b_next} * [(upper_bound(s_non_corner) - mdp(s_non_corner)) / s_non_corner(b_next)]
        if( this->point_set.at(this->is_finite ? h+1 : 0).size() > 0 ){
          for(const auto& non_corner_point : this->point_set.at(this->is_finite ? h+1 : 0)){
            for(const auto& bp_non_corner_point : non_corner_point.first->container){
              auto ratio = (non_corner_point.second.first - non_corner_point.second.second) / non_corner_point.first->getValueAt(bp_non_corner_point.first);
              for(observation z1 = 0; z1 < common::model->getNumObservations(0); ++z1){
                IloExpr expr(env);
                expr = var[common::getNumber(common::getVarNameObservation(z1))];
                for(const auto& bp_current_point : s->container){
                  for(action u2=0; u2<common::model->getNumActions(1); ++u2){
                    weight = 0.0;
                    auto u = common::model->getJointActionIndex(std::vector<action>({u1,u2}));
                    for(observation z2=0; z2<common::model->getNumObservations(1); ++z2){
                      auto z = common::model->getJointObservationIndex(std::vector<observation>({z1,z2}));
                      auto bp_next = s->next(bp_current_point.first, u, z);
                      weight -= (bp_next.first == bp_non_corner_point.first ? 1 : 0) * bp_current_point.second * bp_next.second * ratio;
                    }
                    expr += weight * var[common::getNumber(common::getVarNameBeliefAction(1, bp_current_point.first, u2))];
                  }
                }
                model.add( IloIfThen(env, var[common::getNumber(common::getVarNameNonCornerPointBeliefObservation(identifiers.at(non_corner_point.first), bp_non_corner_point.first, z1))] > 0, expr <= 0 ) );
              }
            }
          }
        }

        // constraint 2 -- for all z1, s_non_corner : \sum_{b : s_non_corner(b)>0} W(z1, s_non_corner, b_next) = 1
        if( this->point_set.at(this->is_finite ? h+1 : 0).size() > 0 ){
          for(observation z1 = 0; z1 < common::model->getNumObservations(0); ++z1){
            for(const auto& non_corner_point : this->point_set.at(this->is_finite ? h+1 : 0)){
              con.add( IloRange(env, 1, 1) );
              for(const auto& bp_non_corner_point : non_corner_point.first->container){
                con[c].setLinearCoef(var[common::getNumber(common::getVarNameNonCornerPointBeliefObservation(identifiers.at(non_corner_point.first), bp_non_corner_point.first, z1))], +1);
              }
              c++;
            }
          }
        }

        // constraint 3 -- for all b : s(b)>0 : \sum_{u2} a2(u2|b) = 1
        for(const auto& bp_current_point : s->container){
          con.add( IloRange(env, 1, 1) );
          for(action u2=0; u2<common::model->getNumActions(1); ++u2){
            con[c].setLinearCoef(var[common::getNumber(common::getVarNameBeliefAction(1, bp_current_point.first, u2))], +1);
          }
          c++;
        }

        model.add(obj);
        model.add(con);
        IloCplex cplex(model);

        cplex.setOut(env.getNullStream());
        cplex.setWarning(env.getNullStream());
        #ifdef DEBUG
        cplex.exportModel("upper-greedy.lp");
        #endif

        if( !cplex.solve() ){
          env.error() << "Failed to optimize greedy upper bound" << std::endl;
          throw(-1);
        }


        value = cplex.getObjValue();
        dr = this->setSolution(s, cplex, var);
      }
      catch(IloException& e){
        std::cerr << "Concert exception caught: " << e << std::endl;
      }
      catch(const std::exception& exc){
        std::cerr << "Non-Concert exception caught: " << exc.what() << std::endl;
      }
      env.end();

      if( upper_bound < value){
        pair = std::make_pair(u1, dr);
        upper_bound = value;
      }
    }

    return pair;
  }

  template<typename ihistory_t, number optimizer, bool pomdp>
  std::shared_ptr<light_decision_rule> light_upper_bound<ihistory_t, optimizer, pomdp>::setSolution(std::shared_ptr<light_occupancy_state<ihistory_t>>& s, const IloCplex& cplex, const IloNumVarArray& var){
    auto dr = std::make_shared<light_decision_rule>();
    for(const auto& bp : s->container){
      for(action u2=0; u2<common::model->getNumActions(1); ++u2){
        dr->setValueAt(bp.first, u2, cplex.getValue(var[common::getNumber(common::getVarNameBeliefAction(1, bp.first, u2))]));
      }
    }
    return dr;
  }

  template<typename ihistory_t, number optimizer, bool pomdp>
  number light_upper_bound<ihistory_t, optimizer, pomdp>::size(horizon h) const{
    return this->point_set[h].size();
  }

  template<typename ihistory_t, number optimizer, bool pomdp>
  number light_upper_bound<ihistory_t, optimizer, pomdp>::size() const{
    number size = 0;
    for(horizon h = 0; h < common::model->getPlanningHorizon(); ++h)
      size += this->size(h);
    return size;
  }

  template<typename ihistory_t, number optimizer, bool pomdp>
  double light_upper_bound<ihistory_t, optimizer, pomdp>::getMarkovDecisionProcessValueAt(const std::shared_ptr<light_occupancy_state<ihistory_t>>& s, horizon h) const{
    double mdp = 0.0;

    for(auto& bp: s->container)
    mdp += bp.second * (*bp.first^*this->mdp_vf.at(h));

    return mdp;
  }

  template<>
  double light_upper_bound<observation_ihistory, ENUMERATION, true>::getMarkovDecisionProcessValueAt(const std::shared_ptr<light_occupancy_state<observation_ihistory>>& s, horizon h) const{
    double mdp = 0.0;

    for(auto& bp: s->container){
      auto hb = std::make_shared<asymmetric_belief_state<observation_ihistory>>(bp.first, s->ih0);
      mdp += bp.second * this->pomdp_vf->getValueAt(hb, h);
    }

    return mdp;
  }

  template<>
  double light_upper_bound<observation_ihistory, LINEAR_PROGRAM, true>::getMarkovDecisionProcessValueAt(const std::shared_ptr<light_occupancy_state<observation_ihistory>>& s, horizon h) const{
    double mdp = 0.0;

    for(auto& bp: s->container){
      auto hb = std::make_shared<asymmetric_belief_state<observation_ihistory>>(bp.first, s->ih0);
      mdp += bp.second * this->pomdp_vf->getValueAt(hb, h);
    }

    return mdp;
  }


  template<typename ihistory_t, number optimizer, bool pomdp>
  double light_upper_bound<ihistory_t, optimizer, pomdp>::getValueAt(const std::shared_ptr<light_occupancy_state<ihistory_t>>& s, horizon h) const{
    auto mdp = this->getMarkovDecisionProcessValueAt(s, h);

    double ratio, min = mdp;
    for(const auto& sv : this->point_set[h]){
      ratio = 1.0;
      for(const auto& bp: sv.first->container)
      ratio = std::min(ratio, s->container.find(bp.first) == s->container.end() ? 0 : s->container.at(bp.first) / bp.second);
      min = std::min(min, mdp + ratio * (sv.second.first - sv.second.second));
    }

    return min;
  }

  //<! check whether or not it is dominated by jt
  template<typename ihistory_t, number optimizer, bool pomdp>
  bool light_upper_bound<ihistory_t, optimizer, pomdp>::isDominated(const std::pair<std::shared_ptr<light_occupancy_state<ihistory_t>>, std::pair<double, double>>& it, const std::pair<std::shared_ptr<light_occupancy_state<ihistory_t>>, std::pair<double, double>>& jt){
    return this->getSawtooth( it.first, jt ) < it.second.first - it.second.second + common::error;
  }

  template<typename ihistory_t, number optimizer, bool pomdp>
  void light_upper_bound<ihistory_t, optimizer, pomdp>::pruning(horizon h){
    std::vector<std::shared_ptr<light_occupancy_state<ihistory_t>>> toDelete;
    auto it_start = this->point_set.at(h).begin();

    for(number it = 0; it < this->point_set.at(h).size(); ++it)
    for(number jt = it+1; jt < this->point_set.at(h).size(); ++jt){
      if( this->isDominated(*std::next(it_start, it) ,*std::next(it_start, jt)) ) toDelete.push_back(std::next(it_start, it)->first);
      else if( this->isDominated(*std::next(it_start, jt), *std::next(it_start, it)) ) toDelete.push_back(std::next(it_start, jt)->first);
    }

    std::unique(toDelete.begin(),toDelete.end());


    for(auto& entry : toDelete){
      this->point_set.at(h).erase(entry);
    }
  }


  template<typename ihistory_t, number optimizer, bool pomdp>
  double light_upper_bound<ihistory_t, optimizer, pomdp>::getSawtooth( const std::shared_ptr<light_occupancy_state<ihistory_t>>& s, const std::pair<std::shared_ptr<light_occupancy_state<ihistory_t>>, std::pair<double, double>>& entry ){
    double max = std::numeric_limits<double>::lowest();
    for(const auto& bp : entry.first->container)
    max = std::max(max, s->getValueAt(bp.first) * (entry.second.first - entry.second.second) / bp.second);
    return max;
  }

  template class light_upper_bound<observation_ihistory, ENUMERATION>;
  template class light_upper_bound<observation_ihistory, LINEAR_PROGRAM>;
  template class light_upper_bound<observation_ihistory, ENUMERATION, true>;
  template class light_upper_bound<observation_ihistory, LINEAR_PROGRAM, true>;
}
