//!date 18.10.2019

#include "../include/utils/decision_rules/light_decision_rule.hpp"

namespace sdm{

  light_decision_rule::light_decision_rule(){};

  light_decision_rule::~light_decision_rule(){};

  light_decision_rule::light_decision_rule(std::unordered_map<std::shared_ptr<Vector>, std::unordered_map<action, double>>& container){
    this->container = container;
  }

  light_decision_rule::light_decision_rule(const std::vector<std::shared_ptr<Vector>>& keys, const std::vector<action>& values){
    for(number i=0; i < keys.size(); ++i){
      this->setValueAt(keys[i], values[i], 1.0);
    }
  }

  void light_decision_rule::setAction(action u0){
    this->u0 = u0;
  }

  action light_decision_rule::getAction() const{
    return this->u0;
  }

  double light_decision_rule::getValueAt(const std::shared_ptr<Vector>& b, action u) const{
    if( this->container.find(b) == this->container.end()  || this->container.at(b).find(u) == this->container.at(b).end() ) return 0;
    return this->container.at(b).at(u);
  }

  void light_decision_rule::setValueAt(const std::shared_ptr<Vector>& b, action u, double p){
      if( this->container.find(b) == this->container.end() ){
        this->container.emplace(b, std::unordered_map<action, double>());
      }

      if( this->container.at(b).find(u) == this->container.at(b).end() ){
        this->container.at(b).emplace(u, p);
      }
      else
        this->container.at(b)[u] = p;
  }

  number light_decision_rule::size() const{
    return this->container.size();
  }


  std::ostream& operator<<(std::ostream& os, const light_decision_rule& ldr){
    os << "\033[1m\033[32m  <light-decision-rule ref=\"" << &ldr << "\" dimension=\"" << ldr.size() << "\"> \033[0m" << std::endl;
    for(auto& pair : ldr.container)
      for(action u=0; u<common::model->getNumActions(1); ++u){
        if(ldr.getValueAt(pair.first, u) != 0)
          os << "\033[1m\033[32m  \t<entry value=\"" << *pair.first << "\" action= " << common::model->getActionName(1,u) << "\tprob=\"" << ldr.getValueAt(pair.first, u) << "\" /> \033[0m" << std::endl;
      }
    os << "\033[1m\033[32m  </light-decision-rule> \033[0m" << std::endl;
    return os;
  }
}
