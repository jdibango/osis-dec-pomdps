#include "../include/utils/hash.hpp"

namespace sdm{
  template<typename T>
  number hash<T>::operator()(const T& value) const {
    return std::hash<T>()(value);
  }

  template<typename T>
  number hash<T*>::operator()(const T*& pvalue) const {
    return hash<T>()(*pvalue);
  }

  template<typename T>
  number hash<std::shared_ptr<T>>::operator()(const std::shared_ptr<T>& pvalue) const {
     return hash<T>()(*pvalue);
  }
}
