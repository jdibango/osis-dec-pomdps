#include <iomanip>

#include "../include/common.hpp"
#include "../include/dpomdp/dpomdp.hpp"

namespace sdm{
  void common::logo(){
  }

  std::string common::getVarNameBagObservation(number k, observation z1){
    std::ostringstream oss;
    oss << "bag." << k << "_obs" << z1;
    return oss.str();

  }

  number common::kronecker(number i, number j){
    return i == j ? 1 : 0;
  }

  std::shared_ptr<Vector> common::getStart(){
    return model->getStart();
  }

  std::default_random_engine& common::global_urng( ) {
    static std::default_random_engine  u{};
    return u;
  }

  void common::randomize( ) {
    static std::random_device  rd{};
    global_urng().seed( rd() );
  }

  int common::pick_a_number( int from, int thru ) {
    static std::uniform_int_distribution<>  d{};
    using  parm_t  = decltype(d)::param_type;
    return d( global_urng(), parm_t{from, thru} );
  }

  double common::pick_a_number( double from, double upto ) {
    static std::uniform_real_distribution<>  d{};
    using  parm_t  = decltype(d)::param_type;
    return d( global_urng(), parm_t{from, upto} );
  }

  void common::saveAlphas(std::shared_ptr<Vector>& alpha){
    if( !alpha_space.contains( alpha ) ){
//      std::cout << "I want to insert @= " <<alpha <<  "\t" << *alpha << std::endl;
      alpha_space.insert(alpha);
    }
  }

  void common::saveBeliefs(std::shared_ptr<Vector>& belief){
    if( !belief_space.contains( belief ) ){
//      std::cout.precision(10);
//      std::cout << "I want to insert  @" << belief << "\t value = " << (*belief)[0] << " " <<  (*belief)[1] << std::endl;
      belief_space.insert(belief);
    }
  }

  std::string common::getVarNameJointBeliefAction( std::pair<std::shared_ptr<Vector>, std::shared_ptr<Vector>> b, action u ){
    std::ostringstream oss;
    oss << "jbelief." << *b.first << "." << *b.second << ".action." << u;
    return oss.str();
  }

  std::string common::getVarNameKappa( std::shared_ptr<Vector> kappa ){
    std::ostringstream oss;
    oss << "point." << *kappa;
    return oss.str();
  }

  std::string common::getVarNameKappaState( std::shared_ptr<Vector> kappa, state x ){
    std::ostringstream oss;
    oss << "point." << *kappa << ".state." << x;
    return oss.str();
  }

  std::string common::getVarNameJointBeliefActionKappaState( std::pair<std::shared_ptr<Vector>, std::shared_ptr<Vector>> b, action u, std::shared_ptr<Vector> kappa, state x ){
    std::ostringstream oss;
    oss << "jbelief." << *b.first << "." << *b.second << ".action." << u << ".point." << *kappa << ".state." << x;
    return oss.str();
  }

  std::string common::getVarNameJointHistoryActionKappaState( observation_jhistory* jh, action u, std::shared_ptr<Vector> kappa, state x ){
    std::ostringstream oss;
    oss << "jhistory." << *jh << ".action." << u << ".point." << *kappa << ".state." << x;
    return oss.str();
  }

  std::string common::getVarNameBeliefAction( agent i, std::shared_ptr<Vector> b, action u ){
    std::ostringstream oss;
    oss << "agent." << i << ".belief." << *b << ".action." << model->getActionName(i, u);
    return oss.str();
  }

  //TODO Replace the transpose operation with a well-founded matrix definition
  std::shared_ptr<Vector> common::nextBelief(const belief_action_observation& belief_az){
    action a;
    observation z;
    std::shared_ptr<Vector> b;
    std::tie(b,a,z) = belief_az;

    auto tmp = belief_transitions.find(belief_az);
    if(tmp != belief_transitions.end()){
      return tmp->second;
    }

    auto weighted_belief = std::make_shared<Vector>(model->getNumStates());
    (*weighted_belief) = model->getDynamics(a,z).transpose() * (*b);

    saveBeliefs(weighted_belief);
    belief_transitions.emplace(belief_az, weighted_belief);
    return weighted_belief;
  }

  double common::clip(double n, double lower, double upper) {
    return std::max(lower, std::min(n, upper));
  }

  std::shared_ptr<Vector> common::nextBelief(const belief_pair_action_observation& belief_az){
    return nextBelief( std::make_tuple(std::get<0>(belief_az), std::get<1>(belief_az), std::get<2>(belief_az).second) );
  }

  void common::clearVariables(){
    variables.clear();
  }

  std::string common::getVarNameWeightedState(number i, state s){
    std::ostringstream oss;
    oss << "weighted-state" << "." << i << "." << s;
    return oss.str();
  }

  std::string common::getVarNameState(state s){
    std::ostringstream oss;
    oss << "state" << "." << s;
    return oss.str();
  }

  std::string common::getVarNameStateDecisionRule(state x, action u){
    std::ostringstream oss;
    oss << "os" << "." << x << "." << u;
    return oss.str();
  }

  std::string common::getVarNameWeight(number index){
    std::ostringstream oss;
    oss << "weight" << "." << index;
    return oss.str();
  }

  std::string common::getVarNameDecisionRule(action a, state s){
    std::ostringstream oss;
    oss << "dr" << "." << a  << "." <<  s;
    return oss.str();
  }

  std::string common::getVarNameOccupancyMeasure(agent i, agent j, state x, observation zi, action ui, observation zj, action uj){
    std::ostringstream oss;
    oss << "om" << "." << x << "." << i  << "." << zi << "." << ui  << "." << j  << "." << zj << "." << uj;
    return oss.str();
  }

  std::string common::getVarNameOccupancyMeasure(agent i, agent j, state xu, state xi, state xj, observation zi, action ui, observation zj, action uj){
    std::ostringstream oss;
    oss << "om" << "." << xu << "." << xi << "." << xj << "." << i  << "." << zi << "." << ui  << "." << j  << "." << zj << "." << uj;
    return oss.str();
  }

  std::string common::getVarNameOccupancyMeasure(agent i, agent j, state x, observation_ihistory* zi, action ui, observation_ihistory* zj, action uj){
    std::ostringstream oss;
    oss << "om" << "." << x << "." << i  << "." << *zi << "." << ui  << "." << j  << "." << *zj << "." << uj;
    return oss.str();
  }

  std::string common::getVarNameOccupancyMeasure(agent i, agent j, state x, action_observation_ihistory* zi, action ui, action_observation_ihistory*zj, action uj){
    std::ostringstream oss;
    oss << "om" << "." << x << "." << i  << "." << *zi << "." << ui  << "." << j  << "." << *zj << "." << uj;
    return oss.str();
  }


  std::string common::getVarNameDecisionRule(agent i, action ui, observation zi){
    std::ostringstream oss;
    oss << "dr" << "." << i  << "." << ui << "." << zi;
    return oss.str();
  }

  std::string common::getVarNameAuxillary(agent i, agent j, state x, observation zi, observation zj){
    std::ostringstream oss;
    oss << "aux" << "." << x << "." << i  << "." << zi << "." << j  << "." << zj;
    return oss.str();
  }

  std::string common::getVarNameAuxillary(agent i, agent j, state xu, state xi, state xj, observation zi, observation zj){
    std::ostringstream oss;
    oss << "aux" << "." << xu << "." << xi << "." << xj << "." << i  << "." << zi << "." << j  << "." << zj;
    return oss.str();
  }

  std::string common::getVarNameAuxillary(agent i, agent j, state x, observation_ihistory* zi,  observation_ihistory* zj){
    std::ostringstream oss;
    oss << "aux" << "." << x << "." << i  << "." << *zi << "." << j  << "." << *zj;
    return oss.str();

  }

  std::string common::getVarNameAuxillary(agent i, agent j, state x, action_observation_ihistory* zi, action_observation_ihistory* zj){
    std::ostringstream oss;
    oss << "aux" << "." << x << "." << i  << "." << *zi << "." << j  << "." << *zj;
    return oss.str();
  }


  std::string common::getVarNameStateJointHistoryDecisionRule(state x, action a, observation_jhistory* jh){
    std::ostringstream oss;
    oss << "jdr" << "." << x << "." << a  << "." << *jh;
    return oss.str();
  }

  std::string common::getVarNameJointHistoryDecisionRule(action a, observation_jhistory* jh){
    std::ostringstream oss;
    oss << "jdr" << "." << a  << "." << *jh;
    return oss.str();
  }

  std::string common::getVarNameStateJointHistoryDecisionRule(state x, action a, action_observation_jhistory* jh){
    std::ostringstream oss;
    oss << "jdr" << "." << x << "." << a  << "." << *jh;
    return oss.str();
  }


  std::string common::getVarNameJointHistoryDecisionRule(action a, action_observation_jhistory* jh){
    std::ostringstream oss;
    oss << "jdr" << "." << a  << "." << *jh;
    return oss.str();
  }

  std::string common::getVarNameStateJointHistory(state s, observation_jhistory* jh){
    std::ostringstream oss;
    oss << "so" << "." << s << "." << *jh;
    return oss.str();
  }

  std::string common::getVarNameStateJointHistory(state s, action_observation_jhistory* jh){
    std::ostringstream oss;
    oss << "so" << "." << s << "." << *jh;
    return oss.str();
  }

  std::string common::getVarNameWeightedStateJointHistory(number i, state s, observation_jhistory* jh){
    std::ostringstream oss;
    oss << "wsh" << "." << i  << "." << s << "." << *jh;
    return oss.str();
  }

  std::string common::getVarNameStateActionJointHistory(state x, action u, observation_jhistory* jh){
    std::ostringstream oss;
    oss << "suo" << "." << x << "." << u << "." << *jh;
    return oss.str();
  }

  std::string common::getVarNameWeightedStateActionJointHistory(number i, state s, action u, observation_jhistory* jh){
    std::ostringstream oss;
    oss << "wsuh" << "." << i  << "." << s << "." << u << "." << *jh;
    return oss.str();
  }

  std::string common::getVarNameIndividualHistoryDecisionRule(action a, observation_ihistory* ih, agent ag){
    std::ostringstream oss;
    oss << "idr" << "." << a  << "." << *ih << "." << ag;
    return oss.str();
  }

  std::string common::getVarNameIndividualHistoryDecisionRule(action a, action_observation_ihistory* ih, agent ag){
    std::ostringstream oss;
    oss << "idr" << "." << a  << "." << *ih << "." << ag;
    return oss.str();
  }

  std::string common::getVarNameStateAction(state x, action u){
    std::ostringstream oss;
    oss << "xu" << "." << x << "." << u;
    return oss.str();
  }

  std::string common::getVarNameStateActionState(state x, action u, state y){
    std::ostringstream oss;
    oss << "xu" << "." << x << "." << u << "." << y;
    return oss.str();
  }

  std::string common::getVarNameStateNode(state x, number n){
    std::ostringstream oss;
    oss << "nx" << "." << n << "." << x;
    return oss.str();
  }

  std::string common::getVarNameWeightedBelief(number i, state s, state s_){
    std::ostringstream oss;
    oss << "weighted-belief" << "." << i  << "." << s << "." << s_;
    return oss.str();
  }

  std::string common::getVarNameWeightedDecisionRule(action a, number i, state s){
    std::ostringstream oss;
    oss << "weighted-dr" << "." << a  << "." << i << "." << s;
    return oss.str();
  }

  std::string common::getVarNameObservation(observation z){
    std::ostringstream oss;
    oss << "z" << "." << z;
    return oss.str();
  }

  std::string common::getVarNameStateObservation(state x, observation z){
    std::ostringstream oss;
    oss << "xz" << "." << x  << "." << z;
    return oss.str();
  }

  std::string common::getVarNameObservationAction(observation z, action u){
    std::ostringstream oss;
    oss << "zu" << "." << z  << "." << u;
    return oss.str();
  }

  std::string common::getVarNameAgentObservation(agent ag, observation z){
    std::ostringstream oss;
    oss << "agz" << "." << ag  << "." << z;
    return oss.str();
  }

  std::string common::getVarNameStateObservationAction(state x, observation z, action u){
    std::ostringstream oss;
    oss << "xzu" << "." << x  << "." << z << "." << u;
    return oss.str();
  }

  std::string common::getVarNameIndividualObservationAction(observation zi, action ai, agent i){
    std::ostringstream oss;
    oss << "zai" << "." << zi  << "." << ai << "." << i;
    return oss.str();
  }

  std::string common::getVarNameIndividualNodeAction(number ni, action ui, agent i){
    std::ostringstream oss;
    oss << "nui" << "." << ni  << "." << ui << "." << i;
    return oss.str();
  }


  std::string common::getVarNameIndividualDynamicsNodeObservationNode(number n, observation z, number m, agent i){
    std::ostringstream oss;
    oss << "nzmi" << "." << n  << "." << z << "." << m << "." << i;
    return oss.str();
  }

  std::string common::getVarNameOccupancyMeasureFSC(state x, number n, action u, observation z, number m){
    std::ostringstream oss;
    oss << "xnuzm" << "." << x << "." << n  << "." << u << "." << z << "." << m;
    return oss.str();
  }

  std::string common::getVarNameMarginalOccupancyMeasure(number n, action u, observation z, number m){
    std::ostringstream oss;
    oss << "xnuzm" << "." << n  << "." << u << "." << z << "." << m;
    return oss.str();
  }


  std::string common::getVarNameLightOccupancyStateValue(number k, observation z1){
    std::ostringstream oss;
    oss << "light_occupancy_state" << "." << k << "." << z1;
    return oss.str();
  }

  std::string common::getVarNameCornerPoint(const std::shared_ptr<Vector>& b, observation z1){
    std::ostringstream oss;
    oss << "ConerPoint" << "." << b << "." << z1;
    return oss.str();
  }

  std::string common::getVarNameNonCornerPointBelief(number m, const std::shared_ptr<Vector>& b){
    std::ostringstream oss;
    oss << "NonConerPoint" << "." << b << "." << m;
    return oss.str();
  }


  std::string common::getVarNameNonCornerPointBeliefObservation(number m, const std::shared_ptr<Vector>& b, observation z){
    std::ostringstream oss;
    oss << "NonConerPoint" << "." << b << "." << m << "." << z;
    return oss.str();
  }



  number common::getNumber(const std::string& name){
    // assert( common::variables.find(name) != common::variables.end() );
    if( common::variables.find(name) == common::variables.end() ){
      std::cerr << "something went wrong in here!"<< "\tname:" << name << std::endl;
      exit(-1);
    }

    return common::variables.at( name );
  }

  void common::setNumber(const std::string& name, number id){
    if( variables.find(name) == variables.end()){
      variables.emplace(name, id);
    }
    else{
      variables.at(name) = id;
    }
  }

  std::string common::getState(state x){
    std::ostringstream oss;
    oss << "state" << x;
    return oss.str();
  }

  std::string common::getEdge(agent i, agent j){
    std::ostringstream oss;
    oss << "agent" << i << "." << j;
    return oss.str();
  }

  std::string common::getStateAction(state x, action ui, action uj){
    std::ostringstream oss;
    oss << "state" << x << "." << ui << "." << uj;
    return oss.str();
  }


  std::string common::getStateActionObservationState(state x, action ui, action uj, observation zi, observation zj, state y){
    std::ostringstream oss;
    oss << "state" << x << "." << ui << "." << uj << "." <<  zi<< "." << zj << "." << y;
    return oss.str();
  }

  std::string common::getAgentActionState(agent i, action ui, state x){
    std::ostringstream oss;
    oss << "agent" << i << "." << ui << "." << x;
    return oss.str();
  }


  void common::printBeliefSpace(){
    std::cout << "<belief-space size="<< belief_space.size() << ">" << std::endl;
    for(const auto& v : belief_space) std::cout << "\t§=" << hash_container()(v) << "@=" << v << "\tvalue=" << *v  << std::endl;
    std::cout << "</belief-space >" << std::endl;
  }


  double common::error = 0.00001;

  vector_container common::alpha_space = {};

  vector_container common::belief_space = {};

  std::shared_ptr<dpomdp> common::model = NULL;

  std::unordered_map<std::string, number> common::variables = {};

  std::map<belief_action_observation, std::shared_ptr<Vector>> common::belief_transitions = {};
}
