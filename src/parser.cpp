/*=============================================================================

==============================================================================*/

#include "../include/parser/ast.hpp"
#include "../include/parser/parser.hpp"
#include "../include/parser/config.hpp"
#include "../include/parser/printer.hpp"
#include "../include/parser/encoder.hpp"
#include "../include/parser/parser_def.hpp"
#include "../include/parser/ast_adapted.hpp"

#include "../include/common.hpp"

#include <boost/spirit/home/x3.hpp>
#include <boost/fusion/include/io.hpp>
#include <boost/config/warning_disable.hpp>

#include <string>
#include <fstream>
#include <iostream>

namespace sdm{
  namespace parser{
    BOOST_SPIRIT_INSTANTIATE(preamble_type, iterator_type, context_type)

    int parse_file(char const* filename){
      std::ifstream in(filename, std::ios_base::in);

      if(!in){
        std::cerr << "Error: Could not open input file: " << filename << std::endl;
        return 1;
      }

      std::string storage; // We will read the contents here.
      in.unsetf(std::ios::skipws); // No white space skipping!
      std::copy(
        std::istream_iterator<char>(in),
        std::istream_iterator<char>(),
        std::back_inserter(storage)
      );

      using sdm::parser::preamble_t; // Our grammar
      sdm::ast::preamble_t ast;      // Our tree

      using boost::spirit::x3::eol;
      using boost::spirit::x3::lexeme;
      using boost::spirit::x3::ascii::char_;
      using boost::spirit::x3::ascii::space;
      auto const space_comment = space | lexeme[ '#' >> *(char_ - eol) >> eol];

      std::string::const_iterator end  = storage.end();
      std::string::const_iterator iter = storage.begin();
      bool r = phrase_parse(iter, end, preamble_t, space_comment, ast);

      if(r && iter == end){
        // std::string name(filename);
        // common::model->setFileName(name);
        #ifdef DEBUG
          std::cout << "-------------------------\n";
          std::cout << "Parsing succeeded\n";
          std::cout << "-------------------------\n";

          #ifdef VERBOSE
            sdm::ast::preamble_printer printer;
            printer(ast);
          #endif
        #endif

        sdm::ast::preamble_encoder encoder;
        encoder(ast);
        return 0;
      }

      else
      {
        std::string::const_iterator some = iter+30;
        std::string context(iter, (some>end)?end:some);
        #ifdef DEBUG
          std::cout << "-------------------------\n";
          std::cout << "Parsing failed\n";
          std::cout << "stopped at: \"" << context << "...\"\n";
          std::cout << "-------------------------\n";
        #endif
        return 1;
      }

      return EXIT_SUCCESS;
    }
  }
}
