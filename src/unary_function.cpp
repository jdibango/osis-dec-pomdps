#include "../include/public/unary_function.hpp"

namespace sdm{
  template<typename S, typename A>
  UnaryFunction<S,A>::~UnaryFunction(){}
  template class UnaryFunction<State*, std::shared_ptr<Action>>;
  template class UnaryFunction<std::shared_ptr<State>, std::shared_ptr<Action>>;
}
