#include "../include/public/state.hpp"

namespace sdm{
  State::~State(){}

  observation State::setObservation(std::shared_ptr<Action>&){
    return 0;
  }

  const std::shared_ptr<State>& State::getUncompressed(){
    return this->uncompressed;
  }

  void State::setUncompressed(const std::shared_ptr<State>& s){
    this->uncompressed = s;
  }
}

namespace std{
  size_t hash<sdm::State*>::operator()(sdm::State*const  s) const{
    return s == NULL ? 0 : s->hash();
  }

  size_t hash<shared_ptr<sdm::State>>::operator()(const shared_ptr<sdm::State>& s) const{
    return s == nullptr ? 0 : s->hash();
  }

  bool equal_to<sdm::State*>::operator()(sdm::State*const  lhs, sdm::State*const  rhs) const{
    if(lhs == NULL && rhs == NULL ){
      return true;
    }

    if ( lhs == NULL || rhs == NULL) {
      return false;
    }

    return  lhs->equal_to(rhs);
  }

  bool equal_to<shared_ptr<sdm::State>>::operator()(const shared_ptr<sdm::State>&  lhs, const shared_ptr<sdm::State>&  rhs) const{
    if(lhs == nullptr && rhs == nullptr ){
      return true;
    }

    if ( lhs == nullptr || rhs == nullptr) {
      return false;
    }

    return  lhs->equal_to(rhs);
  }
}
