#include "../include/utils/backup_structures/asymmetric_vector_set.hpp"

namespace sdm{

  template<typename value_t, typename ihistory_t>
  asymmetric_vector_set<value_t, ihistory_t>::~asymmetric_vector_set(){}

  template<typename value_t, typename ihistory_t>
  asymmetric_vector_set<value_t, ihistory_t>::asymmetric_vector_set(){}

  template<>
  void asymmetric_vector_set<std::shared_ptr<Vector>>::initialize(){
    this->v_default = std::unordered_map<horizon, std::shared_ptr<Vector>>();
    for(horizon h=0; h<=common::model->getPlanningHorizon(); ++h){
      this->container.emplace(h, std::vector<gamma_t>());
      auto init = std::make_shared<Vector>( common::model->getNumStates() );
      init->init( common::model->getMinReward() * (common::model->getPlanningHorizon() - h) );
      this->v_default.emplace(h, init);
    }
  }

  template<>
  void asymmetric_vector_set<double>::initialize(){
    this->v_default = std::unordered_map<horizon, double>();
    for(horizon h=0; h<=common::model->getPlanningHorizon(); ++h){
      this->container.emplace(h, std::vector<gamma_t>());
      this->v_default.emplace(h, common::model->getMinReward() * (common::model->getPlanningHorizon() - h));
    }
  }

  //<! this method returns the value at the given occupancy state at the given horizon
  template<typename value_t, typename ihistory_t>
  double asymmetric_vector_set<value_t, ihistory_t>::getValueAt(const std::shared_ptr<light_occupancy_state<ihistory_t>>& s, horizon h){
    double value, max = std::numeric_limits<double>::lowest();

    if( this->container.at(h).empty() ){
      return this->getVectorValueAt(s, this->v_default.at(h));
    }

    for(const auto& gamma : this->container.at(h)){
      for(const auto& beta : gamma){
        value = this->getValueAt(s, beta.second, h);
        if( max < value ){
          max = value;
        }
      }
    }

    return max;
  }

  template<typename value_t, typename ihistory_t>
  double asymmetric_vector_set<value_t, ihistory_t>::getValueAt(const std::shared_ptr<light_occupancy_state<ihistory_t>>& s, const beta_t& beta, horizon h){
    double value = 0.0;
    for(const auto& bp : s->container)
      value += bp.second * this->getBeliefValueAt(bp.first, beta, h);
    return value;
  }

  template<typename value_t, typename ihistory_t>
  double asymmetric_vector_set<value_t, ihistory_t>::getBeliefValueAt(const belief_t& belief, const beta_t& beta, horizon h){
    if(beta.find(belief) == beta.end())
      return this->getBeliefValueAt(belief, this->v_default.at(h));
    return this->getBeliefValueAt(belief, beta.at(belief));
  }

  template<>
  double asymmetric_vector_set<double>::getVectorValueAt(const std::shared_ptr<light_occupancy_state<observation_ihistory>>&, const double& v_default){
    return v_default;
  }

  template<>
  double asymmetric_vector_set<std::shared_ptr<Vector>>::getVectorValueAt(const std::shared_ptr<light_occupancy_state<observation_ihistory>>& s, const std::shared_ptr<Vector>& v_default){
    double value = 0.0;
    for(const auto& bp : s->container)
      value += bp.second * ((*bp.first)^(*v_default));
    return value;
  }

  template<>
  double asymmetric_vector_set<std::shared_ptr<Vector>>::getBeliefValueAt(const belief_t& belief, const std::shared_ptr<Vector>& v_default){
    return (*belief)^(*v_default);
  }

  template<>
  double asymmetric_vector_set<double>::getBeliefValueAt(const belief_t&, const double& v_default){
    return v_default;
  }

  template<typename value_t, typename ihistory_t>
  void asymmetric_vector_set<value_t, ihistory_t>::setValueAt(const std::shared_ptr<light_occupancy_state<ihistory_t>>& s, horizon h){

    gamma_t gamma = gamma_t();
    auto beta = beta_t();
    if(h+1 == common::model->getPlanningHorizon() && this->container.at(h+1).size() == 0){
      for(auto& bp : s->container)
        beta.emplace(bp.first, this->v_default.at(h+1));
      gamma.emplace(s->z0, beta);
      this->container.at(h+1).push_back(gamma);
    }

    beta = beta_t();
    this->getGreedyDecisionAt(s, beta, h);

    for(auto& gamma : this->container.at(h)){
      if( gamma.find(s->z0) == gamma.end() ){
        gamma.emplace(s->z0, beta);
        break;
      }
    }

    if(this->container.at(h).size() == 0){
      gamma_t gamma = gamma_t();
      gamma.emplace(s->z0, beta);
      this->container.at(h).push_back(gamma);
    }

    this->pruning();
  }

  template<typename value_t, typename ihistory_t>
  std::shared_ptr<light_decision_rule> asymmetric_vector_set<value_t, ihistory_t>::getGreedyDecisionAt(const std::shared_ptr<light_occupancy_state<ihistory_t>>& s, beta_t& maxBeta0, horizon h){
    gamma_t gamma0;
    action maxU1, ju;
    double max0, max1, value0, value1;
    std::shared_ptr<light_decision_rule> jdr, maxJDR;
    //<! initiamize intermediate data
    max0 = std::numeric_limits<double>::lowest();
    //<! go over each gamma-vector
    for(const auto& gamma : this->container[h+1]){
      //<! go over each action of the blind agent (i.e., agent 0)
      for(action u0=0; u0<common::model->getNumActions(0); ++u0){
        //<! initialize intermediate data
        value0 = 0; gamma0 = gamma_t();
        jdr = std::make_shared<light_decision_rule>();
        //<! go over each public observation, i.e., observation of the blind agent
        for(observation z0=0; z0<common::model->getNumObservations(0); ++z0){ // start the computation of gamma_s : (z0,bp.first) -> value_t
          gamma0.emplace(z0, beta_t());
          //+---------------------------------------------------------------+//
          //<! go over each belief involved in the current occupancy state !>//
          //+---------------------------------------------------------------+//
          for(const auto& bp : s->container){
            //<! initiamize intermediate data
            max1 = std::numeric_limits<double>::lowest();
            //<! go over each action of the second agent (i.e., agent 1)
            for(action u1=0; u1<common::model->getNumActions(1); ++u1){
              //<! initialize intermediate data
              value1 = 0;
              ju = common::model->getJointActionIndex(std::vector<action>{u0,u1});
              //<! compute the assignment of (z0, bp.first) to value_t w.r.t. (u, gamma)
              value_t value = this->getBeliefValueAt(bp.first, ju, z0, gamma, h);
              gamma0.at(z0).emplace(bp.first, value);
              value1 += this->getBeliefValueAt(bp.first, value);
              //<! select the best action for a given belief amongst all actions of the non-blind agent
              if( max1 < value1 ){
                maxU1 = u1;
                max1 = value1;
                if( gamma0.at(z0).find(bp.first) == gamma0.at(z0).end() ){
                  gamma0.at(z0).emplace(bp.first, value);
                }
                else{
                  gamma0.at(z0)[bp.first] = value;
                }
              }
            }
            //<! update value0
            value0 += bp.second * max1;
            jdr->setValueAt( bp.first, common::model->getJointActionIndex(std::vector<action>{u0,maxU1}), 1.0 );
          }
        } // end the computation of gamma_s : (z0,bp.first) -> value_t

        //<! select the best gamma for each action of the blind agent
        if( max0 < value0 ){
          maxJDR = jdr;
          max0 = value0;
          maxBeta0 = this->add(s, gamma0);
        }
      }
    }

    return maxJDR;
  }

  template<typename value_t, typename ihistory_t>
  std::unordered_map<std::shared_ptr<Vector>, value_t, hash_container, equal_container> asymmetric_vector_set<value_t, ihistory_t>::add(const std::shared_ptr<light_occupancy_state<ihistory_t>>& s, const gamma_t& gamma){
    auto beta = beta_t();
    for(observation z0=0; z0<common::model->getNumObservations(0); ++z0){
      for(const auto& bp : s->container){
        if( beta.find(bp.first) == beta.end()){
          beta.emplace(bp.first, gamma.at(z0).at(bp.first));
        }
        else {
          beta.at(bp.first) =  add( beta.at(bp.first), gamma.at(z0).at(bp.first) );
        }
      }
    }
    return beta;
  }


  template<>
  double asymmetric_vector_set<double>::add(const double& v1, const double& v2){
    return v1 + v2;
  }

  template<>
  std::shared_ptr<Vector> asymmetric_vector_set<std::shared_ptr<Vector>>::add(const std::shared_ptr<Vector>& v1, const std::shared_ptr<Vector>& v2){
    auto v = std::make_shared<Vector>(common::model->getNumStates()); v->init(0.0);
    *v += (*v1);
    *v += (*v2);
    return v;
  }

  template<>
  double asymmetric_vector_set<double>::getBeliefValueAt(const belief_t& belief, action u, observation z0, const gamma_t& gamma, horizon h){
    double reward  = (*belief) ^ common::model->getReward(u);
    reward /= common::model->getNumObservations(0);

    //<!
    double value = 0, sum = 0;

    //<! go over each private observation, i.e., observation of the non-blind agent
    for(observation z1=0; z1<common::model->getNumObservations(1); ++z1){
      //<! get joint observation
      auto z = common::model->getJointObservationIndex(std::vector<observation>({z0,z1}));
      //<! compute belief, action, observation tuple
      auto belief_uz = make_tuple(belief, u, z);
      //<! compute next-belief
      auto bNext = common::nextBelief(belief_uz);
      //<! probability of bNext
      double prob;
      auto tmp = this->getBeliefValueAt(bNext, z0, gamma, prob, h+1);

      if( h==0 and prob > 0 ){
         auto normalizedBNext = std::make_shared<Vector>();
         *normalizedBNext = (*bNext);
         *normalizedBNext /= prob;
         std::cout << "\t\t\t belief=" << *belief << " u=" << u << " z0=" << z0 << " z1=" << z1 << "\tnext=" << *normalizedBNext << "\tvalue-tmp=" << tmp << "\tvalue=" << (common::model->getDiscount() * prob * tmp) << "\tprob=" << prob << std::endl;
       }

      //<! compute the value_t value
      sum += prob;
      value += common::model->getDiscount() * prob * tmp;
    }

    if( h==0 ) std::cout << "sum=" << sum << "\treward=" << reward << "\tnormalized-value=" << (sum==0?0:value/sum)<< std::endl;
    return reward + (sum==0?0:value/sum);
  }

  template<>
  std::shared_ptr<Vector> asymmetric_vector_set<std::shared_ptr<Vector>>::getBeliefValueAt(const belief_t& belief, action u, observation z0, const gamma_t& gamma, horizon h){
    auto reward = std::make_shared<Vector>(common::model->getNumStates() ); reward->init(0.0);
    auto value  = std::make_shared<Vector>( common::model->getNumStates() ); value->init(0.0);

    //<! init value
    *reward = common::model->getReward(u);
    *reward /= (double)common::model->getNumObservations(0);

    double sum = 0;

      //<! go over each private observation, i.e., observation of the non-blind agent
    for(observation z1=0; z1<common::model->getNumObservations(1); ++z1){
      //<! get joint observation
      auto z = common::model->getJointObservationIndex(std::vector<observation>({z0,z1}));
      //<! compute belief, action, observation tuple
      auto belief_uz = make_tuple(belief, u, z);
      //<! compute next-belief
      auto bNext = common::nextBelief(belief_uz);
      //<! probability of bNext
      double prob;
      //<! get next alpha-vector
      auto alpha = std::make_shared<Vector>( common::model->getNumStates() );
      *alpha = *this->getBeliefValueAt(bNext, z0, gamma, prob, h+1);
      // <! go over next states
      for(state x=0; x<common::model->getNumStates(); ++x){
        for(state y=0; y<common::model->getNumStates(); ++y){
          //<! compute the value_t value
          (*value)[x] += common::model->getDiscount() * common::model->getDynamics(x,u,z,y) * (*alpha)[y];
        }
      }
      if(h == 0 && prob > 0){
          auto normalizedBNext = std::make_shared<Vector>();
          *normalizedBNext = (*bNext);
          *normalizedBNext /= prob;
          std::cout << "\t belief=" << *belief << " u=" << u << " z0=" << z0 << " z1=" << z1 << "\tnext=" << *normalizedBNext << "\tvalue-tmp=" << *alpha << "\tprob=" << prob << std::endl;
      }
      sum += prob;
    }
    if(sum == 0) value->init(0.0);
    else *value /= sum;

    *value = common::model->getDiscount() * *value;
    if( h==0 ) std::cout << "sum=" << sum << "\treward=" << *reward << "\tvalue=" << *value;
    *value += *reward;
    if( h==0 ) std::cout << "\tresult = " << *value << std::endl;
    return value;
  }

  template<typename value_t, typename ihistory_t>
  value_t asymmetric_vector_set<value_t, ihistory_t>::getBeliefValueAt(const belief_t& b, observation z, const gamma_t& g, double& prob, horizon h){
    prob = 0.0;

    if( g.find(z) != g.end() ){
      auto unweighted_nb = std::make_shared<Vector>(common::model->getNumStates());
      unweighted_nb->init(0.0);

      if( (prob = b->sum()) > 0 ){
         *unweighted_nb = (1.0/prob) * (*b);
         common::saveBeliefs(unweighted_nb);
         if( g.at(z).find(unweighted_nb) != g.at(z).end() ){
           return g.at(z).at(unweighted_nb);
         }
      }
    }

    return this->v_default.at(h);
  }


  template<typename value_t, typename ihistory_t>
  void asymmetric_vector_set<value_t, ihistory_t>::pruning(){
  }

  template<typename value_t, typename ihistory_t>
  number asymmetric_vector_set<value_t, ihistory_t>::size()const{
    number size = 0;
    for(horizon h = 0; h < common::model->getPlanningHorizon(); ++h)
      size += this->size(h);
    return size;
  }

  template<typename value_t, typename ihistory_t>
  number asymmetric_vector_set<value_t, ihistory_t>::size(horizon h) const{
    return this->container.at(h).size();
  }

  template class asymmetric_vector_set<double>;
  template class asymmetric_vector_set<std::shared_ptr<Vector>>;
}
