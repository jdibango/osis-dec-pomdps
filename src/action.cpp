#include "../include/public/action.hpp"

namespace sdm{
  Action::~Action(){}
  void Action::setObservation(observation){}
  void Action::operator+=(const Action&){}

}

namespace std{
  size_t hash<shared_ptr<sdm::Action>>::operator()(const shared_ptr<sdm::Action>& a) const{
    return a->hash();
  }

  bool equal_to<shared_ptr<sdm::Action>>::operator()(const shared_ptr<sdm::Action>& lhs, const shared_ptr<sdm::Action>& rhs) const{
    return lhs->equal_to(rhs);
  }
}
