/*=============================================================================

==============================================================================*/
#include "../include/dpomdp/dpomdp.hpp"

namespace sdm{

  dpomdp::dpomdp(){}

  dpomdp::dpomdp(action number_jactions, observation number_jobservations){
    this->number_jactions = number_jactions;
    this->rewards.resize( number_jactions );
    this->number_jobservations = number_jobservations;
    for(std::size_t i=0; i<number_jactions; i++)
    this->dynamics.push_back( std::vector<Matrix>(number_jobservations) );
  }

  dpomdp::dpomdp(bool criterion, double discount,
		  	  	  const std::vector<Vector>& rewards,
				  const std::vector<std::vector<Matrix>>& dynamics,
				  agent number_agents, state number_states,
				  const std::vector<action>& number_actions,
				  const std::vector<observation>& number_observations) : criterion(criterion), discount(discount){
    this->rewards = rewards;
    this->dynamics = dynamics;
    this->number_agents = number_agents;
    this->number_states = number_states;
    this->number_actions = number_actions;
    this->number_observations = number_observations;
    this->number_jactions = 1;

    agent i;
    for(i=0; i<this->number_agents; ++i)
    this->number_jactions *= number_actions[i];
    this->generateJointActions( this->number_agents );

    this->number_jobservations = 1;
    for(i=0; i<number_agents; ++i)
    this->number_jobservations *= number_observations[ i ];
    this->generateJointObservations( this->number_agents );
  }

  dpomdp::~dpomdp(){
    for(agent ag=0; ag<this->getNumAgents(); ++ag){
    	this->action_names_bimap[ag].erase(this->action_names_bimap[ag].begin(), this->action_names_bimap[ag].end());
  	  this->observation_names_bimap[ag].erase(this->observation_names_bimap[ag].begin(), this->observation_names_bimap[ag].end());
  	}
  }

  void dpomdp::generate(std::string name){
    std::ofstream myfile;
    myfile.open(name);
    myfile << "agents: " << this->getNumAgents() << std::endl;
    myfile << "discount: " << this->getDiscount()/1.0 << std::endl;
    myfile << "values: \"reward\"" << std::endl;
    myfile << "states: " << this->getNumStates() << std::endl;
    myfile << "start: \"uniform\"" << std::endl;
    myfile << "actions: \n" << this->getNumActions(0) << "\n" << this->getNumActions(1) << std::endl;
    myfile << "observations: \n" << this->getNumObservations(0) << "\n" << this->getNumObservations(1) << std::endl;

    for(state x=0; x<this->getNumStates(); ++x){
      for(action u=0; u<this->getNumActions(); u++) {
        auto ja = this->getJointAction(u);
        auto u1 = ja->getIndividualItem(0);
        auto u2 = ja->getIndividualItem(1);
        for(state y=0; y<this->getNumStates(); ++y){
          myfile << "T: " << u1 << " " << u2 << " : " << x  << " : " << y  << " : " << this->getTransitionProbability(x, u, y) << std::endl;
        }
      }
    }

    for(state y=0; y<this->getNumStates(); ++y){
      for(action u=0; u<this->getNumActions(); u++) {
        auto ja = this->getJointAction(u);
        auto u1 = ja->getIndividualItem(0);
        auto u2 = ja->getIndividualItem(1);
        for(observation z=0; z<this->getNumObservations(); ++z){
          auto jz = this->getJointObservation(z);
          auto z1 = jz->getIndividualItem(0);
          auto z2 = jz->getIndividualItem(1);
          myfile << "O: " << u1 << " " << u2 << " : " << y  << " : " << z1  << " " << z2  << " : " << this->getObservationProbability(u,z,y) << std::endl;
        }
      }
    }

    for(state x=0; x<this->getNumStates(); ++x){
      for(action u=0; u<this->getNumActions(); u++) {
        auto ja = this->getJointAction(u);
        auto u1 = ja->getIndividualItem(0);
        auto u2 = ja->getIndividualItem(1);
        myfile << "R: " << u1 << " " << u2 << " : " << x  << " : " << this->getReward(x, u)<< std::endl;
      }
    }

    myfile.close();
  }

  void dpomdp::setFileName(std::string filename){
    this->filename = filename;
  }

  std::string dpomdp::getFileName(){
    return this->filename;
  }

  bool dpomdp::getCriterion() const{
    return criterion;
  }

  void dpomdp::setCriterion(bool criterion){
    this->criterion = criterion;
  }

  void dpomdp::setBound(double bound){
    this->bound = std::min(1.0 / (bound * (1.0 - this->discount)), 1.0);
  }

  double dpomdp::getDiscount() const{
    return discount;
  }

  void dpomdp::setDiscount(double discount){
    this->discount = discount;
  }

  std::shared_ptr<Vector> dpomdp::getStart() const{
    return start;
  }

  void dpomdp::setStart(const std::shared_ptr<Vector>& start){
    this->start = start;
    common::saveBeliefs(this->start);
  }

  state dpomdp::init(){
    this->internal = this->start_generator(common::global_urng());
    return this->internal;
  }

  void dpomdp::execute(action u, feedback* f){
    assert(f != nullptr);

    state x;
    double r;
    observation z;

    std::tie(r,z,x) = this->getDynamicsGenerator(this->internal, u);

    f->setReward(r);
    this->internal = x;
    f->setObservation( z );
    f->setState(this->internal);

    assert( this->internal < common::model->getNumStates() );
    assert( f->getObservation() < common::model->getNumObservations() );
  }

  void dpomdp::setInternalState(state x){
    this->internal = x;
  }

  bool dpomdp::isSound(double tolerance) const {
    // Check the initial state-distribution
    if(std::abs(this->start->sum() - 1) > tolerance) return false;

    // Check the dynamics model
    action a;
    observation z;
    number size, r;
    for(a=0; a<this->number_jactions; ++a) {
      auto _sum_ = this->dynamics[a][0];
      for(z=1; z<this->number_jobservations; ++z)
      _sum_ += this->dynamics[a][z];

      size = _sum_.rows();
      for(r=0; r<size; ++r)
      if( std::abs(_sum_.row(r).sum() - 1) > tolerance)  return false;
    }

    return true;
  }

  void dpomdp::setDynamicsGenerator(){
    number i;
    action a;
    state x, y;
    observation z;

    for(i=0, y=0; y<this->getNumStates(); ++y) for(z=0; z<this->getNumObservations(); ++z,++i)
      this->encoding.emplace(i, std::make_pair(y, z));

    for(x=0; x<this->getNumStates(); ++x){
      this->dynamics_generator.emplace(x, std::unordered_map<action, std::discrete_distribution<number>>());
      for(a=0; a<this->getNumActions(); ++a){
        std::vector<double> v;
        for(y=0; y<this->getNumStates(); ++y) for(z=0; z<this->getNumObservations(); ++z)
          v.push_back(this->getDynamics(x,a,z,y));

        this->dynamics_generator[x].emplace(a, std::discrete_distribution<number>(v.begin(), v.end()));
      }
    }

    std::vector<double> v;
    for(state x=0; x<this->getNumStates(); ++x) v.push_back((*this->start)[x]);
    this->start_generator = std::discrete_distribution<number>(v.begin(), v.end());
  }

  std::tuple<double,observation,state> dpomdp::getDynamicsGenerator(state x, action a){
    state y; observation z;
    std::tie(y,z) = this->encoding[ this->dynamics_generator[x][a](common::global_urng()) ];
    return std::make_tuple( this->getReward(x,a), z, y );
  }

  void dpomdp::setPlanningHorizon(number planning_horizon){
    this->planning_horizon = planning_horizon;
  }

  number dpomdp::getPlanningHorizon(){
    return this->planning_horizon;
  }
}
