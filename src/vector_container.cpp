#include "../include/utils/linear_algebra/vector_container.hpp"

namespace sdm{
  size_t hash_container::operator()(const std::shared_ptr<Vector>& arg) const{
    return std::hash<Vector>()(*arg);
  }

  bool equal_container::operator()(const std::shared_ptr<Vector>& left, const std::shared_ptr<Vector>& right) const{
    return (left == right) or std::equal_to<Vector>()(*left, *right);
  }

  vector_container::vector_container(){ }

  bool vector_container::contains(std::shared_ptr<Vector>& p_elem){
   auto iterator = this->find(p_elem);
   if( iterator == this->end() ) return false;
   p_elem = *iterator; return true;
  }
}

namespace std{

  size_t hash<sdm::Vector>::operator()(const sdm::Vector& value) const {
    std::vector<int> dvec;
    for(size_t i=0; i<value.size(); ++i)
      dvec.push_back( lround(100*value[i]) );
    return boost::hash_range(dvec.begin(), dvec.end());
  }

  bool equal_to<sdm::Vector>::operator()(const sdm::Vector& left, const sdm::Vector& right) const{
    // sdm::Vector gap = left; gap -= right; return gap.norm_1() < 0.001;
    for(sdm::number i = 0; i < left.size(); ++i){
      if(abs(left[i] - right[i]) > 0.0001) return false;
    }
    return true;
  }
}
