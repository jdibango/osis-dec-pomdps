#include "../include/algorithms/mdps/search/asymmetric_hsvi.hpp"

namespace sdm{

  //+---------------------------------------------------+//
  //               asymmetric_belief_state               //
  //+---------------------------------------------------+//
  template<typename ihistory_t, bool history>
  asymmetric_belief_state<ihistory_t, history>::asymmetric_belief_state(){
    this->h = new ihistory_t(0);
    this->b = common::model->getStart();
  }

  template<typename ihistory_t, bool history>
  asymmetric_belief_state<ihistory_t, history>::asymmetric_belief_state(const std::shared_ptr<Vector>& b, ihistory_t* h){
    this->b = b;
    this->h = h;
  }

  template<typename ihistory_t, bool history>
  asymmetric_belief_state<ihistory_t, history>::asymmetric_belief_state(const std::shared_ptr<asymmetric_belief_state>& s, action u, observation z, double& prob){
    this->h = s->next(common::model->getActionIndex(0, u), common::model->getObservationIndex(0, z));
    auto buz = std::make_tuple(s->b, u, z);
    auto weightedb = common::nextBelief(buz);
    prob = 0.0;
    if( (prob = weightedb->sum()) > 0 ){
      this->b = std::make_shared<Vector>(common::model->getNumStates());
      *this->b = *weightedb;
      *this->b /= prob;
      common::saveBeliefs(this->b);
    }
  }

  template<typename ihistory_t, bool history>
  ihistory_t* asymmetric_belief_state<ihistory_t, history>::next(action u, observation z){
    auto uz = std::make_pair(u, z);
    return this->h->expand(uz, true);
  }

  template<>
  observation_ihistory* asymmetric_belief_state<observation_ihistory, false>::next(action, observation z){
    return this->h->expand(z, true);
  }

  template<>
  observation_ihistory* asymmetric_belief_state<observation_ihistory, true>::next(action, observation z){
    return this->h->expand(z, true);
  }

  template<typename ihistory_t, bool history>
  asymmetric_belief_state<ihistory_t, history>::~asymmetric_belief_state(){}


  //+---------------------------------------------------+//
  //             hash_asymmetric_belief_state            //
  //+---------------------------------------------------+//

  template<typename ihistory_t, bool history>
  size_t hash_asymmetric_belief_state<ihistory_t, history>::operator()(const std::shared_ptr<asymmetric_belief_state<ihistory_t, history>>& arg) const{
    return history ? std::hash<Vector>()(*arg->b) ^ std::hash<ihistory_t*>()(arg->h) : std::hash<Vector>()(*arg->b);
  }

  //+---------------------------------------------------+//
  //            equal_asymmetric_belief_state            //
  //+---------------------------------------------------+//

  template<typename ihistory_t, bool history>
  bool equal_asymmetric_belief_state<ihistory_t, history>::operator()(const std::shared_ptr<asymmetric_belief_state<ihistory_t, history>>& left, const std::shared_ptr<asymmetric_belief_state<ihistory_t, history>>& right) const{
    return (left == right) or (history ? (std::equal_to<Vector>()(*left->b, *right->b) and (left->h == right->h)) : std::equal_to<Vector>()(*left->b, *right->b));
  }

  //+---------------------------------------------------+//
  //                asymmetric_upper_bound               //
  //+---------------------------------------------------+//
  template<typename ihistory_t, bool history, bool pwlc, bool pomdp>
  asymmetric_upper_bound<ihistory_t, history, pwlc, pomdp>::asymmetric_upper_bound(number length, number trials){
    for(horizon h=0; h<=common::model->getPlanningHorizon(); ++h){
      if( pwlc ) this->container_pwlc.push_back(upper_bound_pwlc_t());
      else this->container.push_back(upper_bound_t());
      this->default_value_function.emplace(h, std::make_shared<Vector>(common::model->getNumStates()));
    }

    if(pomdp) this->getPartiallyObservableMarkovDecisionProcessFiniteValueFunction(length, trials);
    else{
      this->getMarkovDecisionProcessFiniteValueFunction();
    }
  }
  template<typename ihistory_t, bool history, bool pwlc, bool pomdp>
  void asymmetric_upper_bound<ihistory_t, history, pwlc, pomdp>::getPartiallyObservableMarkovDecisionProcessFiniteValueFunction(number length, number trials){
    std::string s = "";
    this->pomdp_vf = std::make_shared<asymmetric_hsvi<ihistory_t, double, std::shared_ptr<asymmetric_belief_state<ihistory_t, false>>, action, false, false>>(length, trials, s);
    this->pomdp_vf->solve( common::model, common::model->getPlanningHorizon(), 0);
  }

  template<typename ihistory_t, bool history, bool pwlc, bool pomdp>
  void asymmetric_upper_bound<ihistory_t, history, pwlc, pomdp>::getMarkovDecisionProcessFiniteValueFunction(){
    double value;
    this->default_value_function.at(common::model->getPlanningHorizon())->init(0.0);
    for(horizon h=common::model->getPlanningHorizon(); h>0; --h){
      this->default_value_function.emplace(h-1, std::make_shared<Vector>(common::model->getNumStates()));
      this->default_value_function.at(h-1)->init(std::numeric_limits<double>::lowest());
      for(state x = 0; x < common::model->getNumStates(); ++x){
        for(action u = 0; u<common::model->getNumActions(); ++u){
          value = common::model->getReward(x,u);
          for(state y = 0; y < common::model->getNumStates(); ++y){
            value += common::model->getDiscount() * common::model->getTransitionProbability(x,u,y) * (*this->default_value_function.at(h))[y];
          }
          (*this->default_value_function.at(h-1))[x] = std::max(value,(*this->default_value_function.at(h-1))[x]);
        }
      }
    }
  }



  template<typename ihistory_t, bool history, bool pwlc, bool pomdp>
  double asymmetric_upper_bound<ihistory_t, history, pwlc, pomdp>::getDefaultValue(const state_t& s, horizon h) const{
    if(pomdp){
      auto hb = std::make_shared<asymmetric_belief_state<ihistory_t, false>>(s->b, s->h);
      return this->pomdp_vf->getValueAt(hb, h);
    }
    return (*s->b) ^ (*this->default_value_function.at(h));    
  }

  template<typename ihistory_t, bool history, bool pwlc, bool pomdp>
  asymmetric_upper_bound<ihistory_t, history, pwlc, pomdp>::~asymmetric_upper_bound(){}

  template<typename ihistory_t, bool history, bool pwlc, bool pomdp>
  double asymmetric_upper_bound<ihistory_t, history, pwlc, pomdp>::sawtooth(const state_t& arg0, const state_t& arg1, double value1, horizon h) const{
    double mdp0 = this->getDefaultValue(arg0, h);
    double mdp1 = this->getDefaultValue(arg1, h);

    double v = mdp0;
    for(state x=0; x<common::model->getNumStates(); ++x) if( (*arg1->b)[x]>0 )
    v = std::min(v, mdp0 + ((*arg0->b)[x])*(value1 - mdp1)/(*arg1->b)[x]);

    return v;
  }

  template<typename ihistory_t, bool history, bool pwlc, bool pomdp>
  void asymmetric_upper_bound<ihistory_t, history, pwlc, pomdp>::setValueAt(const state_t& s, double value, horizon h){
    if( pwlc ){
      if( this->container_pwlc.at(h).find(s->h) == this->container_pwlc.at(h).end() ){
        this->container_pwlc.at(h).emplace(s->h, upper_bound_t());
        this->container_pwlc.at(h).at(s->h).emplace(s, value);
      }
      else {
        auto it = this->container_pwlc.at(h)[s->h].begin();
        while( it != this->container_pwlc.at(h)[s->h].end() ){
          if( it->second >= this->sawtooth(it->first, s, value, h) ){
            it = this->container_pwlc.at(h)[s->h].erase(it);
          }
          else{
            it++;
          }
        }
        this->container_pwlc.at(h)[s->h].emplace(s, value);
      }
    }
    else{
      if( this->container.at(h).find(s) == this->container.at(h).end() ){
        this->container.at(h).emplace(s, value);
      }
      else {
        this->container.at(h)[s] = value;
      }
    }
  }

  template<typename ihistory_t, bool history, bool pwlc, bool pomdp>
  void asymmetric_upper_bound<ihistory_t, history, pwlc, pomdp>::setValueAt(const upper_bound_t& ub, horizon h){
    for(const auto& bv : ub){
      this->setValueAt(bv.first, bv.second, h);
    }
  }

  template<typename ihistory_t, bool history, bool pwlc, bool pomdp>
  double asymmetric_upper_bound<ihistory_t, history, pwlc, pomdp>::getValueAt(const state_t& s, horizon h) const{
    // TODO :: consider how to use pwlc and podmp both
    if( pwlc ){
      if( this->container_pwlc.at(h).find(s->h) == this->container_pwlc.at(h).end() ){
        return (*s->b) ^ (*this->default_value_function.at(h));
      }
      else{
        double ub = std::numeric_limits<double>::max();
        for(const auto& sv : this->container_pwlc.at(h).at(s->h)){
          ub = std::min(ub, this->sawtooth(s, sv.first, sv.second, h));
        }
        return ub;
      }
    }
    if( this->container.at(h).find(s) == this->container.at(h).end() )
    return this->getDefaultValue(s,h);
    return this->container.at(h).at(s);
  }

  template<typename ihistory_t, bool history, bool pwlc, bool pomdp>
  double asymmetric_upper_bound<ihistory_t, history, pwlc, pomdp>::getValueAt(const std::shared_ptr<light_occupancy_state<ihistory_t>>& s, horizon h){
    double value = 0.0;

    for(const auto& bp : s->container){
      auto b = std::make_shared<asymmetric_belief_state<ihistory_t, history>>(bp.first, s->ih0);
      value += this->getValueAt(b, h);
    }

    return value;
  }

  template<typename ihistory_t, bool history, bool pwlc, bool pomdp>
  action asymmetric_upper_bound<ihistory_t, history, pwlc, pomdp>::greedyActionAt(const state_t& s, horizon h){
    observation z;
    action u_max, u;
    double value, prob, max = std::numeric_limits<double>::lowest();

    for(u=0; u<common::model->getNumActions(); ++u){
      value = (*s->b) ^ common::model->getReward(u);

      for(z=0; z<common::model->getNumObservations(); ++z){
        auto s_next = std::make_shared<asymmetric_belief_state<ihistory_t, history>>(s, u, z, prob);
        if( prob > 0 ) value += common::model->getDiscount() * prob * this->getValueAt(s_next, h+1);
      }

      if( max < value ){
        max = value;
        u_max = u;
      }
    }

    this->setValueAt(s, max, h);
    return u_max;
  }

  template<typename ihistory_t, bool history, bool pwlc, bool pomdp>
  std::shared_ptr<light_decision_rule> asymmetric_upper_bound<ihistory_t, history, pwlc, pomdp>::greedyActionAt(const std::shared_ptr<light_occupancy_state<ihistory_t>>& s, horizon h){
    observation z;
    action u0, u1, u1_, u;
    auto ub_max = upper_bound_t();
    std::shared_ptr<light_decision_rule> jdr_tmp;
    std::shared_ptr<light_decision_rule> jdr_max;
    double tmp, prob, max = std::numeric_limits<double>::lowest();
    std::shared_ptr<asymmetric_belief_state<ihistory_t, history>> b;
    std::shared_ptr<asymmetric_belief_state<ihistory_t, history>> b_;

    for(u0=0; u0<common::model->getNumActions(0); ++u0){
      tmp = 0.0;
      auto ub_tmp = upper_bound_t();
      jdr_tmp = std::make_shared<light_decision_rule>(); jdr_tmp->setAction( u0 );

      for(const auto& bp : s->container){
        double tmp_, max_ = std::numeric_limits<double>::lowest();
        b = std::make_shared<asymmetric_belief_state<ihistory_t, history>>(bp.first, s->ih0);
        for(u1=0; u1<common::model->getNumActions(1); ++u1){
          u = common::model->getJointActionIndex(std::vector<action>{u0,u1});
          tmp_ = (*bp.first)^common::model->getReward(u);
          for(z=0; z<common::model->getNumObservations(); ++z){
            b_ = std::make_shared<asymmetric_belief_state<ihistory_t, history>>(b, u, z, prob);
            if(prob > 0) tmp_ += common::model->getDiscount() * prob * this->getValueAt(b_, h+1);
          }
          if(max_ < tmp_){
            u1_ = u1;
            max_ = tmp_;
          }
        }
        tmp += bp.second * max_;
        ub_tmp.emplace(b, max_);
        jdr_tmp->setValueAt(bp.first, u1_, 1.0);
      }

      if( max < tmp ){
        max = tmp;
        ub_max = ub_tmp;
        jdr_max = jdr_tmp;
      }
    }

    this->setValueAt(ub_max, h);
    return jdr_max;
  }


  template<typename ihistory_t, bool history, bool pwlc, bool pomdp>
  number asymmetric_upper_bound<ihistory_t, history, pwlc, pomdp>::size() const{
    number size = 0;
    for(horizon h=0; h<common::model->getPlanningHorizon(); ++h)
    size += this->size(h);
    return size;
  }

  template<typename ihistory_t, bool history, bool pwlc, bool pomdp>
  number asymmetric_upper_bound<ihistory_t, history, pwlc, pomdp>::size(horizon h) const{
    return pwlc ? this->container_pwlc.at(h).size() : this->container.at(h).size();
  }


  //+---------------------------------------------------+//
  //                asymmetric_lower_bound               //
  //+---------------------------------------------------+//
  template<>
  double asymmetric_lower_bound<observation_ihistory, double, false, false>::getDefault(horizon h){
    return common::model->getMinReward() * (common::model->getPlanningHorizon() - h);
  }

  template<>
  double asymmetric_lower_bound<observation_ihistory, double, true, false>::getDefault(horizon h){
    return common::model->getMinReward() * (common::model->getPlanningHorizon() - h);
  }

  template<>
  double asymmetric_lower_bound<action_observation_ihistory, double, false, false>::getDefault(horizon h){
    return common::model->getMinReward() * (common::model->getPlanningHorizon() - h);
  }

  template<>
  double asymmetric_lower_bound<action_observation_ihistory, double, true, false>::getDefault(horizon h){
    return common::model->getMinReward() * (common::model->getPlanningHorizon() - h);
  }

  template<>
  std::shared_ptr<Vector> asymmetric_lower_bound<observation_ihistory, std::shared_ptr<Vector>, false, true>::getDefault(horizon h){
    auto v = std::make_shared<Vector>(common::model->getNumStates());
    v->init( common::model->getMinReward() * (common::model->getPlanningHorizon() - h) );
    return v;
  }

  template<typename ihistory_t, typename value_t, bool history, bool pwlc>
  asymmetric_lower_bound<ihistory_t, value_t, history, pwlc>::asymmetric_lower_bound(){
    for(horizon h=0; h<=common::model->getPlanningHorizon(); ++h){
      if( pwlc ) this->container_pwlc.push_back(lower_bound_pwlc_t());
      else this->container.push_back(lower_bound_t());
      this->default_value_function.push_back( this->getDefault(h) );
    }
  }

  template<typename ihistory_t, typename value_t, bool history, bool pwlc>
  asymmetric_lower_bound<ihistory_t, value_t, history, pwlc>::~asymmetric_lower_bound(){}

  template<>
  double asymmetric_lower_bound<observation_ihistory, std::shared_ptr<Vector>, false, true>::getValueAt(const std::shared_ptr<asymmetric_belief_state<observation_ihistory, false>>& s, std::shared_ptr<Vector>& alpha, horizon h) const{
    alpha = std::make_shared<Vector>(common::model->getNumStates());

    if( this->container_pwlc.at(h).find(s->h) != this->container_pwlc.at(h).end() ){
      double lb = std::numeric_limits<double>::lowest();
      for(const auto& sv : this->container_pwlc.at(h).at(s->h)){
        auto value = (*s->b)^(*sv.second);
        if( lb < value ){
          lb = value;
          *alpha = *sv.second;
        }
      }

      return lb;
    }

    else{
      *alpha = *this->default_value_function.at(h);
    }

    return (*s->b)^(*alpha);
  }

  template<>
  action asymmetric_lower_bound<observation_ihistory, std::shared_ptr<Vector>, false, true>::greedyActionAt(const std::shared_ptr<asymmetric_belief_state<observation_ihistory, false>>& s, std::shared_ptr<Vector>& maxVector, horizon h){
    action uMax;
    double value, prob;
    double max = std::numeric_limits<double>::lowest();
    for(action u=0; u<common::model->getNumActions(); ++u){
      value = (*s->b) ^ common::model->getReward(u);
      auto tmp = std::make_shared<Vector>( common::model->getReward(u) );
      for(observation z=0; z<common::model->getNumObservations(); ++z){
        auto s_next = std::make_shared<asymmetric_belief_state<observation_ihistory, false>>(s, u, z, prob);
        if(prob > 0){
          std::shared_ptr<Vector> tmpZ = nullptr;
          value += common::model->getDiscount() * prob * this->getValueAt(s_next, tmpZ, h+1);
          *tmpZ = common::model->getDynamics(u, z) * (*tmpZ);
          *tmpZ = common::model->getDiscount() * (*tmpZ);
          *tmp += *tmpZ;
        }
      }

      if( max < value ){
        maxVector = tmp;
        max = value;
        uMax = u;
      }
    }

    return uMax;
  }

  template<>
  void asymmetric_lower_bound<observation_ihistory, std::shared_ptr<Vector>, false, true>::setValueAt(const std::shared_ptr<asymmetric_belief_state<observation_ihistory, false>>& s, std::shared_ptr<Vector>& value, horizon h){
    if( this->container_pwlc.at(h).find(s->h) == this->container_pwlc.at(h).end() ){
      this->container_pwlc.at(h).emplace(s->h, std::unordered_map<std::shared_ptr<asymmetric_belief_state<observation_ihistory, false>>, std::shared_ptr<Vector>, hash_asymmetric_belief_state<observation_ihistory, false>, equal_asymmetric_belief_state<observation_ihistory, false>>());
      this->container_pwlc.at(h).at(s->h).emplace(s, value);
    }
    else {
      auto it = this->container_pwlc.at(h)[s->h].begin();
      while( it != this->container_pwlc.at(h)[s->h].end() ){
        auto value1 = (*it->first->b)^(*value);
        auto value0 = (*it->first->b)^(*it->second);
        if( value0 <= value1 ){
          it = this->container_pwlc.at(h)[s->h].erase(it);
        }
        else{
          it++;
        }
      }
      this->container_pwlc.at(h)[s->h].emplace(s, value);
    }
  }

  template<>
  double asymmetric_lower_bound<observation_ihistory, double, false, false>::getValueAt(const std::shared_ptr<asymmetric_belief_state<observation_ihistory, false>>& s, double&, horizon h) const{
    if( this->container.at(h).find(s) == this->container.at(h).end() )
    return this->default_value_function.at(h);
    return this->container.at(h).at(s);
  }

  template<>
  double asymmetric_lower_bound<action_observation_ihistory, double, false, false>::getValueAt(const std::shared_ptr<asymmetric_belief_state<action_observation_ihistory, false>>& s, double&, horizon h) const{
    if( this->container.at(h).find(s) == this->container.at(h).end() )
    return this->default_value_function.at(h);
    return this->container.at(h).at(s);
  }

  template<>
  double asymmetric_lower_bound<observation_ihistory, double, true, false>::getValueAt(const std::shared_ptr<asymmetric_belief_state<observation_ihistory, true>>& s, double&, horizon h) const{
    if( this->container.at(h).find(s) == this->container.at(h).end() )
    return this->default_value_function.at(h);
    return this->container.at(h).at(s);
  }

  template<>
  double asymmetric_lower_bound<action_observation_ihistory, double, true, false>::getValueAt(const std::shared_ptr<asymmetric_belief_state<action_observation_ihistory, true>>& s, double&, horizon h) const{
    if( this->container.at(h).find(s) == this->container.at(h).end() )
    return this->default_value_function.at(h);
    return this->container.at(h).at(s);
  }

  template<>
  action asymmetric_lower_bound<observation_ihistory, double, false, false>::greedyActionAt(const std::shared_ptr<asymmetric_belief_state<observation_ihistory, false>>& s, double& max, horizon h){
    observation z;
    action u_max, u;
    double value, prob, tmp;
    max = std::numeric_limits<double>::lowest();
    for(u=0; u<common::model->getNumActions(); ++u){
      value = (*s->b) ^ common::model->getReward(u);
      for(z=0; z<common::model->getNumObservations(); ++z){
        auto s_next = std::make_shared<asymmetric_belief_state<observation_ihistory, false>>(s, u, z, prob);
        if(prob > 0) value += common::model->getDiscount() * prob * this->getValueAt(s_next, tmp, h+1);
      }

      if( max < value ){
        max = value;
        u_max = u;
      }
    }

    return u_max;
  }

  template<>
  action asymmetric_lower_bound<observation_ihistory, double, true, false>::greedyActionAt(const std::shared_ptr<asymmetric_belief_state<observation_ihistory, true>>& s, double& max, horizon h){
    observation z;
    action u_max, u;
    double value, prob, tmp;
    max = std::numeric_limits<double>::lowest();
    for(u=0; u<common::model->getNumActions(); ++u){
      value = (*s->b) ^ common::model->getReward(u);
      for(z=0; z<common::model->getNumObservations(); ++z){
        auto s_next = std::make_shared<asymmetric_belief_state<observation_ihistory, true>>(s, u, z, prob);
        if(prob > 0) value += common::model->getDiscount() * prob * this->getValueAt(s_next, tmp, h+1);
      }

      if( max < value ){
        max = value;
        u_max = u;
      }
    }

    return u_max;
  }

  template<>
  action asymmetric_lower_bound<action_observation_ihistory, double, true, false>::greedyActionAt(const std::shared_ptr<asymmetric_belief_state<action_observation_ihistory, true>>& s, double& max, horizon h){
    observation z;
    action u_max, u;
    double value, prob, tmp;
    max = std::numeric_limits<double>::lowest();
    for(u=0; u<common::model->getNumActions(); ++u){
      value = (*s->b) ^ common::model->getReward(u);
      for(z=0; z<common::model->getNumObservations(); ++z){
        auto s_next = std::make_shared<asymmetric_belief_state<action_observation_ihistory, true>>(s, u, z, prob);
        if(prob > 0) value += common::model->getDiscount() * prob * this->getValueAt(s_next, tmp, h+1);
      }

      if( max < value ){
        max = value;
        u_max = u;
      }
    }

    return u_max;
  }

  template<>
  action asymmetric_lower_bound<action_observation_ihistory, double, false, false>::greedyActionAt(const std::shared_ptr<asymmetric_belief_state<action_observation_ihistory, false>>& s, double& max, horizon h){
    observation z;
    action u_max, u;
    double value, prob, tmp;
    max = std::numeric_limits<double>::lowest();
    for(u=0; u<common::model->getNumActions(); ++u){
      value = (*s->b) ^ common::model->getReward(u);
      for(z=0; z<common::model->getNumObservations(); ++z){
        auto s_next = std::make_shared<asymmetric_belief_state<action_observation_ihistory, false>>(s, u, z, prob);
        if(prob > 0) value += common::model->getDiscount() * prob * this->getValueAt(s_next, tmp, h+1);
      }

      if( max < value ){
        max = value;
        u_max = u;
      }
    }

    return u_max;
  }

  template<>
  void asymmetric_lower_bound<observation_ihistory, double, false, false>::setValueAt(const std::shared_ptr<asymmetric_belief_state<observation_ihistory, false>>& s, double& value, horizon h){
    if( this->container.at(h).find(s) == this->container.at(h).end() ){
      this->container.at(h).emplace(s, value);
    }
    else {
      this->container.at(h)[s] = value;
    }
  }

  template<>
  void asymmetric_lower_bound<observation_ihistory, double, true, false>::setValueAt(const std::shared_ptr<asymmetric_belief_state<observation_ihistory, true>>& s, double& value, horizon h){
    if( this->container.at(h).find(s) == this->container.at(h).end() ){
      this->container.at(h).emplace(s, value);
    }
    else {
      this->container.at(h)[s] = value;
    }
  }

  template<>
  void asymmetric_lower_bound<action_observation_ihistory, double, true, false>::setValueAt(const std::shared_ptr<asymmetric_belief_state<action_observation_ihistory, true>>& s, double& value, horizon h){
    if( this->container.at(h).find(s) == this->container.at(h).end() ){
      this->container.at(h).emplace(s, value);
    }
    else {
      this->container.at(h)[s] = value;
    }
  }

  template<>
  void asymmetric_lower_bound<action_observation_ihistory, double, false, false>::setValueAt(const std::shared_ptr<asymmetric_belief_state<action_observation_ihistory, false>>& s, double& value, horizon h){
    if( this->container.at(h).find(s) == this->container.at(h).end() ){
      this->container.at(h).emplace(s, value);
    }
    else {
      this->container.at(h)[s] = value;
    }
  }

  template<typename ihistory_t, typename value_t, bool history, bool pwlc>
  void asymmetric_lower_bound<ihistory_t, value_t, history, pwlc>::setValueAt(const std::shared_ptr<asymmetric_belief_state<ihistory_t, history>>& s, horizon h){
    value_t value;
    this->greedyActionAt(s, value, h);
    this->setValueAt(s, value, h);
  }


  template<typename ihistory_t, typename value_t, bool history, bool pwlc>
  double asymmetric_lower_bound<ihistory_t, value_t, history, pwlc>::getValueAt(const std::shared_ptr<light_occupancy_state<ihistory_t>>& s, lower_bound_t& v, horizon h) const{
    double value = 0.0;

    for(const auto& bp : s->container){
      value_t vf;
      auto b = std::make_shared<asymmetric_belief_state<ihistory_t, history>>(bp.first, s->ih0);
      value += this->getValueAt(b, vf, h);
      v.emplace(b, vf);
    }

    return value;
  }

  template<>
  std::shared_ptr<light_decision_rule> asymmetric_lower_bound<observation_ihistory, double, true, false>::greedyActionAt(const std::shared_ptr<light_occupancy_state<observation_ihistory>>& s, std::unordered_map<std::shared_ptr<asymmetric_belief_state<observation_ihistory, true>>, double, hash_asymmetric_belief_state<observation_ihistory, true>, equal_asymmetric_belief_state<observation_ihistory, true>>& lb_max, horizon h){
    observation z;
    action u0, u1, u1_, u;
    std::shared_ptr<light_decision_rule> jdr_tmp;
    std::shared_ptr<light_decision_rule> jdr_max;
    double empty, tmp, prob, max = std::numeric_limits<double>::lowest();
    std::shared_ptr<asymmetric_belief_state<observation_ihistory, true>> b;
    std::shared_ptr<asymmetric_belief_state<observation_ihistory, true>> b_;

    for(u0=0; u0<common::model->getNumActions(0); ++u0){
      tmp = 0.0;
      jdr_tmp = std::make_shared<light_decision_rule>(); jdr_tmp->setAction( u0 );
      std::unordered_map<std::shared_ptr<asymmetric_belief_state<observation_ihistory, true>>, double, hash_asymmetric_belief_state<observation_ihistory, true>, equal_asymmetric_belief_state<observation_ihistory, true>> lb_tmp;

      for(const auto& bp : s->container){
        double tmp_, max_ = std::numeric_limits<double>::lowest();
        b = std::make_shared<asymmetric_belief_state<observation_ihistory, true>>(bp.first, s->ih0);
        for(u1=0; u1<common::model->getNumActions(1); ++u1){
          u = common::model->getJointActionIndex(std::vector<action>{u0,u1});
          tmp_ = (*bp.first)^common::model->getReward(u);
          for(z=0; z<common::model->getNumObservations(); ++z){
            b_ = std::make_shared<asymmetric_belief_state<observation_ihistory, true>>(b, u, z, prob);
            if(prob > 0) tmp_ += common::model->getDiscount() * prob * this->getValueAt(b_, empty, h+1);
          }

          if(max_ < tmp_){
            u1_ = u1;
            max_ = tmp_;
          }
        }

        tmp += bp.second * max_;
        lb_tmp.emplace(b, max_);
        jdr_tmp->setValueAt(bp.first, u1_, 1.0);
      }

      if( max < tmp ){
        max = tmp;
        lb_max = lb_tmp;
        jdr_max = jdr_tmp;
      }
    }

    return jdr_max;
  }

  template<>
  std::shared_ptr<light_decision_rule> asymmetric_lower_bound<action_observation_ihistory, double, true, false>::greedyActionAt(const std::shared_ptr<light_occupancy_state<action_observation_ihistory>>& s, std::unordered_map<std::shared_ptr<asymmetric_belief_state<action_observation_ihistory, true>>, double, hash_asymmetric_belief_state<action_observation_ihistory, true>, equal_asymmetric_belief_state<action_observation_ihistory, true>>& lb_max, horizon h){
    observation z;
    action u0, u1, u1_, u;
    std::shared_ptr<light_decision_rule> jdr_tmp;
    std::shared_ptr<light_decision_rule> jdr_max;
    double empty, tmp, prob, max = std::numeric_limits<double>::lowest();
    std::shared_ptr<asymmetric_belief_state<action_observation_ihistory, true>> b;
    std::shared_ptr<asymmetric_belief_state<action_observation_ihistory, true>> b_;

    for(u0=0; u0<common::model->getNumActions(0); ++u0){
      tmp = 0.0;
      jdr_tmp = std::make_shared<light_decision_rule>(); jdr_tmp->setAction( u0 );
      std::unordered_map<std::shared_ptr<asymmetric_belief_state<action_observation_ihistory, true>>, double, hash_asymmetric_belief_state<action_observation_ihistory, true>, equal_asymmetric_belief_state<action_observation_ihistory, true>> lb_tmp;

      for(const auto& bp : s->container){
        double tmp_, max_ = std::numeric_limits<double>::lowest();
        b = std::make_shared<asymmetric_belief_state<action_observation_ihistory, true>>(bp.first, s->ih0);
        for(u1=0; u1<common::model->getNumActions(1); ++u1){
          u = common::model->getJointActionIndex(std::vector<action>{u0,u1});
          tmp_ = (*bp.first)^common::model->getReward(u);
          for(z=0; z<common::model->getNumObservations(); ++z){
            b_ = std::make_shared<asymmetric_belief_state<action_observation_ihistory, true>>(b, u, z, prob);
            if(prob > 0) tmp_ += common::model->getDiscount() * prob * this->getValueAt(b_, empty, h+1);
          }

          if(max_ < tmp_){
            u1_ = u1;
            max_ = tmp_;
          }
        }

        tmp += bp.second * max_;
        lb_tmp.emplace(b, max_);
        jdr_tmp->setValueAt(bp.first, u1_, 1.0);
      }

      if( max < tmp ){
        max = tmp;
        lb_max = lb_tmp;
        jdr_max = jdr_tmp;
      }
    }

    return jdr_max;
  }


  template<>
  void asymmetric_lower_bound<observation_ihistory, double, true, false>::setValueAt(const std::shared_ptr<light_occupancy_state<observation_ihistory>>& s, horizon h){
    std::unordered_map<std::shared_ptr<asymmetric_belief_state<observation_ihistory, true>>, double, hash_asymmetric_belief_state<observation_ihistory, true>, equal_asymmetric_belief_state<observation_ihistory, true>> vf;
    this->greedyActionAt(s, vf, h);
    for(auto& bv : vf){
      this->setValueAt(bv.first, bv.second, h);
    }
  }

  template<>
  void asymmetric_lower_bound<action_observation_ihistory, double, true, false>::setValueAt(const std::shared_ptr<light_occupancy_state<action_observation_ihistory>>& s, horizon h){
    std::unordered_map<std::shared_ptr<asymmetric_belief_state<action_observation_ihistory, true>>, double, hash_asymmetric_belief_state<action_observation_ihistory, true>, equal_asymmetric_belief_state<action_observation_ihistory, true>> vf;
    this->greedyActionAt(s, vf, h);
    for(auto& bv : vf){
      this->setValueAt(bv.first, bv.second, h);
    }
  }

  template<>
  void asymmetric_lower_bound<observation_ihistory, std::shared_ptr<Vector>, false, true>::setValueAt(const std::shared_ptr<light_occupancy_state<observation_ihistory>>&, horizon ){
    // Nothing to do here
  }


  template<typename ihistory_t, typename value_t, bool history, bool pwlc>
  void asymmetric_lower_bound<ihistory_t, value_t, history, pwlc>::pruning(horizon){}

  template<typename ihistory_t, typename value_t, bool history, bool pwlc>
  number asymmetric_lower_bound<ihistory_t, value_t, history, pwlc>::size() const{
    number size = 0;

    for(horizon h=0; h<common::model->getPlanningHorizon(); ++h){
      size += this->size(h);
    }

    return size;
  }

  template<typename ihistory_t, typename value_t, bool history, bool pwlc>
  number asymmetric_lower_bound<ihistory_t, value_t, history, pwlc>::size(horizon h) const{
    return pwlc ? this->container_pwlc.at(h).size() : this->container.at(h).size();
  }


  //+---------------------------------------------------+//
  //                    asymmetric_hsvi                  //
  //+---------------------------------------------------+//
  template<typename ihistory_t, typename value_t, typename ostate_t, typename oaction_t, bool history, bool pwlc, bool pomdp>
  asymmetric_hsvi<ihistory_t, value_t, ostate_t, oaction_t, history, pwlc, pomdp>::~asymmetric_hsvi(){}

  template<typename ihistory_t, typename value_t, typename ostate_t, typename oaction_t, bool history, bool pwlc, bool pomdp>
  asymmetric_hsvi<ihistory_t, value_t, ostate_t, oaction_t, history, pwlc, pomdp>::asymmetric_hsvi(number length_limit, number trials, std::string results){
    limited_length::setLengthLimit(length_limit);
    this->trials = trials;
    this->save(results);
  }

  template<typename ihistory_t, typename value_t, typename ostate_t, typename oaction_t, bool history, bool pwlc, bool pomdp>
  double asymmetric_hsvi<ihistory_t, value_t, ostate_t, oaction_t, history, pwlc, pomdp>::getValueAt(const ostate_t& s, horizon h) const{
    return this->ub->getValueAt(s, h);
  }

  template<typename ihistory_t, typename value_t, typename ostate_t, typename oaction_t, bool history, bool pwlc, bool pomdp>
  void asymmetric_hsvi<ihistory_t, value_t, ostate_t, oaction_t, history, pwlc, pomdp>::explore(const ostate_t& s, horizon h){
    action a;
    double prob;
    observation z;

    #ifdef DEBUG
    this->print(s, h, false);
    #else
    this->save(s, h);
    #endif

    if( !this->stop(s, h) ){
      a = this->ub->greedyActionAt(s, h);
      z = this->getGreedyObservation(s, a, h);
      auto s_next = std::make_shared<asymmetric_belief_state<ihistory_t, history>>(s, a, z, prob);
      this->explore(s_next, h+1);
      this->ub->greedyActionAt(s, h);
      this->lb->setValueAt(s, h);
    }

    #ifdef DEBUG
    this->print(s, h, true);
    #endif
  }

  template<>
  bool asymmetric_hsvi<observation_ihistory, double, std::shared_ptr<light_occupancy_state<observation_ihistory>>, std::shared_ptr<light_decision_rule>, true, false>::stop(const std::shared_ptr<light_occupancy_state<observation_ihistory>>& s, horizon h) const{
    std::unordered_map<std::shared_ptr<asymmetric_belief_state<observation_ihistory, true>>, double, hash_asymmetric_belief_state<observation_ihistory, true>, equal_asymmetric_belief_state<observation_ihistory, true>> v_lb;
    return this->trial >= this->trials || this->ub->getValueAt(s,h) - this->lb->getValueAt(s,v_lb,h) <= 2 * this->epsilon / (std::pow(common::model->getDiscount(),h));
  }

  template<>
  bool asymmetric_hsvi<action_observation_ihistory, double, std::shared_ptr<light_occupancy_state<action_observation_ihistory>>, std::shared_ptr<light_decision_rule>, true, false>::stop(const std::shared_ptr<light_occupancy_state<action_observation_ihistory>>& s, horizon h) const{
    auto vlb = std::unordered_map<std::shared_ptr<asymmetric_belief_state<action_observation_ihistory, true>>, double, hash_asymmetric_belief_state<action_observation_ihistory, true>, equal_asymmetric_belief_state<action_observation_ihistory, true>>();
    return this->trial >= this->trials || this->ub->getValueAt(s,h) - this->lb->getValueAt(s,vlb,h) <= 2 * this->epsilon / (std::pow(common::model->getDiscount(),h));
  }

  template<>
  bool asymmetric_hsvi<action_observation_ihistory, double, std::shared_ptr<light_occupancy_state<action_observation_ihistory>>, std::shared_ptr<light_decision_rule>, true, false, true>::stop(const std::shared_ptr<light_occupancy_state<action_observation_ihistory>>& s, horizon h) const{
    auto vlb = std::unordered_map<std::shared_ptr<asymmetric_belief_state<action_observation_ihistory, true>>, double, hash_asymmetric_belief_state<action_observation_ihistory, true>, equal_asymmetric_belief_state<action_observation_ihistory, true>>();
    return this->trial >= this->trials || this->ub->getValueAt(s,h) - this->lb->getValueAt(s,vlb,h) <= 2 * this->epsilon / (std::pow(common::model->getDiscount(),h));
  }

  template<>
  observation asymmetric_hsvi<observation_ihistory, double, std::shared_ptr<light_occupancy_state<observation_ihistory>>, std::shared_ptr<light_decision_rule>, true, false>::getGreedyObservation(const std::shared_ptr<light_occupancy_state<observation_ihistory>>& s, const std::shared_ptr<light_decision_rule>& a, horizon h) const{
    observation z_max, z;
    double value, max = std::numeric_limits<double>::lowest();
    std::unordered_map<std::shared_ptr<asymmetric_belief_state<observation_ihistory, true>>, double, hash_asymmetric_belief_state<observation_ihistory, true>, equal_asymmetric_belief_state<observation_ihistory, true>> v_default;

    for(z=0; z<common::model->getNumObservations(0); ++z){
      auto uz = std::make_pair(a->getAction(), z);
      auto s_next = s->next(uz, a);
      if( s_next->prob > 0 ){
        value = common::model->getDiscount() * s_next->prob * (this->ub->getValueAt(s_next, h+1) - this->lb->getValueAt(s_next, v_default, h+1));
        if( max < value ){
          max = value;
          z_max = z;
        }
      }
    }

    return z_max;
  }

  template<>
  observation asymmetric_hsvi<action_observation_ihistory, double, std::shared_ptr<light_occupancy_state<action_observation_ihistory>>, std::shared_ptr<light_decision_rule>, true, false>::getGreedyObservation(const std::shared_ptr<light_occupancy_state<action_observation_ihistory>>& s, const std::shared_ptr<light_decision_rule>& a, horizon h) const{
    observation z_max, z;
    double value, max = std::numeric_limits<double>::lowest();
    std::unordered_map<std::shared_ptr<asymmetric_belief_state<action_observation_ihistory, true>>, double, hash_asymmetric_belief_state<action_observation_ihistory, true>, equal_asymmetric_belief_state<action_observation_ihistory, true>> v_default;

    for(z=0; z<common::model->getNumObservations(0); ++z){
      auto uz = std::make_pair(a->getAction(), z);
      auto s_next = s->next(uz, a);
      if( s_next->prob > 0 ){
        value = common::model->getDiscount() * s_next->prob * (this->ub->getValueAt(s_next, h+1) - this->lb->getValueAt(s_next, v_default, h+1));
        if( max < value ){
          max = value;
          z_max = z;
        }
      }
    }

    return z_max;
  }

  template<>
  observation asymmetric_hsvi<action_observation_ihistory, double, std::shared_ptr<light_occupancy_state<action_observation_ihistory>>, std::shared_ptr<light_decision_rule>, true, false, true>::getGreedyObservation(const std::shared_ptr<light_occupancy_state<action_observation_ihistory>>& s, const std::shared_ptr<light_decision_rule>& a, horizon h) const{
    observation z_max, z;
    double value, max = std::numeric_limits<double>::lowest();
    std::unordered_map<std::shared_ptr<asymmetric_belief_state<action_observation_ihistory, true>>, double, hash_asymmetric_belief_state<action_observation_ihistory, true>, equal_asymmetric_belief_state<action_observation_ihistory, true>> v_default;

    for(z=0; z<common::model->getNumObservations(0); ++z){
      auto uz = std::make_pair(a->getAction(), z);
      auto s_next = s->next(uz, a);
      if( s_next->prob > 0 ){
        value = common::model->getDiscount() * s_next->prob * (this->ub->getValueAt(s_next, h+1) - this->lb->getValueAt(s_next, v_default, h+1));
        if( max < value ){
          max = value;
          z_max = z;
        }
      }
    }

    return z_max;
  }

  template<>
  void asymmetric_hsvi<observation_ihistory, double, std::shared_ptr<light_occupancy_state<observation_ihistory>>, std::shared_ptr<light_decision_rule>, true, false>::print(const std::shared_ptr<light_occupancy_state<observation_ihistory>>& s, horizon h, bool color){
    std::unordered_map<std::shared_ptr<asymmetric_belief_state<observation_ihistory, true>>, double, hash_asymmetric_belief_state<observation_ihistory, true>, equal_asymmetric_belief_state<observation_ihistory, true>> v_lb;
    auto sl = this->lb->size(h);
    auto su = this->ub->size(h);
    auto ub = this->ub->getValueAt(s,  h);
    auto lb = this->lb->getValueAt(s, v_lb, h);
    auto dur = std::chrono::duration_cast<std::chrono::duration<double>>(std::chrono::high_resolution_clock::now() - this->start_time);
    if( color ) std::cout << "\033[1m\033[36m  <trial=\"" << this->trial << "\" depth=\"" << h << "\"  lb["<< h <<"]=\"" << lb  <<  "\" size["<< h <<"]=\"" << sl  <<  "\" ub["<< h <<"]=\"" << ub <<  "\" size["<< h <<"]=\"" << su << "\" stop=\"" << (ub - lb > 2*this->epsilon / std::pow(common::model->getDiscount(), h) ? "no" : "yes")<<"\" Time cost:"<< dur.count() << "\" /> \033[0m" << std::endl;
    else std::cout << "\033[1m\033[35m  <trial=\"" << this->trial << "\" depth=\"" << h << "\"  lb["<< h <<"]=\"" << lb  <<  "\" size["<< h <<"]=\"" << sl  <<  "\" ub["<< h <<"]=\"" << ub <<  "\" size["<< h <<"]=\"" << su << "\" stop=\"" << (ub - lb > 2*this->epsilon / std::pow(common::model->getDiscount(), h) ? "no" : "yes")<<"\" Time cost:"<< dur.count() << "\" /> \033[0m" << std::endl;
  }

  template<>
  void asymmetric_hsvi<action_observation_ihistory, double, std::shared_ptr<light_occupancy_state<action_observation_ihistory>>, std::shared_ptr<light_decision_rule>, true, false>::print(const std::shared_ptr<light_occupancy_state<action_observation_ihistory>>& s, horizon h, bool color){
    std::unordered_map<std::shared_ptr<asymmetric_belief_state<action_observation_ihistory, true>>, double, hash_asymmetric_belief_state<action_observation_ihistory, true>, equal_asymmetric_belief_state<action_observation_ihistory, true>> v_lb;
    auto sl = this->lb->size(h);
    auto su = this->ub->size(h);
    auto ub = this->ub->getValueAt(s,  h);
    auto lb = this->lb->getValueAt(s, v_lb, h);
    auto dur = std::chrono::duration_cast<std::chrono::duration<double>>(std::chrono::high_resolution_clock::now() - this->start_time);
    if( color ) std::cout << "\033[1m\033[36m  <trial=\"" << this->trial << "\" depth=\"" << h << "\"  lb["<< h <<"]=\"" << lb  <<  "\" size["<< h <<"]=\"" << sl  <<  "\" ub["<< h <<"]=\"" << ub <<  "\" size["<< h <<"]=\"" << su << "\" stop=\"" << (ub - lb > 2*this->epsilon / std::pow(common::model->getDiscount(), h) ? "no" : "yes")<<"\" Time cost:"<< dur.count() << "\" /> \033[0m" << std::endl;
    else std::cout << "\033[1m\033[35m  <trial=\"" << this->trial << "\" depth=\"" << h << "\"  lb["<< h <<"]=\"" << lb  <<  "\" size["<< h <<"]=\"" << sl  <<  "\" ub["<< h <<"]=\"" << ub <<  "\" size["<< h <<"]=\"" << su << "\" stop=\"" << (ub - lb > 2*this->epsilon / std::pow(common::model->getDiscount(), h) ? "no" : "yes")<<"\" Time cost:"<< dur.count() << "\" /> \033[0m" << std::endl;
  }

  template<>
  void asymmetric_hsvi<action_observation_ihistory, double, std::shared_ptr<light_occupancy_state<action_observation_ihistory>>, std::shared_ptr<light_decision_rule>, true, false, true>::print(const std::shared_ptr<light_occupancy_state<action_observation_ihistory>>& s, horizon h, bool color){
    std::unordered_map<std::shared_ptr<asymmetric_belief_state<action_observation_ihistory, true>>, double, hash_asymmetric_belief_state<action_observation_ihistory, true>, equal_asymmetric_belief_state<action_observation_ihistory, true>> v_lb;
    auto sl = this->lb->size(h);
    auto su = this->ub->size(h);
    auto ub = this->ub->getValueAt(s,  h);
    auto lb = this->lb->getValueAt(s, v_lb, h);
    auto dur = std::chrono::duration_cast<std::chrono::duration<double>>(std::chrono::high_resolution_clock::now() - this->start_time);
    if( color ) std::cout << "\033[1m\033[36m  <trial=\"" << this->trial << "\" depth=\"" << h << "\"  lb["<< h <<"]=\"" << lb  <<  "\" size["<< h <<"]=\"" << sl  <<  "\" ub["<< h <<"]=\"" << ub <<  "\" size["<< h <<"]=\"" << su << "\" stop=\"" << (ub - lb > 2*this->epsilon / std::pow(common::model->getDiscount(), h) ? "no" : "yes")<<"\" Time cost:"<< dur.count() << "\" /> \033[0m" << std::endl;
    else std::cout << "\033[1m\033[35m  <trial=\"" << this->trial << "\" depth=\"" << h << "\"  lb["<< h <<"]=\"" << lb  <<  "\" size["<< h <<"]=\"" << sl  <<  "\" ub["<< h <<"]=\"" << ub <<  "\" size["<< h <<"]=\"" << su << "\" stop=\"" << (ub - lb > 2*this->epsilon / std::pow(common::model->getDiscount(), h) ? "no" : "yes")<<"\" Time cost:"<< dur.count() << "\" /> \033[0m" << std::endl;
  }


   template<>
  void asymmetric_hsvi<observation_ihistory, double, std::shared_ptr<light_occupancy_state<observation_ihistory>>, std::shared_ptr<light_decision_rule>, true, false>::save(const std::shared_ptr<light_occupancy_state<observation_ihistory>>& s, horizon h){
    std::unordered_map<std::shared_ptr<asymmetric_belief_state<observation_ihistory, true>>, double, hash_asymmetric_belief_state<observation_ihistory, true>, equal_asymmetric_belief_state<observation_ihistory, true>> v_lb;
    auto dur = std::chrono::duration_cast<std::chrono::duration<double>>(std::chrono::high_resolution_clock::now() - this->start_time);
    if(h == 0) this->perf_out << this->trial<< "," << h << ","<< this->ub->getValueAt(s, h) << "," << this->lb->getValueAt(s, v_lb, h) << ","<< this->ub->size() << ","<< this->lb->size() << ","<< dur.count() << std::endl;
    if(dur.count() > 18000) exit(0);
  }

  template<>
  void asymmetric_hsvi<action_observation_ihistory, double, std::shared_ptr<light_occupancy_state<action_observation_ihistory>>, std::shared_ptr<light_decision_rule>, true, false>::save(const std::shared_ptr<light_occupancy_state<action_observation_ihistory>>& s, horizon h){
    std::unordered_map<std::shared_ptr<asymmetric_belief_state<action_observation_ihistory, true>>, double, hash_asymmetric_belief_state<action_observation_ihistory, true>, equal_asymmetric_belief_state<action_observation_ihistory, true>> v_lb;
    auto dur = std::chrono::duration_cast<std::chrono::duration<double>>(std::chrono::high_resolution_clock::now() - this->start_time);
    if(h == 0) this->perf_out << this->trial<< "," << h << ","<< this->ub->getValueAt(s, h) << "," << this->lb->getValueAt(s, v_lb, h) << ","<< this->ub->size() << ","<< this->lb->size() << ","<< dur.count() << std::endl;
    if(dur.count() > 18000) exit(0);
  }

  template<>
  void asymmetric_hsvi<action_observation_ihistory, double, std::shared_ptr<light_occupancy_state<action_observation_ihistory>>, std::shared_ptr<light_decision_rule>, true, false, true>::save(const std::shared_ptr<light_occupancy_state<action_observation_ihistory>>& s, horizon h){
    std::unordered_map<std::shared_ptr<asymmetric_belief_state<action_observation_ihistory, true>>, double, hash_asymmetric_belief_state<action_observation_ihistory, true>, equal_asymmetric_belief_state<action_observation_ihistory, true>> v_lb;
    auto dur = std::chrono::duration_cast<std::chrono::duration<double>>(std::chrono::high_resolution_clock::now() - this->start_time);
    if(h == 0) this->perf_out << this->trial<< "," << h << ","<< this->ub->getValueAt(s, h) << "," << this->lb->getValueAt(s, v_lb, h) << ","<< this->ub->size() << ","<< this->lb->size() << ","<< dur.count() << std::endl;
    if(dur.count() > 18000) exit(0);
  }

  template<typename ihistory_t, typename value_t, typename ostate_t, typename oaction_t, bool history, bool pwlc, bool pomdp>
  void asymmetric_hsvi<ihistory_t, value_t, ostate_t, oaction_t, history, pwlc, pomdp>::save(const ostate_t& s, horizon h){
    value_t v_lb = this->lb->getDefault(h);
    auto dur = std::chrono::duration_cast<std::chrono::duration<double>>(std::chrono::high_resolution_clock::now() - this->start_time);
    if(h == 0) this->perf_out << this->trial<< "," << h << ","<< this->ub->getValueAt(s, h) << "," << this->lb->getValueAt(s, v_lb, h) << ","<< this->ub->size() << ","<< this->lb->size() << ","<< dur.count() << std::endl;
    if(dur.count() > 18000) exit(0);
  }


  template<>
  void asymmetric_hsvi<observation_ihistory, double, std::shared_ptr<light_occupancy_state<observation_ihistory>>, std::shared_ptr<light_decision_rule>, true, false>::explore(const std::shared_ptr<light_occupancy_state<observation_ihistory>>& s, horizon h){
    #ifdef DEBUG
    this->print(s, h, false);
    #else
    this->save(s, h);
    #endif

    if( !this->stop(s, h) ){
      auto a = this->ub->greedyActionAt(s, h);
      auto z = this->getGreedyObservation(s, a, h);
      auto uz = std::make_pair(a->getAction(), z);
      auto s_next = s->next(uz, a);
      this->explore(s_next, h+1);
      this->ub->greedyActionAt(s, h);
      this->lb->setValueAt(s, h);
    }

    #ifdef DEBUG
    this->print(s, h, true);
    #endif
  }

  template<>
  void asymmetric_hsvi<action_observation_ihistory, double, std::shared_ptr<light_occupancy_state<action_observation_ihistory>>, std::shared_ptr<light_decision_rule>, true, false>::explore(const std::shared_ptr<light_occupancy_state<action_observation_ihistory>>& s, horizon h){
    #ifdef DEBUG
    this->print(s, h, false);
    #else
    this->save(s, h);
    #endif

    if( !this->stop(s, h) ){
      auto a = this->ub->greedyActionAt(s, h);
      auto z = this->getGreedyObservation(s, a, h);
      auto uz = std::make_pair(a->getAction(), z);
      auto s_next = s->next(uz, a);
      this->explore(s_next, h+1);
      this->ub->greedyActionAt(s, h);
      this->lb->setValueAt(s, h);
    }

    #ifdef DEBUG
    this->print(s, h, true);
    #endif
  }

  template<>
  void asymmetric_hsvi<action_observation_ihistory, double, std::shared_ptr<light_occupancy_state<action_observation_ihistory>>, std::shared_ptr<light_decision_rule>, true, false, true>::explore(const std::shared_ptr<light_occupancy_state<action_observation_ihistory>>& s, horizon h){
    #ifdef DEBUG
    this->print(s, h, false);
    #else
    this->save(s, h);
    #endif

    if( !this->stop(s, h) ){
      auto a = this->ub->greedyActionAt(s, h);
      auto z = this->getGreedyObservation(s, a, h);
      auto uz = std::make_pair(a->getAction(), z);
      auto s_next = s->next(uz, a);
      this->explore(s_next, h+1);
      this->ub->greedyActionAt(s, h);
      this->lb->setValueAt(s, h);
    }

    #ifdef DEBUG
    this->print(s, h, true);
    #endif
  }


  template<typename ihistory_t, typename value_t, typename ostate_t, typename oaction_t, bool history, bool pwlc, bool pomdp>
  void asymmetric_hsvi<ihistory_t, value_t, ostate_t, oaction_t, history, pwlc, pomdp>::solve(const std::shared_ptr<World>& world, horizon planning_horizon, double epsilon, double){
    this->epsilon = epsilon; common::error = epsilon;
    common::model = std::static_pointer_cast<dpomdp>(world);
    common::model->setPlanningHorizon(planning_horizon);
    this->ub = std::make_shared<asymmetric_upper_bound<ihistory_t, history, pwlc, pomdp>>(limited_length::getLengthLimit(), this->trials);
    this->lb = std::make_shared<asymmetric_lower_bound<ihistory_t, value_t, history, pwlc>>();

    this->trial = 0;
    this->start = this->getStart();
    this->start_time = std::chrono::high_resolution_clock::now();
    do{
      this->explore(this->start, 0);
      this->trial++;
    }while( !this->stop(this->start ,0) );
    this->save(this->start, 0);
  }

  template<typename ihistory_t, typename value_t, typename ostate_t, typename oaction_t, bool history, bool pwlc, bool pomdp>
  bool asymmetric_hsvi<ihistory_t, value_t, ostate_t, oaction_t, history, pwlc, pomdp>::stop(const ostate_t& s, horizon h) const{
    value_t vlb = this->lb->getDefault(h);
    return this->trial >= this->trials || this->ub->getValueAt(s,h) - this->lb->getValueAt(s,vlb,h) <= 2 * this->epsilon / (std::pow(common::model->getDiscount(),h));
  }

  template<typename ihistory_t, typename value_t, typename ostate_t, typename oaction_t, bool history, bool pwlc, bool pomdp>
  observation asymmetric_hsvi<ihistory_t, value_t, ostate_t, oaction_t, history, pwlc, pomdp>::getGreedyObservation(const ostate_t& s, const oaction_t& u, horizon h) const{
    observation zMax;
    value_t v_default = this->lb->getDefault(h);
    double prob, value, max = std::numeric_limits<double>::lowest();
    for(observation z=0; z<common::model->getNumObservations(); ++z){
      auto s_next = std::make_shared<asymmetric_belief_state<ihistory_t, history>>(s, u, z, prob);
      if( prob > 0 ){
        value = common::model->getDiscount() * prob * (this->ub->getValueAt(s_next, h+1) - this->lb->getValueAt(s_next, v_default, h+1));
        if( max < value ){
          max = value;
          zMax = z;
        }
      }
    }
    return zMax;
  }

  template<typename ihistory_t, typename value_t, typename ostate_t, typename oaction_t, bool history, bool pwlc, bool pomdp>
  void asymmetric_hsvi<ihistory_t, value_t, ostate_t, oaction_t, history, pwlc, pomdp>::print(const ostate_t& s, horizon h, bool color){
    auto sl = this->lb->size(h);
    auto su = this->ub->size(h);
    auto ub = this->ub->getValueAt(s,  h);
    value_t v_lb = this->lb->getDefault(h);
    auto lb = this->lb->getValueAt(s, v_lb, h);
    auto dur = std::chrono::duration_cast<std::chrono::duration<double>>(std::chrono::high_resolution_clock::now() - this->start_time);
    if( color ) std::cout << "\033[1m\033[36m  <trial=\"" << this->trial << "\" depth=\"" << h << "\"  lb["<< h <<"]=\"" << lb  <<  "\" size["<< h <<"]=\"" << sl  <<  "\" ub["<< h <<"]=\"" << ub <<  "\" size["<< h <<"]=\"" << su << "\" stop=\"" << (ub - lb > 2*this->epsilon / std::pow(common::model->getDiscount(), h) ? "no" : "yes")<<"\" Time cost:"<< dur.count() << "\" /> \033[0m" << std::endl;
    else std::cout << "\033[1m\033[35m  <trial=\"" << this->trial << "\" depth=\"" << h << "\"  lb["<< h <<"]=\"" << lb  <<  "\" size["<< h <<"]=\"" << sl  <<  "\" ub["<< h <<"]=\"" << ub <<  "\" size["<< h <<"]=\"" << su << "\" stop=\"" << (ub - lb > 2*this->epsilon / std::pow(common::model->getDiscount(), h) ? "no" : "yes")<<"\" Time cost:"<< dur.count() << "\" /> \033[0m" << std::endl;
  }

  template<typename ihistory_t, typename value_t, typename ostate_t, typename oaction_t, bool history, bool pwlc, bool pomdp>
  void asymmetric_hsvi<ihistory_t, value_t, ostate_t, oaction_t, history, pwlc, pomdp>::save(std::string results, bool save){
    if( save ){
      this->perf_out.open(results);
      this->perf_out << "trial"<< "," << "horizon" << ","<< "ub_value" << "," << "lb_value" << ", "<< "ub_size" << ","<< "lb_size" << ","<< "time" << std::endl;
    }
  }

 
  template<>
  std::shared_ptr<light_occupancy_state<observation_ihistory>> asymmetric_hsvi<observation_ihistory, double, std::shared_ptr<light_occupancy_state<observation_ihistory>>, std::shared_ptr<light_decision_rule>, true, false>::getStart(){
    return std::make_shared<light_occupancy_state<observation_ihistory>>();
  }

  template<>
  std::shared_ptr<asymmetric_belief_state<observation_ihistory, false>> asymmetric_hsvi<observation_ihistory, double, std::shared_ptr<asymmetric_belief_state<observation_ihistory, false>>, action, false, false>::getStart(){
    return std::make_shared<asymmetric_belief_state<observation_ihistory, false>>();
  }

  template<>
  std::shared_ptr<asymmetric_belief_state<observation_ihistory, false>> asymmetric_hsvi<observation_ihistory, double, std::shared_ptr<asymmetric_belief_state<observation_ihistory, false>>, action, false, false, true>::getStart(){
    return std::make_shared<asymmetric_belief_state<observation_ihistory, false>>();
  }

  template<>
  std::shared_ptr<asymmetric_belief_state<observation_ihistory, false>> asymmetric_hsvi<observation_ihistory, std::shared_ptr<Vector>, std::shared_ptr<asymmetric_belief_state<observation_ihistory, false>>, action, false, true>::getStart(){
    return std::make_shared<asymmetric_belief_state<observation_ihistory, false>>();
  }

  template<>
  std::shared_ptr<light_occupancy_state<action_observation_ihistory>> asymmetric_hsvi<action_observation_ihistory, double, std::shared_ptr<light_occupancy_state<action_observation_ihistory>>, std::shared_ptr<light_decision_rule>, true, false>::getStart(){
    return std::make_shared<light_occupancy_state<action_observation_ihistory>>();
  }

  template<>
  std::shared_ptr<light_occupancy_state<action_observation_ihistory>> asymmetric_hsvi<action_observation_ihistory, double, std::shared_ptr<light_occupancy_state<action_observation_ihistory>>, std::shared_ptr<light_decision_rule>, true, false, true>::getStart(){
    return std::make_shared<light_occupancy_state<action_observation_ihistory>>();
  }

  template<>
  std::shared_ptr<asymmetric_belief_state<action_observation_ihistory, false>> asymmetric_hsvi<action_observation_ihistory, double, std::shared_ptr<asymmetric_belief_state<action_observation_ihistory, false>>, action, false, false>::getStart(){
    return std::make_shared<asymmetric_belief_state<action_observation_ihistory, false>>();
  }

  template<>
  std::shared_ptr<asymmetric_belief_state<action_observation_ihistory, false>> asymmetric_hsvi<action_observation_ihistory, double, std::shared_ptr<asymmetric_belief_state<action_observation_ihistory, false>>, action, false, false, true>::getStart(){
    return std::make_shared<asymmetric_belief_state<action_observation_ihistory, false>>();
  }


  template class asymmetric_belief_state<observation_ihistory, true>;
  template class asymmetric_belief_state<observation_ihistory, false>;
  template class asymmetric_belief_state<action_observation_ihistory, true>;
  template class asymmetric_belief_state<action_observation_ihistory, false>;

  template struct hash_asymmetric_belief_state<observation_ihistory, true>;
  template struct hash_asymmetric_belief_state<observation_ihistory, false>;
  template struct hash_asymmetric_belief_state<action_observation_ihistory, true>;
  template struct hash_asymmetric_belief_state<action_observation_ihistory, false>;

  template struct equal_asymmetric_belief_state<observation_ihistory, true>;
  template struct equal_asymmetric_belief_state<observation_ihistory, false>;
  template struct equal_asymmetric_belief_state<action_observation_ihistory, true>;
  template struct equal_asymmetric_belief_state<action_observation_ihistory, false>;


  template class asymmetric_upper_bound<observation_ihistory, true, false>;
  template class asymmetric_upper_bound<observation_ihistory, false, true>;
  template class asymmetric_upper_bound<observation_ihistory, false, false>;
  template class asymmetric_upper_bound<observation_ihistory, false, false, true>;
  template class asymmetric_upper_bound<action_observation_ihistory, true, false>;
  template class asymmetric_upper_bound<action_observation_ihistory, true, false, true>;


  template class asymmetric_lower_bound<observation_ihistory, double, true, false>;
  template class asymmetric_lower_bound<observation_ihistory, double, false, false>;
  template class asymmetric_lower_bound<action_observation_ihistory, double, false, false>;
  template class asymmetric_lower_bound<action_observation_ihistory, double, true, false>;
  template class asymmetric_lower_bound<observation_ihistory, std::shared_ptr<Vector>, false, true>;

  template class asymmetric_hsvi<observation_ihistory, double, std::shared_ptr<asymmetric_belief_state<observation_ihistory, false>>, action, false, false>;
  template class asymmetric_hsvi<observation_ihistory, double, std::shared_ptr<asymmetric_belief_state<observation_ihistory, false>>, action, false, false, true>;
  template class asymmetric_hsvi<action_observation_ihistory, double, std::shared_ptr<asymmetric_belief_state<action_observation_ihistory, false>>, action, false, false>;
  template class asymmetric_hsvi<action_observation_ihistory, double, std::shared_ptr<asymmetric_belief_state<action_observation_ihistory, false>>, action, false, false, true>;
  template class asymmetric_hsvi<observation_ihistory, std::shared_ptr<Vector>, std::shared_ptr<asymmetric_belief_state<observation_ihistory, false>>, action, false, true>;
  template class asymmetric_hsvi<observation_ihistory, double, std::shared_ptr<light_occupancy_state<observation_ihistory>>, std::shared_ptr<light_decision_rule>, true, false>;
  template class asymmetric_hsvi<action_observation_ihistory, double, std::shared_ptr<light_occupancy_state<action_observation_ihistory>>, std::shared_ptr<light_decision_rule>, true, false>;
  template class asymmetric_hsvi<action_observation_ihistory, double, std::shared_ptr<light_occupancy_state<action_observation_ihistory>>, std::shared_ptr<light_decision_rule>, true, false, true>;

}
