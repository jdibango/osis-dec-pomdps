/*=============================================================================

==============================================================================*/
#include "../include/dpomdp/__state__.hpp"

namespace sdm{

  state __state__::getNumStates() const{
    return number_states;
  }

  void __state__::setNumStates(state number_states){
    this->number_states = number_states;
  }

  void __state__::setNumStates(const std::vector<std::string>& state_names){
    this->setNumStates( state_names.size() );

    state idx =0;
    for(idx=0; idx<this->number_states; ++idx)
    this->state_names_bimap.insert( name2index(state_names[idx], idx) );
  }

  state __state__::getStateIndex(const std::string& name){
    return this->state_names_bimap.empty() ? 0 : this->state_names_bimap.left.at(name);
  }

  std::string __state__::getStateName(state s){
    return this->state_names_bimap.empty() ? std::to_string(s) : this->state_names_bimap.right.at(s);
  }

}
