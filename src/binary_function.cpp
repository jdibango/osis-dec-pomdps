#include "../include/public/binary_function.hpp"

namespace sdm{
  template<typename S, typename A>
  BinaryFunction<S,A>::~BinaryFunction(){}

  template class BinaryFunction<State*, std::shared_ptr<Action>>;
  template class BinaryFunction<std::shared_ptr<State>, std::shared_ptr<Action>>;
}
