#include "../include/utils/backup_structures/asymmetric_point_set.hpp"

namespace sdm{

  asymmetric_point_set::~asymmetric_point_set(){}

  asymmetric_point_set::asymmetric_point_set(){}

  double asymmetric_point_set::getValueAt(const std::shared_ptr<light_occupancy_state<observation_ihistory>>& s, horizon h){
    double value;
    this->getGreedyDecisionAt(s, value, h);
    return value;
  }

  std::shared_ptr<light_decision_rule> asymmetric_point_set::getGreedyDecisionAt(const std::shared_ptr<light_occupancy_state<observation_ihistory>>& s, double& max, horizon h){
    std::shared_ptr<light_decision_rule> jdr;
    auto tmp = std::make_shared<light_decision_rule>();

    action u, uMax;
    double value, value1, max1;
    //<! go over each action of the blind agent (i.e., agent 0)
    max = std::numeric_limits<double>::lowest();
    for(action u0=0; u0<common::model->getNumActions(0); ++u0){
      value = 0;
      tmp->setAction(u0);
      //+---------------------------------------------------------------+//
      //<! go over each belief involved in the current occupancy state !>//
      //+---------------------------------------------------------------+//
      for(const auto& bp : s->container){
        //<! initiamize intermediate data
        max1 = std::numeric_limits<double>::lowest();
        //<! go over each action of the second agent (i.e., agent 1)
        for(action u1=0; u1<common::model->getNumActions(1); ++u1){
          u = common::model->getJointActionIndex(std::vector<action>{u0,u1});
          value1 = this->getValueAt(s->ih0, bp.first, u, h);
          //<! select the best action for a given belief amongst all actions of the non-blind agent
          if( max1 < value1 ){
            uMax = u1;
            max1 = value1;
          }
        }
        value += bp.second * max1;
        tmp->setValueAt(bp.first, uMax, 1.0);
      }

      //<! select the best gamma for each action of the blind agent
      if( max < value ){
        jdr = tmp;
        max = value;
      }
    }

    return jdr;
  }

  void asymmetric_point_set::setValueAt(const std::shared_ptr<light_occupancy_state<observation_ihistory>>& s, const std::shared_ptr<light_decision_rule>& a, horizon h){
    double value;
    auto vf = this->getValueFunctionAt(s, a, h);
    for(const auto& bp : s->container){
      for(action u=0; u<common::model->getNumActions(); ++u){
        if( a->getValueAt(bp.first, u) == 1.0 ){
          value = this->getValueAt(bp.first, u, vf);

          if( this->container.at(h).find(s->ih0) == this->container.at(h).end() ){
            this->container.at(h).emplace(s->ih0, gamma_t());
          }

          if( this->container.at(h).at(s->ih0).find(u) == this->container.at(h).at(s->ih0).end() ){
            this->container.at(h).at(s->ih0).emplace(u, beta_t());
          }

          if( this->container.at(h).at(s->ih0).at(u).find(bp.first) == this->container.at(h).at(s->ih0).at(u).end() ){
            auto pair = std::make_pair(value, (*bp.first) ^ (*this->v_default[h]));
            this->container.at(h).at(s->ih0).at(u).emplace(bp.first, pair);
          }
          else{
            this->container.at(h).at(s->ih0).at(u).at(bp.first).first = std::min(value, this->container.at(h).at(s->ih0).at(u).at(bp.first).first);
          }
        }
      }
    }
  }

  double asymmetric_point_set::getValueAt(const belief_t& b, action u, const theta_t& vf) const{
    double prob, value = (*b)^common::model->getReward(u);

    observation z;
    for(observation z0=0; z0<common::model->getNumObservations(0); ++z0){
      for(observation z1=0; z1<common::model->getNumObservations(1); ++z1){
        z = common::model->getJointObservationIndex(std::vector<observation>{z0,z1});
        //<! compute belief, action, observation tuple
        auto belief_uz = make_tuple(b, u, z);
        //<! compute next-belief
        auto _bNext = common::nextBelief(belief_uz);
        //<! compute unweighted next-belief
        auto bNext = std::make_shared<Vector>(common::model->getNumStates());
        if( (prob = bNext->sum()) > 0 ){
          *bNext = (1.0/prob) * (*_bNext);
          common::saveBeliefs(bNext);
        }
        //<! get value of the next-belief
        value += common::model->getDiscount() * prob * vf.at(z).at(bNext);
      }
    }

    return value;
  }

  std::unordered_map<observation, std::unordered_map<std::shared_ptr<Vector>, double>> asymmetric_point_set::getValueFunctionAt(const std::shared_ptr<light_occupancy_state<observation_ihistory>>& s, const std::shared_ptr<light_decision_rule>& a, horizon h){
    double v;
    auto theta = theta_t();
    for(observation z=0; z<common::model->getNumObservations(0); ++z){
      auto uz = std::make_pair(a->getAction(), z);
      auto s_next = s->next(uz, a);
      auto a_next = this->getGreedyDecisionAt(s_next, v, h+1);
      theta.emplace(z, value_t());
      for(const auto& bp : s_next->container){
        for(action u=0; u<common::model->getNumActions(); ++u)
          if( a_next->getValueAt(bp.first, u) == 1.0 )
            theta.at(z).emplace(bp.first, this->getValueAt(s_next->ih0, bp.first, u, h+1));
      }
    }

    return theta;
  }

  double asymmetric_point_set::getValueAt(observation_ihistory* ih0, const belief_t& b, action u, horizon h) const{
    if( this->container.at(h).at(ih0).find(u) == this->container.at(h).at(ih0).end() ){
      return (*b) ^ (*this->v_default.at(h));
    }

    else if( this->container.at(h).at(ih0).at(u).find(b) != this->container.at(h).at(ih0).at(u).end() ){
      return this->container.at(h).at(ih0).at(u).at(b).first;
    }

    double value, min = (*b) ^ (*this->v_default.at(h));
    for(const auto& bv : this->container.at(h).at(ih0).at(u)){
      value = this->sawtooth(b, bv.first, bv.second, h);
      if( min > value ){
        min = value;
      }
    }

    return min;
  }

  double asymmetric_point_set::sawtooth(const belief_t& b, const belief_t b_, const std::pair<double, double>& v_, horizon h) const{
    double v, min = (*b) ^ (*this->v_default.at(h));

    v = min;
    for(state x=0; x<common::model->getNumStates(); ++x) if( (*b_)[x]>0 )
      v = std::min(v, min + ((*b)[x])*(v_.first-v_.second)/(*b_)[x]);

    return v;
  }

  number asymmetric_point_set::size() const{
    number size = 0;
    for(const auto& v : this->container)
      size += this->size(v.first);

    return size;
  }

  number asymmetric_point_set::size(horizon h) const{
    return this->container.at(h).size();
  }

  void asymmetric_point_set::initialize(){
    for(horizon h=common::model->getPlanningHorizon(); h<=common::model->getPlanningHorizon(); --h){
      this->container.emplace(h, std::unordered_map<observation_ihistory*, gamma_t>());
    }

    double value;
    this->v_default.emplace(common::model->getPlanningHorizon(), std::make_shared<Vector>(common::model->getNumStates()));
    this->v_default.at(common::model->getPlanningHorizon())->init(0.0);
    for(horizon h=common::model->getPlanningHorizon(); h > 0; --h){
      this->v_default.emplace(h-1, std::make_shared<Vector>(common::model->getNumStates()));
      this->v_default.at(h-1)->init(std::numeric_limits<double>::lowest());
      for(state x = 0; x < common::model->getNumStates(); ++x){
        for(action u = 0; u<common::model->getNumActions(); ++u){
          value = common::model->getReward(x,u);
          for(state y = 0; y < common::model->getNumStates(); ++y){
              value += common::model->getDiscount() * common::model->getTransitionProbability(x,u,y) * (*this->v_default.at(h))[y];
          }
          (*this->v_default.at(h-1))[x] = std::max(value,(*this->v_default.at(h-1))[x]);
        }
      }
    }
  }

  void asymmetric_point_set::pruning(){}

}
