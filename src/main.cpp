#include <time.h>
#include <vector>
#include <math.h>
#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <eigen3/Eigen/Dense>
#include <boost/program_options.hpp>

#include "../include/types.hpp"
#include "../include/parser/parser.hpp"


#include "../include/algorithms/mdps/search/light_fbhsvi.hpp"
#include "../include/algorithms/mdps/search/asymmetric_hsvi.hpp"

///////////////////////////////////////////////////////////////////////////////
//  Main program
///////////////////////////////////////////////////////////////////////////////
namespace
{
  const size_t SUCCESS = 0;
  const size_t ERROR_IN_COMMAND_LINE = 1;
  const size_t ERROR_UNHANDLED_EXCEPTION = 2;
} // namespace

int main(int argv, char** args){
  using namespace sdm;

  try{
    namespace po = boost::program_options;
    po::options_description desc("Allowed options");

    std::string model, filename, algorithm, results_dir, bench_dir;
    number memory, planning_horizon, trials;
    float epsilon_optimal, discount_factor;

    desc.add_options()
    ("help,h", "produce help message")
    ("algorithm,a", po::value<std::string>(&algorithm)->default_value("HSVI_1"), "set the algorithm to be used -- e.g. HSVI_1")
    ("model,g", po::value<std::string>(&model)->default_value("dpomdp"), "set the model of the problem to be solved -- e.g. dpomdp")
    ("filename,f", po::value<std::string>(&filename)->default_value("mabc"), "set the benchmark filename -- e.g. mabc, recycling, tiger, etc")
    ("planning_horizon,p", po::value<number>(&planning_horizon)->default_value(0), "set the planning horizon")
    ("optimal-epsilon,o", po::value<float>(&epsilon_optimal)->default_value(0.0001), "set the epsilon optimal parameter")
    ("trials,t", po::value<number>(&trials)->default_value(1), "set the number of trials")
    ("memory,m", po::value<number>(&memory)->default_value(1), "set the memory length limit")
    ("input-directory,x", po::value<std::string>(&bench_dir)->default_value("include/dpomdp/examples"), "set the input directory -- see include/dpomdp/examples")
    ("discount-factor,d", po::value<float>(&discount_factor)->default_value(1.0), "set the discount factor")
    ("output-directory,z", po::value<std::string>(&results_dir)->default_value(""), "set the output directory -- e.g. results/<algorithm-name>/<bench>/<params>")
    ;

    po::variables_map vm;
    try{
      po::store(po::parse_command_line(argv, args, desc), vm);

      if(vm.count("help")){
        std::cout << "Basic Command Line Parameter" << std::endl << desc << std::endl;
        return SUCCESS;
      }

      po::notify(vm);
    } catch(po::error& e){
      std::cerr << "ERROR: " << e.what() << std::endl << std::endl;
      std::cerr << desc << std::endl;
      return ERROR_IN_COMMAND_LINE;
    }

    if( model.compare("dpomdp") == 0 ){
      std::string bench_filename;
      bench_filename.append(bench_dir);
      bench_filename.append("/");
      bench_filename.append(filename);
      bench_filename.append(".dpomdp");
      sdm::parser::parse_file(bench_filename.c_str());
      common::model->setDiscount(discount_factor);
      common::model->setPlanningHorizon(planning_horizon);
    }


    if( planning_horizon == 0 && discount_factor < 1.0 ){
      planning_horizon = (number) (log((1-common::model->getDiscount())*epsilon_optimal / common::model->getMaxReward()) / log(common::model->getDiscount()));
    } else if( planning_horizon == 0 && discount_factor == 1.0) {
      planning_horizon = 1000;
    }



    if(algorithm.compare("HSVI_1") == 0){

      // auto ahsvi = std::make_shared<asymmetric_hsvi<action_observation_ihistory, double, std::shared_ptr<asymmetric_belief_state<action_observation_ihistory>>, action, false, false, true>>(memory, trials, results_dir);
      auto ahsvi = std::make_shared<asymmetric_hsvi<action_observation_ihistory, double, std::shared_ptr<light_occupancy_state<action_observation_ihistory>>, std::shared_ptr<light_decision_rule>, true, false, true>>(memory, trials, results_dir);      
      ahsvi->solve(common::model, planning_horizon, epsilon_optimal);
    }

    else if(algorithm.compare("HSVI_2") == 0){

      auto lfbhsvi = std::make_shared<light_fbhsvi<observation_ihistory, ENUMERATION,FINITE_HORIZON, POMDP>>(memory, trials, results_dir);
      lfbhsvi->solve(common::model, planning_horizon, epsilon_optimal);
    }

    else if(algorithm.compare("HSVI_3") == 0){

      auto lfbhsvi = std::make_shared<light_fbhsvi<observation_ihistory, LINEAR_PROGRAM, FINITE_HORIZON, POMDP>>(memory, trials, results_dir);
      lfbhsvi->solve(common::model, planning_horizon, epsilon_optimal);
    }




    else{
      #ifdef DEBUG
      std::cout << "Please input right algorithm" << std::endl;
      #endif
    }

    // if(backup){
    //   Performance::writeStepAndEpisodeAndAverageDataToCVS(results_dir.c_str());
    // }
  } catch(std::exception& e){
    std::cerr << "Unhandled Exception reached the top of main: " << e.what() << ", application will now exit" << std::endl;
    return ERROR_UNHANDLED_EXCEPTION;
  }

  return SUCCESS;
}
