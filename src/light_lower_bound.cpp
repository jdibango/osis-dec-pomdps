//! \date 12 October 2019

#include "../include/utils/backup_structures/piece_wise_linear_convex/light_lower_bound.hpp"

namespace sdm{

  template<typename ihistory_t, number optimizer>
  light_lower_bound<ihistory_t, optimizer>::light_lower_bound(){};

  template<typename ihistory_t, number optimizer>
  light_lower_bound<ihistory_t, optimizer>::~light_lower_bound(){};

  template<typename ihistory_t, number optimizer>
  void light_lower_bound<ihistory_t, optimizer>::storeAlphaVector(number i_bag, number i_vector, action u, observation z, horizon h){
    auto key = std::make_tuple(this->container.at(this->is_finite ? h+1 : 0)[i_bag][i_vector], u, z);
    if( this->alpha_container.find(key) == this->alpha_container.end() ){
      auto i_alpha = this->container.at(this->is_finite ? h+1 : 0)[i_bag][i_vector];
      if(this->alpha_container.find(std::make_tuple(i_alpha, u, z)) == this->alpha_container.end()){
        auto j_alpha = std::make_shared<Vector>(common::model->getNumStates()); j_alpha->init(0.0);
        for(state x=0; x<common::model->getNumStates(); ++x){
          for(state y=0; y<common::model->getNumStates(); ++y){
            (*j_alpha)[x] += common::model->getDynamics(x,u,z,y) * (*i_alpha)[y];
          }
        }
        common::saveAlphas(j_alpha);
        this->alpha_container.emplace(std::make_tuple(i_alpha, u, z), j_alpha);
      }
    }
  }

  template<typename ihistory_t, number optimizer>
  number light_lower_bound<ihistory_t, optimizer>::size() const{
    number size = 0;
    for(horizon h=0; h<common::model->getPlanningHorizon(); ++h)
      size += this->container.at(h).size();
    return size;
  }

  template<typename ihistory_t, number optimizer>
  number light_lower_bound<ihistory_t, optimizer>::size(horizon h) const{
    return this->container.at(h).size();
  }
  template<typename ihistory_t, number optimizer>
  std::shared_ptr<Vector> light_lower_bound<ihistory_t, optimizer>::getArgMaxAlphaVector(const std::shared_ptr<Vector>& b, action u, observation z, number bag, horizon h){
    double value, min = std::numeric_limits<double>::lowest();
    std::shared_ptr<Vector> witness = std::make_shared<Vector>(common::model->getNumStates()); witness->init(0.0);
    for(number i=0; i<this->container.at(this->is_finite ? h+1 : 0)[bag].size(); ++i){
      this->storeAlphaVector(bag, i, u, z, h);
      auto i_vector = this->alpha_container.at(std::make_tuple(this->container.at(this->is_finite ? h+1 : 0)[bag][i], u, z));
      if( min < (value = (*i_vector)^(*b)) ){
        min = value;
        *witness = *i_vector;
      }
    }

    return witness;
  }


  template<typename ihistory_t, number optimizer>
  void light_lower_bound<ihistory_t, optimizer>::initialize(bool is_finite){
    this->is_finite = is_finite;
    for(horizon h=0; h<=common::model->getPlanningHorizon(); h++){
      auto init = std::make_shared<Vector>(common::model->getNumStates());
      init->init(common::model->getMinReward() * (this->is_finite ? common::model->getPlanningHorizon() - h : 1.0 / (1-common::model->getDiscount())));
      this->container.emplace(h, std::vector<std::vector<std::shared_ptr<Vector>>>());
      std::vector<std::shared_ptr<Vector>> v; v.push_back(init);
      this->container.at(h).push_back(v);
    }
  }


  template<>
  void light_lower_bound<observation_ihistory, LINEAR_PROGRAM>::setValueAt(std::shared_ptr<light_occupancy_state<observation_ihistory>>& s, double, horizon h){
    std::set<std::shared_ptr<Vector>> container;
    double max = std::numeric_limits<double>::lowest();
    std::pair<action, std::shared_ptr<light_decision_rule>> jdr_max;
    auto size = this->container.at(this->is_finite ? h+1 : 0).size();

    for(action u1=0; u1<common::model->getNumActions(0); ++u1){
      std::string varname;
      number index = 0, c = 0;
      std::shared_ptr<light_decision_rule> dr;
      container = s->getSuccessorSupports(u1);
      double value = std::numeric_limits<double>::lowest(), weight;

      IloEnv env;
      try{
        IloModel model(env);
        IloRangeArray con(env);
        IloNumVarArray var(env);
        IloObjective obj = IloMaximize(env);

        common::clearVariables();

        //+-------------------------------------------------------------------+//
        //|                           VARIABLES                               |//
        //+-------------------------------------------------------------------+//
        var.add(IloNumVar(env, -IloInfinity, +IloInfinity, "bound"));
        common::setNumber("bound", index++);

        //variable a2(u2|b)
        for(const auto& bprob : s->container){
          for(action u2=0; u2<common::model->getNumActions(1); ++u2){
            varname = common::getVarNameBeliefAction(1, bprob.first, u2);
            var.add(IloNumVar(env, 0, 1, varname.c_str()));
            common::setNumber(varname, index++);
          }
        }

        for(observation z1=0; z1<common::model->getNumObservations(0); ++z1){
          //variable W(z1)
          varname = common::getVarNameObservation(z1);
          var.add(IloNumVar(env, -IloInfinity, +IloInfinity, varname.c_str()));
          common::setNumber(varname, index++);

          // variable W(k,z)
          for(number k=0; k<this->container.at(this->is_finite ? h+1 : 0).size(); ++k){
            varname = common::getVarNameBagObservation(k, z1);
            var.add(IloBoolVar(env, varname.c_str()));
            common::setNumber(varname, index++);
          }
        }

        //+-------------------------------------------------------------------+//
        //|                      OBJECTIVE FUNCTIONS                          |//
        //+-------------------------------------------------------------------+//
        obj.setLinearCoef(var[common::getNumber("bound")], +1);


        //+-------------------------------------------------------------------+//
        //|                      CONSTRAINTS FUNCTIONS                        |//
        //+-------------------------------------------------------------------+//
        // constraint 0 : function first term : first immediate reward \sum_{u2}\sum_{b}{s(b)* a2(u2|b) * R(b,u2)} + \sum_{z1} W(z1)
        con.add(IloRange(env, 0, 0));
        con[c].setLinearCoef(var[common::getNumber("bound")], -1);
        for(const auto& bp : s->container){
          for(action u2 = 0; u2 < common::model->getNumActions(1); ++u2){
            auto u = common::model->getJointActionIndex(std::vector<action>{u1,u2});
            weight = bp.second * *bp.first ^ common::model->getReward(u);
            con[c].setLinearCoef(var[common::getNumber(common::getVarNameBeliefAction(1, bp.first, u2))], weight);
          }
        }
        for(observation z1=0; z1<common::model->getNumObservations(0); ++z1){
          con[c].setLinearCoef(var[common::getNumber(common::getVarNameObservation(z1))], common::model->getDiscount());
        }
        c++;


        //constraint 1: W(z1) <=  W(k,z1) * big_M + \sum_{u2} \sum_{b:s(b)>0} a2(u2|b) [ s(b) \sum_{z2} pr(z|b,u) * max_{\alpha} <\tau(b,u,z),\alpha>) ]
        for(observation z1=0; z1<common::model->getNumObservations(0); ++z1){
          for(number k=0; k<this->container.at(this->is_finite ? h+1 : 0).size(); ++k){
            IloExpr expr(env);
            expr = var[common::getNumber(common::getVarNameObservation(z1))];
            for(action u2 = 0; u2 < common::model->getNumActions(1); ++u2){
              for(const auto& bp : s->container){
                weight = 0.0;
                for(observation z2=0; z2<common::model->getNumObservations(1); ++z2){
                  auto u = common::model->getJointActionIndex(std::vector<action>({u1,u2}));
                  auto z = common::model->getJointObservationIndex(std::vector<observation>({z1,z2}));
                  auto bp_ = s->next(bp.first, u, z);
                  weight -= bp.second * bp_.second * this->getValueAt(bp_.first, this->container.at(this->is_finite ? h+1 : 0)[k]);
                }
                expr += weight * var[common::getNumber(common::getVarNameBeliefAction(1, bp.first, u2))];
              }
            }
            model.add( IloIfThen(env, var[common::getNumber(common::getVarNameBagObservation(k, z1))] > 0, expr <= 0) );
          }
        }

        //constraint 2: \sum_{k} W(k,z1) = 1
        for(observation z1=0; z1<common::model->getNumObservations(0); ++z1){
          con.add(IloRange(env, 1, 1));
          for(number k=0; k<size; ++k)
          con[c].setLinearCoef(var[common::getNumber(common::getVarNameBagObservation(k, z1))],+1.0);
          c++;
        }

        //constraint 3: \sum_{u2} a2(u2|b) = 1
        for(const auto& bp : s->container){
          con.add(IloRange(env, 1, 1));
          for(action u2=0; u2<common::model->getNumActions(1); ++u2)
          con[c].setLinearCoef(var[common::getNumber(common::getVarNameBeliefAction(1, bp.first, u2))], +1);
          c++;
        }

        model.add(obj);
        model.add(con);
        IloCplex cplex(model);

        cplex.setOut(env.getNullStream());
        cplex.setWarning(env.getNullStream());
        #ifdef DEBUG
        cplex.exportModel("lower-greedy.lp");
        #endif

        if( !cplex.solve() ){
          env.error() << "Failed to optimize greedy lower bound" << std::endl;
          throw(-1);
        }

        value = cplex.getObjValue();
        dr = this->setSolution(s, cplex, var);

      } catch(IloException& e){
        std::cerr << "Concert exception caught: " << e << std::endl;
      }catch(const std::exception& exc){
        std::cerr << "Non-Concert exception caught: " << exc.what() << std::endl;
      }

      env.end();

      if(value > max ){
        jdr_max = std::make_pair(u1, dr);
        max = value;
      }
    }
    if(jdr_max.second == NULL ) return;
    this->setValueAt(s, jdr_max, h);
  }

  template<typename ihistory_t, number optimizer>
  std::shared_ptr<light_decision_rule> light_lower_bound<ihistory_t, optimizer>::setSolution(std::shared_ptr<light_occupancy_state<ihistory_t>>& s, const IloCplex& cplex, const IloNumVarArray& var){
    auto dr = std::make_shared<light_decision_rule>();
    for(const auto& bp : s->container){
      for(action u2=0; u2<common::model->getNumActions(1); ++u2){
        dr->setValueAt(bp.first, u2, cplex.getValue(var[common::getNumber(common::getVarNameBeliefAction(1, bp.first, u2))]));
      }
    }
    return dr;
  }

  template<>
  void light_lower_bound<observation_ihistory, ENUMERATION>::setValueAt(std::shared_ptr<light_occupancy_state<observation_ihistory>>& s, double, horizon h){
    auto dc_generator = s->getVariations();
    double value, max = std::numeric_limits<double>::lowest();
    std::pair<action, std::shared_ptr<light_decision_rule>> jdr_max;
    for(action u1 = 0; u1 < common::model->getNumActions(0); ++u1){
      auto ldr = dc_generator.begin();
      auto dr = std::make_shared<light_decision_rule>(ldr->container);
      do{
        auto jdr = std::make_pair(u1, dr);
        value = s->getReward(jdr);
        for(observation z1=0; z1<common::model->getNumObservations(0); ++z1){
          auto uz = std::make_pair(u1,z1);
          auto s_next = s->next(uz, dr);
          value += common::model->getDiscount() * s_next->prob * this->getValueAt(s_next, this->is_finite ? h+1 : 0);
        }

        if(value > max){
          max = value;
          jdr_max = jdr;
        }

        if((ldr = dc_generator.next()) != nullptr)
        dr = std::make_shared<light_decision_rule>(ldr->container);
      }while( ldr  != nullptr );
    }

    this->setValueAt(s, jdr_max, h);
  }

  template<typename ihistory_t, number optimizer>
  void light_lower_bound<ihistory_t, optimizer>::setValueAt(std::shared_ptr<light_occupancy_state<ihistory_t>>& s, std::pair<action, std::shared_ptr<light_decision_rule>> jdr, horizon h){
    std::vector<std::shared_ptr<Vector>> alpha_bag;
    for(const auto& bp : s->container){
      auto alpha = std::make_shared<Vector>(common::model->getNumStates()); alpha->init(0.0);
      for(action u2=0; u2<common::model->getNumActions(1); ++u2){
        auto alpha_u2 = std::make_shared<Vector>(common::model->getNumStates()); alpha_u2->init(0.0);
        auto u = common::model->getJointActionIndex(std::vector<action>({jdr.first, u2}));
        *alpha_u2 = common::model->getReward(u);
        for(observation z1=0; z1<common::model->getNumObservations(0); ++z1){
          auto bag = this->getBestBagAt(s, jdr.first, jdr.second, z1, h);
          for(observation z2=0; z2<common::model->getNumObservations(1); ++z2){
            auto z = common::model->getJointObservationIndex(std::vector<observation>({z1,z2}));
            *alpha_u2 += common::model->getDiscount() * (*this->getArgMaxAlphaVector(bp.first, u, z, bag, h));
          }
        }
        *alpha += jdr.second->getValueAt(bp.first, u2) * (*alpha_u2);
      }
      alpha_bag.push_back(alpha);
    }
    this->container.at(h).push_back(alpha_bag);
    this->pruning(h);
  }

  template<typename ihistory_t, number optimizer>
  double  light_lower_bound<ihistory_t, optimizer>::getValueAt(const std::shared_ptr<Vector>& b, const std::vector<std::shared_ptr<Vector>>& bag){
    double max = std::numeric_limits<double>::lowest();
    for(auto& alpha : bag)
    max = std::max(max,  *b ^ *alpha);
    return max;
  }

  template<typename ihistory_t, number optimizer>
  double light_lower_bound<ihistory_t, optimizer>::getValueAt(const std::shared_ptr<light_occupancy_state<ihistory_t>>& s, horizon h){
    double max = std::numeric_limits<double>::lowest(), tmp;
    for(auto& bag : this->container.at(h)){
      tmp = 0.0;
      for(auto& bp : s->container){
        tmp += bp.second * this->getValueAt(bp.first, bag);
      }
      max = std::max(max, tmp);
    }
    return max;
  }


  template<typename ihistory_t, number optimizer>
  number light_lower_bound<ihistory_t, optimizer>::getBestBagAt(std::shared_ptr<light_occupancy_state<ihistory_t>>& s, action u1, std::shared_ptr<light_decision_rule>& dr, observation z1, horizon h){
    number max_bag = 0;
    auto uz = std::make_pair(u1,z1);
    double tmp, max = std::numeric_limits<double>::lowest();

    auto s_next = s->next(uz,dr);
    for(number bag=0; bag<this->container.at(this->is_finite ? h+1 : 0).size(); ++bag){
      tmp = 0.0;
      for(auto& bp : s_next->container) tmp += bp.second * this->getValueAt(bp.first, this->container.at(this->is_finite ? h+1 : 0)[bag]);

      if(tmp > max){
        max = tmp; max_bag = bag;
      }
    }

    return max_bag;
  }


  template<typename ihistory_t, number optimizer>
  bool light_lower_bound<ihistory_t, optimizer>::isDominatedPointWiseBy(const std::shared_ptr<Vector>& alpha_0, const std::shared_ptr<Vector>& alpha_1){
    for(number i = 0; i < (*alpha_0).size(); ++i)
    if((*alpha_0)[i] - common::error <= (*alpha_1)[i]) return false;
    return true;
  }


  template<typename ihistory_t, number optimizer>
  bool light_lower_bound<ihistory_t, optimizer>::isDominatedBagWiseBy(const std::shared_ptr<Vector>& alpha, const std::vector<std::shared_ptr<Vector>>& bag){
    for(number i = 0; i < bag.size(); ++i)
    if(!this->isDominatedPointWiseBy(alpha, bag.at(i))) return false;
    return true;
  }


  template<typename ihistory_t, number optimizer>
  bool light_lower_bound<ihistory_t, optimizer>::isDominatedBy(const std::vector<std::shared_ptr<Vector>>& bag_0, const std::vector<std::shared_ptr<Vector>>& bag_1){
    bool result = true;

    for(number i = 0; i < bag_0.size(); ++i)
    if(!isDominatedBagWiseBy(bag_0.at(i), bag_1)){
      result =false;
      break;
    }

    if(result == false && bag_0.size() == bag_1.size()){
      for(number i = 0; i < bag_0.size(); ++i)
      if(!((*bag_0[i]) == (*bag_1[i]))) return false;
      result = true;
    }

    return result;
  }

  //warning Remember to create a vector for pruning bags in case of some errors occured after pruning.
  template<typename ihistory_t, number optimizer>
  void light_lower_bound<ihistory_t, optimizer>::pruning(horizon d){
    std::vector<number> toDelete;

    for(number bag_1 = 0; bag_1 < this->container.at(d).size(); ++bag_1){
      for(number bag_2 = bag_1+1; bag_2 < this->container.at(d).size(); ++bag_2){
        if(isDominatedBy(this->container.at(d)[bag_1], this->container.at(d)[bag_2]))
          toDelete.push_back(bag_2);
        else if(isDominatedBy(this->container.at(d)[bag_2], this->container.at(d)[bag_1]))
          toDelete.push_back(bag_1);
      }
    }

    std::unique(toDelete.begin(),toDelete.end());
    std::sort(toDelete.begin(),toDelete.end());

    for(int bag_2 = toDelete.size() - 1; bag_2 >= 0; --bag_2){
      this->container.at(d).erase(this->container.at(d).begin() + toDelete[bag_2]);
      this->deleteAllAlphaVectors(toDelete[bag_2],d);
    }
  }

  template<typename ihistory_t, number optimizer>
  void light_lower_bound<ihistory_t, optimizer>::deleteAlphaVector(number i, number j, action u, observation z, horizon h){
    if(this->container.at(h).size() > i){
      if(this->alpha_container.find(std::make_tuple(this->container.at(h)[i][j], u, z)) != this->alpha_container.end())
      this->alpha_container.erase(std::make_tuple(this->container.at(h)[i][j], u, z));
    }
  }

  template<typename ihistory_t, number optimizer>
  void light_lower_bound<ihistory_t, optimizer>::deleteAllAlphaVectors(number i_bag, horizon h){
    for(number i_vector=0; i_vector<this->container.at(h)[i_bag].size(); ++i_vector){
      for(action u=0; u<common::model->getNumActions(); ++u){
        for(observation z=0; z<common::model->getNumObservations(); ++z){
          this->deleteAlphaVector(i_bag, i_vector, u, z, h);
        }
      }
    }
  }

  template class light_lower_bound<observation_ihistory, ENUMERATION>;
  template class light_lower_bound<observation_ihistory, LINEAR_PROGRAM>;
}
