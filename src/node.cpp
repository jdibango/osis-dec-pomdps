#include <sstream>

#include "../include/dpomdp/dpomdp.hpp"
#include "../include/utils/recursive_struct/node.hpp"
// #include "../include/utils/decision_rules/joint_decision_rule.hpp"
// #include "../include/utils/decision_rules/joint_decision_rule.hpp"
// #include "../include/utils/decision_rules/delayed_joint_decision_rule.hpp"
// #include "../include/utils/decision_rules/best_response_decision_rule.hpp"
#include "../include/utils/information_states/history_state/jhistory.hpp"

namespace sdm{
 template<typename T, int N>
 node<T,N>::node(){}

 template<typename T, int N>
 node<T,N>::~node(){}

 template<typename T, int N>
 node<T,N>::node(const T& item, base_graph<T,N>* parent) : item(item), parent(parent){}

 template<typename T, int N>
 base_graph<T,N>*const node<T,N>::getParent() const{
   return this->parent;
 }

  template<typename T, int N>
  T node<T,N>::getItem() const{
    return this->item;
  }

  template class node<observation>;
  template class node<observation,1>;
  template class node<std::pair<action,observation>>;
  template class node<std::pair<action,observation>,1>;
  // template class node<std::shared_ptr<random_jdecision_rule>>;
  // template class node<std::shared_ptr<random_uniform_jdecision_rule>>;
  // template class node<std::shared_ptr<delayed_joint_decision_rule>>;
  // template class node<std::shared_ptr<best_response_decision_rule>>;
  // template class node<std::shared_ptr<deterministic_jdecision_rule>>;
  // template class node<std::shared_ptr<deterministic_uniform_jdecision_rule>>;
}
