
//! \date 12 October 2019

#include "../include/utils/information_states/occupancy_state/light_occupancy_state.hpp"

//! \namespace  sdm
//!
//! Namespace grouping all tools required for sequential decision making.
namespace sdm{
  template<typename ihistory_t>
  light_occupancy_state<ihistory_t>::light_occupancy_state(horizon h){
    this->h = h;
    this->z0 = 0;
    this->prob = 1;
    this->ih0 = new ihistory_t(0);
    this->container.emplace(common::model->getStart(), 1.0);
  }

  template<typename ihistory_t>
  light_occupancy_state<ihistory_t>::~light_occupancy_state(){}

  template<typename ihistory_t>
  light_occupancy_state<ihistory_t>::light_occupancy_state(std::unordered_map<std::shared_ptr<Vector>, double>& tmp, horizon h){
    this->h = h;
    this->z0 = 0;
    auto sum = 0.0;
    this->ih0 = new ihistory_t(0);

    for(auto& bp : tmp) sum += bp.second;
    for(auto& bp : tmp) tmp[bp.first] /= sum;

    this->prob = sum;
    this->container = tmp;
  }

  template<typename ihistory_t>
  horizon light_occupancy_state<ihistory_t>::getHorizon() const{
    return this->h;
  }

  template<typename ihistory_t>
  variations<std::vector<std::shared_ptr<Vector>>, light_decision_rule> light_occupancy_state<ihistory_t>::getVariations(){
    std::vector<action> actions;
    std::vector<std::shared_ptr<Vector>> beliefs;

    for(const auto& bp : this->container){
      beliefs.push_back(bp.first);
      actions.push_back(common::model->getNumActions(1));
    }

    variations<std::vector<std::shared_ptr<Vector>>, light_decision_rule> dc_generator(beliefs, actions);

    return dc_generator;
  }

  template<typename ihistory_t>
  double light_occupancy_state<ihistory_t>::getReward(std::pair<action, std::shared_ptr<light_decision_rule>>& a){
    action u;
    double reward = 0.0;

    for(auto& bp : this->container){
      for(action u2=0; u2<common::model->getNumActions(); ++u2){
        u = common::model->getJointActionIndex(std::vector<action>({a.first, u2}));
        reward += a.second->getValueAt(bp.first, u2) * bp.second * (*bp.first ^ common::model->getReward(u));
      }
    }

    return reward;
  }

  template<typename ihistory_t>
  std::set<std::shared_ptr<Vector>> light_occupancy_state<ihistory_t>::getSuccessorSupports(action u1){
    std::set<std::shared_ptr<Vector>> container;
    for(const auto& bp : this->container){
      for(action u2=0; u2<common::model->getNumActions(1); ++u2){
        action u = common::model->getJointActionIndex( std::vector<action>({u1,u2}));
        for(observation z=0; z<common::model->getNumObservations(); ++z){
          container.insert( this->next(bp.first, u, z).first );
        }
      }
    }
    return container;
  }


  template<typename ihistory_t>
  double light_occupancy_state<ihistory_t>::getValueAt(const std::shared_ptr<Vector>& b) const{
    if( this->container.find(b) == this->container.end() ) return 0;
    return this->container.at(b);
  }

  template<typename ihistory_t>
  std::shared_ptr<light_occupancy_state<ihistory_t>> light_occupancy_state<ihistory_t>::next(std::pair<action, observation>& agent_1, const std::shared_ptr<light_decision_rule>& agent_2){
    action u;
    observation z;
    std::unordered_map<std::shared_ptr<Vector>, double> tmp;

    for(auto& bp : this->container){
      for(observation z2=0; z2<common::model->getNumObservations(1); ++z2){
        z = common::model->getJointObservationIndex(std::vector<observation>({agent_1.second, z2}));
        for(action u2=0; u2<common::model->getNumActions(1); ++u2){
          if( (prob = agent_2->getValueAt(bp.first, u2)) > 0 ){
            u = common::model->getJointActionIndex(std::vector<action>({agent_1.first, u2}));
            auto nbp = this->next(bp.first, u, z);
            if( nbp.second > 0 ){
              if( tmp.find(nbp.first) == tmp.end() ) tmp.emplace(nbp.first, bp.second * nbp.second * prob);
              else tmp[nbp.first] += bp.second * nbp.second * prob;
            }
          }
        }
      }
    }

    auto loc =  std::make_shared<light_occupancy_state<ihistory_t>>(tmp, this->h+1);
    loc->ih0 = this->next(agent_1.first, agent_1.second);
    loc->z0 = agent_1.second;
    return loc;
  }

  template<typename ihistory_t>
  std::pair<std::shared_ptr<Vector>, double> light_occupancy_state<ihistory_t>::next(const std::shared_ptr<Vector>& belief, action u, observation z){
    auto unweighted_nb = std::make_shared<Vector>(common::model->getNumStates()); unweighted_nb->init(0.0);
    auto weighted_nb = std::make_shared<Vector>(common::model->getNumStates()); weighted_nb->init(0.0);
    auto belief_uz = std::make_tuple(belief, u, z);
    weighted_nb = common::nextBelief(belief_uz);
    auto prob = weighted_nb->sum();

    if( prob > 0 ){
       *unweighted_nb = (1.0/prob) * *weighted_nb;
       common::saveBeliefs(unweighted_nb);
    }

    return std::make_pair(unweighted_nb, prob);
  }

  template<typename ihistory_t>
  std::shared_ptr<light_decision_rule> light_occupancy_state<ihistory_t>::getRandomDecisionRule(){
    auto dr_container = std::make_shared<light_decision_rule>();

    for(auto& bp : this->container)
      dr_container->setValueAt(bp.first, common::pick_a_number(0,common::model->getNumActions(1)-1), 1.0);

    return dr_container;
  }

  template<typename ihistory_t>
  ihistory_t* light_occupancy_state<ihistory_t>::next(action u, observation z){
    auto uz = std::make_pair(u, z);
    return this->ih0->expand(uz, true);
  }

  template<>
  observation_ihistory* light_occupancy_state<observation_ihistory>::next(action, observation z){
    return this->ih0->expand(z, true);
  }

  template<typename ihistory_t>
  std::ostream& operator<<(std::ostream& os, const light_occupancy_state<ihistory_t>& los){
    os << "\033[1m\033[32m  <light-occupancy-state ref=\"" << &los << "\" prob=\"" << los.prob << "\" horizon=\"" << los.getHorizon() << "\" public-observation=\'"<< los.z0 <<"\'  dimension=\"" << los.container.size() << "\"> \033[0m" << std::endl;
    for(auto& pair : los.container) os << "\033[1m\033[32m  \t<belief value@"<< pair.first <<"=\"" << *pair.first << "\" prob=\"" << pair.second << "\" /> \033[0m" << std::endl;
    os << "\033[1m\033[32m  </light-occupancy-state> \033[0m" << std::endl;
    return os;
  }

  template class light_occupancy_state<observation_ihistory>;
  template class light_occupancy_state<action_observation_ihistory>;
  template std::ostream& operator<<(std::ostream&, const light_occupancy_state<observation_ihistory>&);
}
