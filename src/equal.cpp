#include "../include/utils/equal.hpp"

namespace sdm{

  template<typename T>
  bool equal<T>::operator()(const T& left, const T& right) const {
    return left == right;
  }

  template<typename T>
  bool equal<T*>::operator()(const T*& left, const T*& right) const {
    return left == right ? true : equal<T>()(*left, *right);
  }

  template<typename T>
  bool equal<std::shared_ptr<T>>::operator()(const std::shared_ptr<T>& left, const std::shared_ptr<T>& right) const {
    return left == right ? true : equal<T>()(*left, *right);
  }

  template<>
  bool equal<void*>::operator()(const void*& left, const void*& right) const {
    return left == right;
  }
}
