
#include "../include/utils/recursive_struct/graph.hpp"
#include "../include/dpomdp/dpomdp.hpp"

namespace sdm{
  number limited_length::length_limit = 99;

  std::unordered_set<void*> deleted = {};

  limited_length::limited_length(){}

  limited_length::~limited_length(){}

  void limited_length::setLengthLimit(number _length_limit){
    length_limit = _length_limit < 1 ? 1 : _length_limit;
  }

  number limited_length::getLengthLimit(){
    return length_limit;
  }

  template<typename T, int N>
  base_graph<T,N>::base_graph() : limited_length(){
    this->setHorizon( 0 );
    this->origin = this;
  }

  template<typename T, int N>
  base_graph<T,N>::base_graph(base_graph<T,N>* parent, const T& item, bool backup) : limited_length(){
    this->setHorizon( parent->getHorizon() + 1 );

    if(backup){
      this->origin = parent->origin;
      parent->children.emplace(item, this);
      this->data = new node<T,N>(item, parent);
    }
  }

  template<typename T, int N>
  base_graph<T,N>::~base_graph(){
    this->marked = true;
    for(auto child : this->children){
      if( child.second == this or child.second->marked ){        //<! treats separately self-loops or marked children
        this->children.erase(child.first);                       //<! remove without deleting the node from the set of children
      }
      else{                                                      //<! checks wether or not there is no self-loops
        delete child.second;                                     //<! deletes each child
      }
    }

    this->children.clear();                                      //<! cleans up the set of children
    if(this->data != NULL){                                      //<! deletes the parent data
      delete this->data;
      this->data = NULL;
    }
  }

  template<typename T, int N>
  number base_graph<T,N>::getNumChildren() const{
    number n = 1;
    if( this->children.empty() ) return n;
    for(auto child : this->children){
      if( child.second == this ){
        n++;
      }
      else {
        n += child.second->getNumChildren();
      }
    }
    return n;
  }

  template<typename T, int N> template<typename output>
  output*const base_graph<T,N>::base_expand(const T& item, bool backup){
    if( backup and (this->children.find(item) != this->children.end()) ){
      return static_cast<output*>(this->children[item]);
    }
    if( backup and (this->getHorizon() >= base_graph<T,N>::length_limit) ){
      return this->template base_truncated_expand<output>(item, backup);
    }
    return new output(static_cast<output*>(this), item, backup);
  }

  template<typename T, int N> template<typename output>
  output*const base_graph<T,N>::base_truncated_expand(const T& item, bool backup){
    std::list<T> items;

    //<! fill in the vector of observation to simulate
    items.push_front(item);
    auto parent = this->getParent();

    while( items.size() < base_graph<T,N>::length_limit ){
      items.push_front( parent->getItem() );
      parent = parent->getParent();
    }

    //<! iteratively expands the base_graph
    auto trace = static_cast<output*>(this->origin);
    for(auto it=items.begin(); it!=items.end(); ++it){
      trace = trace->template base_expand<output>( *it, backup );
    }

    //<! stores 'trace' as a child
    if( backup ) this->children.emplace(item, trace);                            //<! TODO WARNING this line might cause troubles!!!

    //<! returns the final base_graph
    return trace;
  }

  template<typename T, int N>
  base_graph<T,N>*const base_graph<T,N>::getParent() const{
    return this->data->getParent();
  }

  template<typename T, int N>
  node<T,N>*const base_graph<T,N>::getData() const{
    return this->data;
  }

  template<typename T, int N>
  T base_graph<T,N>::getItem() const{
    return this->data->getItem();
  }

  template<typename T, int N>
  horizon base_graph<T,N>::getHorizon() const{
    return this->depth;
  }

  template<typename T, int N>
  base_graph<T,N>*const base_graph<T,N>::getChild(const T& item) const{
    return this->children.find(item) != this->children.end() ? this->children.at(item) : nullptr;
  }

  template<typename T, int N>
  void base_graph<T,N>::setHorizon(horizon depth){
    this->depth = depth;
  }

  template<typename T, int N>
  std::string base_graph<T,N>::to_string(const T& item){
    return std::to_string(item);
  }

  template<>
  std::string base_graph<std::pair<action, observation>>::to_string(const std::pair<action, observation>& item){
    return '(' + std::to_string(item.first) + ',' + std::to_string(item.second) + ')';
  }

  template<>
  std::string base_graph<std::pair<action, observation>,1>::to_string(const std::pair<action, observation>& item){
    return base_graph<std::pair<action, observation>>::to_string(item);
  }

  template<typename T, int N>
  action graph<T,N>::getLabel() const{
    return this->label;
  }

  template<typename T, int N>
  void graph<T,N>::setLabel(const action& label){
    this->label = label;
  }

  template<typename T, int N>
  void graph<T,N>::setParams(const std::vector<T>& keys, const std::vector<action>& values){
    this->keys = keys;
    this->values = values;
  }

  template<typename T, int N>
  const std::vector<T>&  graph<T,N>::getKeys() const{
    return this->keys;
  }

  template<typename T, int N>
  const std::vector<action>&  graph<T,N>::getValues() const{
    return this->values;
  }

  template<typename T, int N>
  agent graph<T,N>::getAgent() const{
    return this->name;
  }

  template<typename T, int N>
  void graph<T,N>::setAgent(const agent& name){
    this->name = name;
  }

  template<typename T, int N>
  graph<T,N>::graph() : base_graph<T,N>(){}

  template<typename T, int N>
  graph<T,N>::graph(const agent& name) : base_graph<T,N>(){
    this->setAgent(name);
  }

  template<typename T, int N>
  graph<T,N>::graph(graph<T,N>* parent, const T& item, bool backup) : base_graph<T,N>(parent, item, backup){
    this->setAgent(parent->getAgent());
  }

  template<typename T, int N>
  graph<T,N>::graph(const std::vector<T>& keys, const std::vector<action>& values){
    this->setParams(keys, values);
  }

  template<typename T, int N>
  graph<T,N>::~graph(){}

  template<typename T, int N>
  graph<T,N>*const graph<T,N>::expand(const T& item, bool backup){
    return this->template base_expand<graph<T,N>>(item, backup);
  }

  template<typename T, int N>
  void graph<T,N>::initTree(const std::vector<graph*>& subtrees, const horizon& planning_horizon){
    int key, size = this->keys.size();
    this->setHorizon(planning_horizon);
    for(key=0; key<size; ++key){
      auto subtree = subtrees[ this->values[key] ];
      subtree->data = new node<T,N>(this->keys[key], this);
      this->children.emplace(this->keys[key], subtree);
    }
  }

  template<typename T>
  graph<T,1>::graph() : base_graph<T,1>(), std::unordered_map<agent, graph<T>*>(){
    agent name;
    for(name = 0; name < common::model->getNumAgents(); ++name){
      this->emplace(name, new graph<T>(name));
    }
  }

  template<typename T>
  graph<T,1>::graph(const std::vector<graph<T>*>& ih_set) : base_graph<T,1>(), std::unordered_map<agent, graph<T>*>(){
    for(auto ih : ih_set){
      this->emplace(ih->getAgent(), ih);
    }
  }

  template<typename T>
  graph<T,1>::~graph(){
    if( this->data == NULL ){
      for(agent name = 0; name < common::model->getNumAgents(); ++name){
        if( this->find(name) != this->end() ){
          delete this->at(name);
          this->at(name) = NULL;
        }
      }
    }

    this->clear();
  }

  template<typename T>
  graph<T,1>*const graph<T,1>::expand(const T& item, bool backup){
    return this->template base_expand<graph<T,1>>(item, backup);
  }

  template<>
  graph<observation,1>::graph(graph<observation,1>* parent, const observation& item, bool backup) : base_graph<observation,1>(parent, item, backup), std::unordered_map<agent, graph<observation>*>(){
    assert(item < common::model->getNumObservations());
    for(auto pair : *parent){
      auto o = common::model->getObservationIndex(pair.first, item);
      this->emplace(pair.first, pair.second->expand(o, backup));
    }
  }

  template<>
  graph<std::pair<action,observation>,1>::graph(graph<std::pair<action,observation>,1>* parent, const std::pair<action,observation>& item, bool backup) : base_graph<std::pair<action,observation>,1>(parent, item, backup), std::unordered_map<agent, graph<std::pair<action,observation>>*>(){
    for(auto pair : *parent){
      auto a = common::model->getActionIndex(pair.first, item.first);
      auto o = common::model->getObservationIndex(pair.first, item.second);
      this->emplace(pair.first, pair.second->expand(std::make_pair(a,o), backup));
    }
  }

  template class base_graph<observation>;
  template class base_graph<observation,1>;
  template class base_graph<std::pair<action,observation>>;
  template class base_graph<std::pair<action,observation>,1>;


  template class graph<observation>;
  template class graph<observation,1>;
  template class graph<std::pair<action,observation>>;
  template class graph<std::pair<action,observation>,1>;


}

namespace std{
  size_t hash<pair<sdm::action,sdm::observation>>::operator()(const pair<sdm::action,sdm::observation>& s) const {
    return hash<sdm::action>()(s.first) ^ hash<sdm::observation>()(s.second);
  }

  bool equal_to<pair<sdm::action,sdm::observation>>::operator()(const pair<sdm::action,sdm::observation>& lhs, const pair<sdm::action,sdm::observation>& rhs) const {
    return lhs == rhs;
  }
}
