//! \date 12 October 2019

#include "../include/algorithms/mdps/search/light_fbhsvi.hpp"

namespace sdm{

  template<typename ihistory_t, number optimizer, bool is_finite, bool pomdp>
  light_fbhsvi<ihistory_t, optimizer, is_finite, pomdp>::light_fbhsvi(number length_limit, number trials, std::string results){
    this->trials = trials;
    this->saveStatsBegins(results);
    limited_length::setLengthLimit(length_limit);
  }

  template<typename ihistory_t, number optimizer, bool is_finite, bool pomdp>
  light_fbhsvi<ihistory_t, optimizer, is_finite, pomdp>::~light_fbhsvi(){}

  template<typename ihistory_t, number optimizer, bool is_finite, bool pomdp>
  void light_fbhsvi<ihistory_t, optimizer, is_finite, pomdp>::solve(const std::shared_ptr<World>& world, horizon planning_horizon, double epsilon, double){
    this->epsilon = epsilon; common::error = epsilon;
    common::model = std::static_pointer_cast<dpomdp>(world);
    this->start = std::make_shared<light_occupancy_state<ihistory_t>>();
    common::model->setPlanningHorizon(is_finite ? planning_horizon : 0);

    this->start_time = std::chrono::high_resolution_clock::now();
    this->light_lb.initialize(is_finite);
    this->light_ub.initialize(limited_length::getLengthLimit(), this->trials, is_finite);

    this->trial = 0;
    do{
      this->explore(this->start,0.0, 0);
      this->trial++;
    }while( !this->stop(this->start,0.0,0) );

    this->saveStats(this->start, 0);
  }


  template<typename ihistory_t, number optimizer, bool is_finite, bool pomdp>
  void light_fbhsvi<ihistory_t, optimizer, is_finite, pomdp>::explore(std::shared_ptr<light_occupancy_state<ihistory_t>>& s, double r, horizon h){
    #ifdef DEBUG
    this->printDebug(s,h,false);
    #else
    this->saveStats(s,h);
    #endif

    if(!this->stop(s, r, h)){
      auto a = this->greedyActionAt(s, is_finite ? h : 0);
      auto z = this->getGreedyObservation(s, a, is_finite ? h : 0);
      auto action_observation = std::make_pair(a.first, z);
      auto next = s->next(action_observation,a.second);
      this->explore(next, r + pow(common::model->getDiscount(),h) * s->getReward(a), h+1);
      this->update(s, is_finite ? h : 0);
    }

    #ifdef DEBUG
    this->printDebug(s,h,true);
    #endif
  }

  template<typename ihistory_t, number optimizer, bool is_finite, bool pomdp>
  observation light_fbhsvi<ihistory_t, optimizer, is_finite, pomdp>::getGreedyObservation(std::shared_ptr<light_occupancy_state<ihistory_t>>& s, std::pair<action, std::shared_ptr<light_decision_rule>>& a, horizon h){
    observation z_star;
    double q, q_max = std::numeric_limits<double>::lowest();

    for(observation z=0; z<common::model->getNumObservations(0); ++z){
      q = this->gap(s, a, z, h);
      if(q > q_max){
        q_max = q;  z_star = z;
      }
    }

    return z_star;
  }

  template<typename ihistory_t, number optimizer, bool is_finite, bool pomdp>
  void light_fbhsvi<ihistory_t, optimizer, is_finite, pomdp>::update(std::shared_ptr<light_occupancy_state<ihistory_t>>& s, horizon h){
    this->light_lb.setValueAt(s, this->light_ub.getValueAt(s, is_finite ? h : 0), is_finite ? h : 0);
    this->light_ub.setValueAt(s, this->light_lb.getValueAt(s, is_finite ? h : 0), is_finite ? h : 0);
  }

  template<typename ihistory_t, number optimizer, bool is_finite, bool pomdp>
  double light_fbhsvi<ihistory_t, optimizer, is_finite, pomdp>::reward(std::shared_ptr<light_occupancy_state<ihistory_t>>& loc, std::pair<action, std::shared_ptr<light_decision_rule>>& a, horizon h){
    double reward = 0.0;

    for(auto& bp : loc->container){
      for(state x = 0; x < common::model->getNumStates(); ++x){
        for(action u2 = 0; u2 < common::model->getNumActions(1); ++u2){
          auto u = common::model->getJointActionIndex(std::vector<action>({a.first, u2}));
          reward += bp.second * (*bp.first)[x] * common::model->getReward(x,u) * a.second->getValueAt(bp.first, u2);
        }
      }
    }

    return pow(common::model->getDiscount(),h) * reward;
  }

  template<typename ihistory_t, number optimizer, bool is_finite, bool pomdp>
  double light_fbhsvi<ihistory_t, optimizer, is_finite, pomdp>::gap(std::shared_ptr<light_occupancy_state<ihistory_t>>& s, std::pair<action, std::shared_ptr<light_decision_rule>>& a, observation z, horizon h){
     auto action_observation = std::make_pair(a.first, z);
     auto s_next = s->next(action_observation, a.second);
     return std::abs(light_ub.getValueAt(s_next, is_finite ? h+1 : 0) - light_lb.getValueAt(s_next, is_finite ? h+1 : 0));
  }


  template<typename ihistory_t, number optimizer, bool is_finite, bool pomdp>
  bool light_fbhsvi<ihistory_t, optimizer, is_finite, pomdp>::stop(const std::shared_ptr<light_occupancy_state<ihistory_t>>& s, double, horizon h){
    return this->trial >= this->trials || this->light_ub.getValueAt(s,is_finite ? h : 0) - this->light_lb.getValueAt(s,is_finite ? h : 0) <= 2 * this->epsilon / (std::pow(common::model->getDiscount(),h));
  }

  template<typename ihistory_t, number optimizer, bool is_finite, bool pomdp>
  std::pair<action, std::shared_ptr<light_decision_rule>> light_fbhsvi<ihistory_t, optimizer, is_finite, pomdp>::greedyActionAt(std::shared_ptr<light_occupancy_state<ihistory_t>>& s, horizon h){
    double lower_bound = this->light_lb.getValueAt(s,is_finite ? h : 0);
    double upper_bound = this->light_ub.getValueAt(s, is_finite ? h : 0);
    if( !is_finite ) this->light_lb.setValueAt(s, upper_bound, is_finite ? h : 0);
    return is_finite ? this->light_ub.getGreedyDecisionAt(s, upper_bound, lower_bound, is_finite ? h : 0) : this->light_ub.setValueAt(s, lower_bound, is_finite ? h : 0);
  }

  template<typename ihistory_t, number optimizer, bool is_finite, bool pomdp>
  void light_fbhsvi<ihistory_t, optimizer, is_finite, pomdp>::printDebug(std::shared_ptr<light_occupancy_state<ihistory_t>>& s,horizon h, bool color){
    auto sl = this->light_lb.size(is_finite ? h : 0);
    auto su = this->light_ub.size(is_finite ? h : 0);
    auto ub = this->light_ub.getValueAt(s, is_finite ? h : 0);
    auto lb = this->light_lb.getValueAt(s, is_finite ? h : 0);
    auto dur = std::chrono::duration_cast<std::chrono::duration<double>>(std::chrono::high_resolution_clock::now() - this->start_time);
    if( color ) std::cout << "\033[1m\033[36m  <trial=\"" << this->trial << "\" depth=\"" << h << "\"  lb["<< h <<"]=\"" << lb  <<  "\" size["<< h <<"]=\"" << sl  <<  "\" ub["<< h <<"]=\"" << ub <<  "\" size["<< h <<"]=\"" << su << "\" stop=\"" << (ub - lb > 2*this->epsilon / std::pow(common::model->getDiscount(), h) ? "no" : "yes")<<"\" Time cost:"<< dur.count() << "\" /> \033[0m" << std::endl;
    else std::cout << "\033[1m\033[35m  <trial=\"" << this->trial << "\" depth=\"" << h << "\"  lb["<< h <<"]=\"" << lb  <<  "\" size["<< h <<"]=\"" << sl  <<  "\" ub["<< h <<"]=\"" << ub <<  "\" size["<< h <<"]=\"" << su << "\" stop=\"" << (ub - lb > 2*this->epsilon / std::pow(common::model->getDiscount(), h) ? "no" : "yes")<<"\" Time cost:"<< dur.count() << "\" /> \033[0m" << std::endl;
  }

  template<typename ihistory_t, number optimizer, bool is_finite, bool pomdp>
  void light_fbhsvi<ihistory_t, optimizer, is_finite, pomdp>::saveStatsBegins(std::string results, bool save){
    if( save ){
      this->perf_out.open(results);
      perf_out << "trial"<< "," << "horizon" << ","<< "ub_value" << "," << "lb_value" << ", "<< "ub_size" << ","<< "lb_size" << ","<< "time" << std::endl;
    }
  }

  template<typename ihistory_t, number optimizer, bool is_finite, bool pomdp>
  void light_fbhsvi<ihistory_t, optimizer, is_finite, pomdp>::saveStats(std::shared_ptr<light_occupancy_state<ihistory_t>>& s,horizon h){
    auto dur = std::chrono::duration_cast<std::chrono::duration<double>>(std::chrono::high_resolution_clock::now() - this->start_time);
    if(h == 0) perf_out << this->trial<< "," << h << ","<< this->light_ub.getValueAt(s, is_finite ? h : 0) << "," << this->light_lb.getValueAt(s, is_finite ? h : 0) << ","<< this->light_ub.size() << ","<< this->light_lb.size() << ","<< dur.count() << std::endl;
    if(dur.count() > 18000) exit(0);
  }

  template class light_fbhsvi<observation_ihistory, ENUMERATION>;
  template class light_fbhsvi<observation_ihistory, ENUMERATION, INFINITE_HORIZON>;
  template class light_fbhsvi<observation_ihistory, LINEAR_PROGRAM>;
  template class light_fbhsvi<observation_ihistory, LINEAR_PROGRAM, INFINITE_HORIZON>;
  template class light_fbhsvi<observation_ihistory, ENUMERATION,FINITE_HORIZON, POMDP>;
  template class light_fbhsvi<observation_ihistory, LINEAR_PROGRAM,FINITE_HORIZON, POMDP>;
}
