#include "../include/dpomdp/__agent__.hpp"

namespace sdm{
  agent __agent__::getNumAgents() const{
    return this->number_agents;
  }

  void __agent__::setNumAgents(agent number_agents){
    this->number_agents = number_agents;
  }

  void __agent__::setNumAgents(const std::vector<std::string>& agent_names){
    this->setNumAgents( agent_names.size() );

    number idx =0;
    for(idx=0; idx<this->number_agents; ++idx)
      agent_names_bimap.insert( name2index(agent_names[idx], idx) );
  }

  agent __agent__::getAgentIndex(const std::string& name){
    return this->agent_names_bimap.empty() ? 0 : this->agent_names_bimap.left.at(name);
  }

  std::string __agent__::getAgentName(agent ag){
    return this->agent_names_bimap.empty() ? std::to_string(ag) : this->agent_names_bimap.right.at(ag);
  }

}
