#-----------------------------------------------------------------------------------------------------
#
# Makefile
#
# AUTHORS
#
#-----------------------------------------------------------------------------------------------------

# You can choose one linear algebra library and comment all other
EIGEN  				:= -DEIGEN
BOOST  				:= #-DBOOST
UCB           := #-DUCB
OPENMP 				:= #-DOMP //TODO seems to fail to improve performances, worst it slows down the performances
DENSE  				:= -DDENSE
SPARSE 				:= #-DSPARSE

LIBRARY				:= $(EIGEN) $(BOOST) $(DENSE) $(SPARSE) $(OPENMP) $(UCB)

# Determine the platform
UNAME_S       :=	$(shell uname -s)


#-----------------------------------------------------------------------------------------------------
ifeq ($(UNAME_S), Darwin)
	SYSTEM			:=	x86-64_osx
else
	SYSTEM			:=	x86-64_linux
endif

LIBFORMAT			:=	static_pic

#-----------------------------------------------------------------------------------------------------
# When you adapt this makefile to compile your programs please copy this makefile and set
# CPLEXDIR and CONCERTDIR to the directories where CPLEX and CONCERT are in  installed.
#-----------------------------------------------------------------------------------------------------
ifeq ($(UNAME_S), Darwin)
	BOOSTDIR			:=	/usr/local
	PYTHONCFLAGS	:=  -I/usr/local/Cellar/python3/3.7.2_2/Frameworks/Python.framework/Versions/3.7/include/python3.7m -I/usr/local/Cellar/python3/3.7.2_2/Frameworks/Python.framework/Versions/3.7/include/python3.7m -Wno-unused-result -Wsign-compare -Wunreachable-code -fno-common -dynamic -DNDEBUG -g -fwrapv -O3 -Wall -Wstrict-prototypes
	PYTHONLDFLAGS	:=  -L/usr/local/opt/python3/Frameworks/Python.framework/Versions/3.7/lib/python3.7/config-3.7m-darwin -lpython3.7m -ldl -framework CoreFoundation
	CPLEXPATH			:=	/Applications/CPLEX_Studio128
else
	PYTHONCFLAGS	:=  -I/usr/include/python2.7 -I/usr/include/x86_64-linux-gnu/python2.7  -fno-strict-aliasing -D_FORTIFY_SOURCE=2 -g -fstack-protector --param=ssp-buffer-size=4 -Wformat -Werror=format-security  -DNDEBUG -g -fwrapv -O2 -Wall -Wstrict-prototypes
	PYTHONLDFLAGS	:=  -L/usr/lib/python2.7/config-x86_64-linux-gnu -L/usr/lib -lpthread -ldl  -lutil -lm  -lpython2.7 -Xlinker -export-dynamic -Wl,-O1 -Wl,-Bsymbolic-functions
	BOOSTDIR			:=  /usr/local
	CPLEXPATH			:=	/opt/ibm/ILOG/CPLEX_Studio128
endif

CPLEXDIR			:=	$(CPLEXPATH)/cplex
CONCERTDIR		:= 	$(CPLEXPATH)/concert
CPLEXBINDIR		:=	$(CPLEXDIR)/bin/$(SYSTEM)
CPLEXLIBDIR		:=	$(CPLEXDIR)/lib/$(SYSTEM)/$(LIBFORMAT)
CONCERTLIBDIR	:=	$(CONCERTDIR)/lib/$(SYSTEM)/$(LIBFORMAT)
SRCDIR				:=	src
BINDIR				:=	build
OBJDIR				:=	obj
DOCDIR				:=	docs

#-----------------------------------------------------------------------------------------------------
# Target executable
#-----------------------------------------------------------------------------------------------------
TARGET				:=	$(BINDIR)/sdms

#-----------------------------------------------------------------------------------------------------
# Compiler selection
#-----------------------------------------------------------------------------------------------------

ifeq ($(UNAME_S), Darwin)
	CXX	:=	clang++
else
	CXX	:=	clang++
endif

#-----------------------------------------------------------------------------------------------------
# Compiler options
#-----------------------------------------------------------------------------------------------------
CXXOPT				:= $(LIBRARY)


#-----------------------------------------------------------------------------------------------------
# Link options and libraries
#-----------------------------------------------------------------------------------------------------
BOOSTLIBDIR		:=	$(BOOSTDIR)/lib

CXXLNDIRS		  := -L$(BOOSTLIBDIR)
CXXLNFLAGS		:= -lboost_iostreams -lboost_filesystem -lboost_system -lboost_program_options

CPLEXFLAGS		:= -isystem$(CPLEXDIR)/include -isystem$(CONCERTDIR)/include -DNDEBUG
CPLEXLNFLAGS  := -L$(CPLEXLIBDIR) -lilocplex -lcplex -L$(CONCERTLIBDIR) -lconcert -lm -m64 -lpthread

ifeq ($(UNAME_S), Darwin)
	BOOSTINCDIR	:= $(BOOSTDIR)/include/boost
	EIGENINCDIR := /usr/local/include/eigen3
else
	BOOSTINCDIR	:= $(BOOSTDIR)/include/boost
	EIGENINCDIR := /usr/include/eigen3
endif

CXXFLAGS			:= $(CXXOPT) -I$(BOOSTINCDIR) -I$(EIGENINCDIR)

ifeq ($(UNAME_S), Darwin)
	CXXFLAGS += -O3 -W -Wall -pedantic -std=c++1y -DIL_STD
else
	CXXFLAGS += -O3 -W -Wall -pedantic -std=c++1y -DIL_STD # change the optimize level to debug.
endif

#-----------------------------------------------------------------------------------------------------
# make all	:	to compile the sources.
# make execute	:	to execute the examples.
#-----------------------------------------------------------------------------------------------------
EXT						:=	cpp

#-----------------------------------------------------------------------------------------------------
# Code lists
#-----------------------------------------------------------------------------------------------------
SRC						:=	$(shell find $(SRCDIR) -type f -name *.$(EXT))
OBJ						:=	$(patsubst $(SRCDIR)/%, $(OBJDIR)/%, $(SRC:.$(EXT)=.o))
OBJ_NO_MAIN		:=	$(filter-out $(OBJDIR)/main.o,$(OBJ))
TEMP					:=	$(patsubst $(SRCDIR)/%, $(OBJDIR)/%, $(SRC:.$(EXT)=.d))

#-----------------------------------------------------------------------------------------------------
# Folder lists
#-----------------------------------------------------------------------------------------------------
INCDIRS				:=	$(shell find include/**/* -name '*.hpp' -exec dirname {} \; | sort | uniq)
INCLIST				:=	$(patsubst include/%, -isystem include/%, $(INCDIRS))
BINLIST				:=	$(patsubst include/%, $(BINDIR)/%, $(INCDIRS))
INC						:=	-isystem include $(INCLIST)

#------------------------------------------------------------------------------------------------------
# Default target
default:	all

# Target all builds the executable
all:		$(TARGET)

# Current size of the projet
count:
			@ find . "(" -name "Makefile" -or -name "*.hpp" -or -name "*.cpp" ")" -exec wc -l {} +

debug: CXXFLAGS += -DDEBUG -pg -g
debug: $(TARGET)

# The constant DEBUG is set at yes
ifeq ($(DEBUG),yes)
		CXXFLAGS += -pg -g
endif

#-----------------------------------------------------------------------------------------------------
.PHONY:		help clean mrproper commit pull
.PRECIOUS:	$(OBJDIR)/%.o $(OBJDIR)/%.d

# Display the help message
help:
		@ echo 'Usage: '
		@ echo 'make		Building project executable'
		@ echo 'make all	Building project executable'
		@ echo 'make clean	Clean objects directory'
		@ echo 'make mrproper	Delete objects and binary directory'
		@ echo 'make help	Show this help message'
		@ echo ''

clean:
		@ echo '\033[1m\033[32m ######################## Cleaning...\033[0m'
		@ echo 'rm -rf $(OBJ) *~'
		@ rm -rf $(OBJ) *~

mrproper:	clean
		@ echo 'Deleting ...'
		@ echo 'rm -rf $(OBJDIR) $(BINDIR) $(TARGET) $(TSTDIR)/*.out $(TEST_OBJ) $(TEST_EXE) *.csv *.lp *.txt *~'
		@ rm -rf $(OBJDIR) $(BINDIR) $(TARGET) $(TSTDIR)/*.out $(TEST_OBJ) $(TEST_EXE) $(DOCDIR) *.csv *.lp *.txt *~

commit:
		@ echo 'Update git repository ...'
		@ echo 'git add -A; git commit -m Me; git push'
		@ git add -A; git commit -m "Me"; git push

pull:
		@ echo 'Update git repository ...'
		@ echo 'git pull; make mrproper; make -j30 debug'
		@ git pull; make mrproper; make -j30 debug
push:
		@git status; git stage .; git commit -m ""; git push;

#-----------------------------------------------------------------------------------------------------
# Sources
#-----------------------------------------------------------------------------------------------------
$(TARGET):	$(OBJ)
		@ echo 'Linking ...'
		@ echo 'Linking $(TARGET)'
		@ mkdir -p $(BINDIR)
		@ echo '$(CXX) -o $@ $^ $(CXXLNDIRS) $(CXXLNFLAGS) $(PYTHONLDFLAGS) $(CXXFLAGS) $(PYTHONCFLAGS) $(CPLEXLNFLAGS)'; $(CXX) -o $@ $^ $(CXXLNDIRS) $(CXXLNFLAGS) $(PYTHONLDFLAGS) $(CXXFLAGS) $(PYTHONCFLAGS) $(CPLEXLNFLAGS)

$(OBJDIR)/%.o:	$(SRCDIR)/%.$(EXT) $(OBJDIR)/%.d 
		@ echo 'Compiling $< ...'
		@ mkdir -p $(OBJDIR)
		@ echo '$(CXX) -c -o $@ $< $(INC) $(CXXFLAGS) $(PYTHONCFLAGS) $(CPLEXFLAGS)'; $(CXX) -c -o $@ $< $(INC) $(CXXFLAGS) $(PYTHONCFLAGS) $(CPLEXFLAGS)

$(BINDIR)/%.exe: $(OBJ_NO_MAIN) $(OBJDIR)/%.o
		@ echo 'Linking ...'
		@ echo 'Linking $@'
		@ mkdir -p $(BINDIR)
		@ echo '$(CXX) -o $@ $^ $(CXXLNDIRS) $(CXXLNFLAGS) $(PYTHONLDFLAGS) $(CXXFLAGS) $(PYTHONCFLAGS) $(CPLEXLNFLAGS)'; $(CXX) -o $@ $^ $(CXXLNDIRS) $(CXXLNFLAGS) $(PYTHONLDFLAGS) $(CXXFLAGS) $(PYTHONCFLAGS)  $(CPLEXLNFLAGS)


#-----------------------------------------------------------------------------------------------------
# Dependency
#-----------------------------------------------------------------------------------------------------
$(OBJDIR)/%.d : $(SRCDIR)/%.$(EXT)
		@ echo 'Checking $@ ...'
		@ mkdir -p $(OBJDIR)
		@ set -e; rm -f $@; \
  	$(CC) -MM $(CPPFLAGS) $(CXXFLAGS) $(CPLEXFLAGS) $< > $@.$$$$; \
  	sed 's,\($*\)\.o[ :]*,\1.o $@ : ,g' < $@.$$$$ > $@; \
  	rm -f $@.$$$$
  
ifeq ($(MAKECMDGOALS),$(filter-out  push clean mrproper commit, $(MAKECMDGOALS)))
-include $(TEMP)	
endif

#-----------------------------------------------------------------------------------------------------
# Local Variables:
# mode: makefile
# End:
